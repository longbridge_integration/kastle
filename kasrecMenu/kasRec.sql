set echo off
set termout off
set heading off
set verify off
set pagesize 0
set lines 500
set trims on
set feedback off
set serveroutput on size 1000000
set colsep |

exec dbms_output.enable(null);

spool kasRec.lst

declare
acctName varchar2(100);
acctNum varchar2(20);
schmcode varchar2(15);
offAcctName varchar2(200);
crncyCode varchar2(20);
pstdUser varchar2(50);
tranType        varchar2(10);
tranPart        varchar2(200);
refNum       varchar2(20);
tranId       varchar2(20);
tranDate   Date;
valDate   Date;
prodCode        varchar2(20);
tranAmt     Number(25,4);
frmDate   Date;
toDate   Date;

cursor st is 
select (select acct_name from tbaadm.gam where acid = b2k_id)"Loan_Account_Name",
(select schm_code from tbaadm.gam where acid = b2k_id)"Product_Code",
g.foracid,g.acct_name,d.ref_num,g.schm_code,g.ACCT_CRNCY_CODE,
d.tran_date,d.value_date,d.tran_id,d.PART_TRAN_TYPE,d.tran_amt,
d.TRAN_PARTICULAR"Narration",d.PSTD_USER_ID,to_date('&&2','dd-mm-yyyy'),to_date('&&3','dd-mm-yyyy')
from tbaadm.acpart a,tbaadm.dtd d,tbaadm.gam g
where trim(d.tran_id) = trim(a.tran_id)
and a.tran_date = d.tran_date
and a.partitioned_acid = d.acid
and d.acid = g.acid
and a.tran_amt = d.tran_amt
and a.part_tran_srl_num = d.part_tran_srl_num
and g.bacid = (select bacid from tbaadm.gam where acid = a.partitioned_acid)
and g.bacid = '&&1'
--and a.partitioned_acid = (select acid from tbaadm.gam where foracid = upper('&&1')) 
and a.tran_date between to_date('&&2','dd-mm-yyyy') and to_date('&&3','dd-mm-yyyy')
union all
select (select acct_name from tbaadm.gam where acid = b2k_id)"Loan_Account_Name",
(select schm_code from tbaadm.gam where acid = b2k_id)"Product_Code",
g.foracid,g.acct_name,d.ref_num,g.schm_code,g.ACCT_CRNCY_CODE,
d.tran_date,d.value_date,d.tran_id,d.PART_TRAN_TYPE,d.tran_amt,
d.TRAN_PARTICULAR"Narration",d.PSTD_USER_ID,to_date('&&2','dd-mm-yyyy'),to_date('&&3','dd-mm-yyyy')
from tbaadm.acpart a,tbaadm.htd d,tbaadm.gam g
where trim(d.tran_id) = trim(a.tran_id)
and a.tran_date = d.tran_date
and a.PARTITIONED_ACID = d.acid
and d.acid = g.acid
and a.tran_amt = d.tran_amt
and a.part_tran_srl_num = d.part_tran_srl_num
and g.bacid = (select bacid from tbaadm.gam where acid = a.partitioned_acid)
and g.bacid = '&&1'
--and a.partitioned_acid = (select acid from tbaadm.gam where foracid = upper('&&1'))
and a.tran_date between to_date('&&2','dd-mm-yyyy') and to_date('&&3','dd-mm-yyyy');

begin
open st;
loop
fetch st into acctName,prodCode,acctNum,offAcctName,refNum,schmcode,crncyCode,tranDate,valDate,tranId,tranType,tranAmt,tranPart,pstdUser,frmDate,toDate;
exit when st%notfound;

dbms_output.put_line(acctName||'|'||
                    prodCode||'|'||
                    acctNum||'|'||
                    offAcctName||'|'||
                    refNum||'|'||
                    schmcode||'|'||
                    crncyCode||'|'||
                    tranDate||'|'||
                    valDate||'|'||
                    tranId||'|'||
                    tranType||'|'||
                    tranAmt||'|'||
                    tranPart||'|'||
                    pstdUser||'|'||
                    frmDate||'|'||
                    toDate);
                    
end loop;

close st;




end;

/

spool off;

spool /finacle/UATFCMB/APP/Finacle/FC/app/cust/01/INFENG/reportExcels/kasRec.xlsx

declare
acctName varchar2(100);
acctNum varchar2(20);
schmcode varchar2(15);
offAcctName varchar2(200);
crncyCode varchar2(20);
pstdUser varchar2(50);
tranType        varchar2(10);
tranPart        varchar2(200);
refNum       varchar2(20);
tranId       varchar2(20);
tranDate   Date;
valDate   Date;
prodCode        varchar2(20);
tranAmt     Number(25,4);
frmDate   Date;
toDate   Date;

cursor st is 
select (select acct_name from tbaadm.gam where acid = b2k_id)"Loan_Account_Name",
(select schm_code from tbaadm.gam where acid = b2k_id)"Product_Code",
g.foracid,g.acct_name,d.ref_num,g.schm_code,g.ACCT_CRNCY_CODE,
d.tran_date,d.value_date,d.tran_id,d.PART_TRAN_TYPE,d.tran_amt,
d.TRAN_PARTICULAR"Narration",d.PSTD_USER_ID,to_date('&&2','dd-mm-yyyy'),to_date('&&3','dd-mm-yyyy')
from tbaadm.acpart a,tbaadm.dtd d,tbaadm.gam g
where trim(d.tran_id) = trim(a.tran_id)
and a.tran_date = d.tran_date
and a.partitioned_acid = d.acid
and d.acid = g.acid
and a.tran_amt = d.tran_amt
and a.part_tran_srl_num = d.part_tran_srl_num
and g.bacid = (select bacid from tbaadm.gam where acid = a.partitioned_acid)
and g.bacid = '&&1'
--and a.partitioned_acid = (select acid from tbaadm.gam where foracid = upper('&&1')) 
and a.tran_date between to_date('&&2','dd-mm-yyyy') and to_date('&&3','dd-mm-yyyy')
union all
select (select acct_name from tbaadm.gam where acid = b2k_id)"Loan_Account_Name",
(select schm_code from tbaadm.gam where acid = b2k_id)"Product_Code",
g.foracid,g.acct_name,d.ref_num,g.schm_code,g.ACCT_CRNCY_CODE,
d.tran_date,d.value_date,d.tran_id,d.PART_TRAN_TYPE,d.tran_amt,
d.TRAN_PARTICULAR"Narration",d.PSTD_USER_ID,to_date('&&2','dd-mm-yyyy'),to_date('&&3','dd-mm-yyyy')
from tbaadm.acpart a,tbaadm.htd d,tbaadm.gam g
where trim(d.tran_id) = trim(a.tran_id)
and a.tran_date = d.tran_date
and a.PARTITIONED_ACID = d.acid
and d.acid = g.acid
and a.tran_amt = d.tran_amt
and a.part_tran_srl_num = d.part_tran_srl_num
and g.bacid = (select bacid from tbaadm.gam where acid = a.partitioned_acid)
and g.bacid = '&&1'
--and a.partitioned_acid = (select acid from tbaadm.gam where foracid = upper('&&1'))
and a.tran_date between to_date('&&2','dd-mm-yyyy') and to_date('&&3','dd-mm-yyyy');

begin
open st;
loop
fetch st into acctName,prodCode,acctNum,offAcctName,refNum,schmcode,crncyCode,tranDate,valDate,tranId,tranType,tranAmt,tranPart,pstdUser,frmDate,toDate;
exit when st%notfound;

dbms_output.put_line(acctName||'|'||
                    prodCode||'|'||
                    acctNum||'|'||
                    offAcctName||'|'||
                    refNum||'|'||
                    schmcode||'|'||
                    crncyCode||'|'||
                    tranDate||'|'||
                    valDate||'|'||
                    tranId||'|'||
                    tranType||'|'||
                    tranAmt||'|'||
                    tranPart||'|'||
                    pstdUser||'|'||
                    frmDate||'|'||
                    toDate);
                    
end loop;

close st;




end;

/

spool off;
