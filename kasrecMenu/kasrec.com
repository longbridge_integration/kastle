##########################################################################################################################
# Script Name    	: kasrec.com
# Author         	: LONGBRIDGE
# Script Calls   	: kasrec.scr
# Description           : calls kasRec.scr and transfers the input file name and transfer flag value to it
# Date           	: 27-09-2015
# Modification Log 	: <none>
###########################################################################################################################


exebatch babx4061 $B2K_SESSION_ID $1 $2 $3 $4 $5 $6
#exebatch babx4061 0 $1
if [ $? -ne 0 ]
then
                echo "com file Processing Failed."
                exit 1
fi
exit 0