<%--
  Created by IntelliJ IDEA.
  User: LB-PRJ-020
  Date: 9/23/2017
  Time: 7:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>KastleFI</title>
</head>
<body>
<h3>Welcome to KastleFI Interface Services</h3>
<a href="${pageContext.request.requestURL}services/AcctClosure?wsdl">AcctClosure</a><br>
<a href="${pageContext.request.requestURL}services/AcctInq?wsdl">AcctInq</a><br>
<a href="${pageContext.request.requestURL}services/AverageBalance?wsdl">AverageBalance</a><br>
<a href="${pageContext.request.requestURL}services/BulkPayment?wsdl">BulkPayment</a><br>
<a href="${pageContext.request.requestURL}services/BranchTransfer?wsdl">BranchTransfer</a><br>
<a href="${pageContext.request.requestURL}services/CustInq?wsdl">CustInq</a><br>
<a href="${pageContext.request.requestURL}services/DrawPower?wsdl">DrwnPower</a><br>
<a href="${pageContext.request.requestURL}services/doAccrual?wsdl">EOD</a><br>
<a href="${pageContext.request.requestURL}services/ExposureCheck?wsdl">ExposureCheck</a><br/>
<a href="${pageContext.request.requestURL}services/LienUpdate?wsdl">LienUpdate</a><br>
<a href="${pageContext.request.requestURL}services/Topup?wsdl">Topup</a><br>
<a href="${pageContext.request.requestURL}services/getLastNTransactionsWithPagination?wsdl">LastNTransactionWithDescription</a><br>
</body>
</html>
