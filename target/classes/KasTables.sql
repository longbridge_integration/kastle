---Table 1
DROP TABLE CUSTOM.KAS_ACCT_CLOSURE_AUDIT_TBL CASCADE CONSTRAINTS;

CREATE TABLE CUSTOM.KAS_ACCT_CLOSURE_AUDIT_TBL
(
  ID             NUMBER,
  REQUEST_ID     VARCHAR2(255 CHAR),
  APP_CODE       VARCHAR2(12 CHAR),
  ACCT_NUM       VARCHAR2(20 CHAR),
  RESPONSE_CODE  VARCHAR2(2 CHAR),
  ERROR_MESSAGE  VARCHAR2(255 CHAR),
  RESP_MESSAGE   VARCHAR2(255 CHAR),
  REQUEST_DATE   DATE,
  RESPONSE_DATE  DATE,
  STATUS         VARCHAR2(20 CHAR)
);

DROP SYNONYM TBAGEN.KAS_ACCT_CLOSURE_AUDIT_TBL;

CREATE SYNONYM TBAGEN.KAS_ACCT_CLOSURE_AUDIT_TBL FOR CUSTOM.KAS_ACCT_CLOSURE_AUDIT_TBL;

DROP SYNONYM TBAUTIL.KAS_ACCT_CLOSURE_AUDIT_TBL;

CREATE SYNONYM TBAUTIL.KAS_ACCT_CLOSURE_AUDIT_TBL FOR CUSTOM.KAS_ACCT_CLOSURE_AUDIT_TBL;

GRANT  INSERT, SELECT, UPDATE ON CUSTOM.KAS_ACCT_CLOSURE_AUDIT_TBL TO PUBLIC;


--Table 1 Sequence
DROP SEQUENCE CUSTOM.KAS_ACCT_CLOSURE_AUDIT_SEQ;

CREATE SEQUENCE CUSTOM.KAS_ACCT_CLOSURE_AUDIT_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;
  

---Table 2
DROP TABLE CUSTOM.KAS_APPROVED_SYSTEMS_TBL CASCADE CONSTRAINTS;

CREATE TABLE CUSTOM.KAS_APPROVED_SYSTEMS_TBL
(
  SEQ_NUM        NUMBER(30),
  IP_ADDRESS     VARCHAR2(20 BYTE),
  SYSTEM_DESC    VARCHAR2(5 BYTE),
  APPROVED_DATE  DATE                           DEFAULT SYSDATE
);

CREATE UNIQUE INDEX CUSTOM.KAS_APPROVED_SYSTEMS_TBL_PK ON CUSTOM.KAS_APPROVED_SYSTEMS_TBL
(IP_ADDRESS, SYSTEM_DESC)
LOGGING
TABLESPACE USERS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

GRANT INSERT,SELECT,UPDATE ON CUSTOM.KAS_APPROVED_SYSTEMS_TBL TO PUBLIC;
--Table 2 sequence
DROP SEQUENCE CUSTOM.KAS_APPROVED_SYSTEMS_SEQ;

CREATE SEQUENCE CUSTOM.KAS_APPROVED_SYSTEMS_SEQ
  START WITH 1
  MAXVALUE 1000000000000000000000000000
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;



--Table 3
DROP TABLE CUSTOM.KAS_BR_TRANS_AUDIT_TBL CASCADE CONSTRAINTS;

CREATE TABLE CUSTOM.KAS_BR_TRANS_AUDIT_TBL
(
  ID             NUMBER,
  REQUEST_ID     VARCHAR2(255 CHAR),
  APP_CODE       VARCHAR2(12 CHAR),
  ACCT_NUM       VARCHAR2(20 CHAR),
  TO_SOL         VARCHAR2(20 CHAR),
  FROM_SOL       VARCHAR2(20 CHAR),
  RESPONSE_CODE  VARCHAR2(2 CHAR),
  ERROR_MESSAGE  VARCHAR2(255 CHAR),
  RESP_MESSAGE   VARCHAR2(255 CHAR),
  REQUEST_DATE   DATE,
  RESPONSE_DATE  DATE,
  STATUS         VARCHAR2(20 CHAR)
);

DROP SYNONYM TBAGEN.TBA_KAS_BR_TRANS_AUDIT_TBL;

CREATE SYNONYM TBAGEN.TBA_KAS_BR_TRANS_AUDIT_TBL FOR CUSTOM.KAS_BR_TRANS_AUDIT_TBL;

DROP SYNONYM TBAUTIL.KAS_BR_TRANS_AUDIT_TBL;

CREATE SYNONYM TBAUTIL.KAS_BR_TRANS_AUDIT_TBL FOR CUSTOM.KAS_BR_TRANS_AUDIT_TBL;

GRANT INSERT, SELECT, UPDATE ON CUSTOM.KAS_BR_TRANS_AUDIT_TBL TO PUBLIC;
--Table 3 Sequence
DROP SEQUENCE CUSTOM.KAS_BR_TRANS_AUDIT_SEQ;

CREATE SEQUENCE CUSTOM.KAS_BR_TRANS_AUDIT_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;

--Table 4
DROP TABLE CUSTOM.KAS_BULK_PAY_AUDIT_TBL CASCADE CONSTRAINTS;

CREATE TABLE CUSTOM.KAS_BULK_PAY_AUDIT_TBL
(
  ID             NUMBER,
  REQUEST_ID     VARCHAR2(255 CHAR),
  APP_CODE       VARCHAR2(12 CHAR),
  ACCT_NUM       VARCHAR2(1000 CHAR),
  AMOUNT         VARCHAR2(100 CHAR),
  RESPONSE_CODE  VARCHAR2(2 CHAR),
  ERROR_MESSAGE  VARCHAR2(255 CHAR),
  RESP_MESSAGE   VARCHAR2(255 CHAR),
  REQUEST_DATE   DATE,
  RESPONSE_DATE  DATE,
  STATUS         VARCHAR2(50 CHAR)
);

DROP SYNONYM TBAGEN.KAS_BULK_PAY_AUDIT_TBL;

CREATE SYNONYM TBAGEN.KAS_BULK_PAY_AUDIT_TBL FOR CUSTOM.KAS_BULK_PAY_AUDIT_TBL;

DROP SYNONYM TBAUTIL.KAS_BULK_PAY_AUDIT_TBL;

CREATE SYNONYM TBAUTIL.KAS_BULK_PAY_AUDIT_TBL FOR CUSTOM.KAS_BULK_PAY_AUDIT_TBL;

GRANT INSERT, SELECT, UPDATE ON CUSTOM.KAS_BULK_PAY_AUDIT_TBL TO PUBLIC;
--Table 4 Sequence
DROP SEQUENCE CUSTOM.KAS_BULK_PAY_AUDIT_SEQ;

CREATE SEQUENCE CUSTOM.KAS_BULK_PAY_AUDIT_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;




--Table 5
DROP TABLE CUSTOM.KAS_DRW_POW_AUDIT_TBL CASCADE CONSTRAINTS;

CREATE TABLE CUSTOM.KAS_DRW_POW_AUDIT_TBL
(
  ID             NUMBER,
  REQUEST_ID     VARCHAR2(255 CHAR),
  APP_CODE       VARCHAR2(12 CHAR),
  ACCT_NUM       VARCHAR2(20 CHAR),
  EVENT          VARCHAR2(255 CHAR),
  APP_DATE       DATE,
  DRAW_POW       VARCHAR2(255 CHAR),
  IND_CODE       VARCHAR2(20 CHAR),
  RESPONSE_CODE  VARCHAR2(2 CHAR),
  ERROR_MESSAGE  VARCHAR2(255 CHAR),
  RESP_MESSAGE   VARCHAR2(255 CHAR),
  REQUEST_DATE   DATE,
  RESPONSE_DATE  DATE,
  STATUS         VARCHAR2(20 CHAR),
  REMARK         VARCHAR2(255 BYTE)
);

DROP SYNONYM TBAUTIL.KAS_DRW_POW_AUDIT_TBL;

CREATE SYNONYM TBAUTIL.KAS_DRW_POW_AUDIT_TBL FOR CUSTOM.KAS_DRW_POW_AUDIT_TBL;


DROP SYNONYM TBAGEN.KAS_DRW_POW_AUDIT_TBL;

CREATE SYNONYM TBAGEN.KAS_DRW_POW_AUDIT_TBL FOR CUSTOM.KAS_DRW_POW_AUDIT_TBL;

GRANT INSERT, SELECT, UPDATE ON CUSTOM.KAS_DRW_POW_AUDIT_TBL TO PUBLIC;
--Table 5 sequence
DROP SEQUENCE CUSTOM.KAS_DRW_POW_AUDIT_SEQ;

CREATE SEQUENCE CUSTOM.KAS_DRW_POW_AUDIT_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;



--Table 6
DROP TABLE CUSTOM.KAS_LIEN_UPDATE_AUDIT_TBL CASCADE CONSTRAINTS;

CREATE TABLE CUSTOM.KAS_LIEN_UPDATE_AUDIT_TBL
(
  REQUEST_ID         VARCHAR2(255 CHAR),
  APP_CODE           VARCHAR2(12 CHAR),
  LAON_OP_ACCT_NUM   VARCHAR2(20 CHAR),
  LOAN_TRN_AMT       VARCHAR2(20 CHAR),
  LOAN_ACCT_NUM      VARCHAR2(20 CHAR),
  FRLN_AMT           VARCHAR2(20 CHAR),
  FRLN_RCODE         VARCHAR2(20 CHAR),
  FRLN_REMARK        VARCHAR2(500 CHAR),
  FRLN_EXP_DATE      VARCHAR2(20 BYTE),
  MDLN_LIENID        VARCHAR2(20 CHAR),
  MDLN_UNLIEN_AMT    VARCHAR2(20 CHAR),
  ACCT_INFO          VARCHAR2(4000 CHAR),
  RESPONSE_CODE      VARCHAR2(2 CHAR),
  ERROR_MESSAGE      VARCHAR2(4000 CHAR),
  RESP_MESSAGE       VARCHAR2(4000 CHAR),
  REQUEST_DATE       TIMESTAMP(3),
  RESPONSE_DATE      TIMESTAMP(3),
  STATUS             VARCHAR2(20 CHAR),
  LOAN_TRN_AMT_CURR  VARCHAR2(3 BYTE),
  SESSION_ID         VARCHAR2(255 CHAR)         NOT NULL,
  ACCT_INFO_RESP     VARCHAR2(4000 CHAR)
);

CREATE UNIQUE INDEX CUSTOM.KAS_LIEN_UPDATE_AUDIT_TBL_PK ON CUSTOM.KAS_LIEN_UPDATE_AUDIT_TBL
(REQUEST_ID,SESSION_ID)
LOGGING
TABLESPACE USERS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

DROP SYNONYM TBAGEN.KAS_LIEN_UPDATE_AUDIT_TBL;

CREATE SYNONYM TBAGEN.KAS_LIEN_UPDATE_AUDIT_TBL FOR CUSTOM.KAS_LIEN_UPDATE_AUDIT_TBL;

DROP SYNONYM TBAUTIL.KAS_LIEN_UPDATE_AUDIT_TBL;

CREATE SYNONYM TBAUTIL.KAS_LIEN_UPDATE_AUDIT_TBL FOR CUSTOM.KAS_LIEN_UPDATE_AUDIT_TBL;

ALTER TABLE CUSTOM.KAS_LIEN_UPDATE_AUDIT_TBL ADD (
  CONSTRAINT KAS_LIEN_UPDATE_AUDIT_TBL_CT
 PRIMARY KEY
 (SESSION_ID)
    USING INDEX 
    TABLESPACE USERS
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT INSERT, UPDATE ON CUSTOM.KAS_LIEN_UPDATE_AUDIT_TBL TO PUBLIC;


---Table 7
DROP TABLE CUSTOM.KAS_TOPUP_AUDIT_TBL CASCADE CONSTRAINTS;

CREATE TABLE CUSTOM.KAS_TOPUP_AUDIT_TBL
(
  ID             NUMBER,
  REQUEST_ID     VARCHAR2(255 CHAR),
  APP_CODE       VARCHAR2(12 CHAR),
  ACCT_NUM       VARCHAR2(20 CHAR),
  TOPUP          VARCHAR2(255 CHAR),
  RESPONSE_CODE  VARCHAR2(2 CHAR),
  ERROR_MESSAGE  VARCHAR2(255 CHAR),
  RESP_MESSAGE   VARCHAR2(255 CHAR),
  REQUEST_DATE   DATE,
  RESPONSE_DATE  DATE,
  STATUS         VARCHAR2(20 CHAR)
);

DROP SYNONYM TBAGEN.KAS_TOPUP_AUDIT_TBL;

CREATE SYNONYM TBAGEN.KAS_TOPUP_AUDIT_TBL FOR CUSTOM.KAS_TOPUP_AUDIT_TBL;

DROP SYNONYM TBAUTIL.TBA_KAS_TOPUP_AUDIT_TBL;

CREATE SYNONYM TBAUTIL.TBA_KAS_TOPUP_AUDIT_TBL FOR CUSTOM.KAS_TOPUP_AUDIT_TBL;

GRANT SELECT,INSERT,UPDATE ON CUSTOM.KAS_TOPUP_AUDIT_TBL TO PUBLIC;
--Table 7 sequence
DROP SEQUENCE CUSTOM.KAS_TOPUP_AUDIT_SEQ;

CREATE SEQUENCE CUSTOM.KAS_TOPUP_AUDIT_SEQ
  START WITH 81
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;
--End of Table Creation


--Table 8 KaSEOD used for accruals doing Accruals
DROP TABLE CUSTOM.KASEOD CASCADE CONSTRAINTS;

CREATE TABLE CUSTOM.KASEOD
(
  REQUESTID               VARCHAR2(20 BYTE)     NOT NULL,
  ACCOUNTNUMBER           VARCHAR2(16 BYTE),
  CREDITDEBITFLG          VARCHAR2(1 BYTE),
  TRANSACTIONAMOUNT       NUMBER(18,2),
  CURRENCYCODE            VARCHAR2(3 BYTE),
  TRANSACTIONPARTICULARS  VARCHAR2(50 BYTE),
  VALUEDATE               DATE                  DEFAULT sysdate,
  RATE                    NUMBER(18,2)          DEFAULT 1,
  RATECODE                VARCHAR2(10 BYTE)     DEFAULT 'NOR',
  PMTINSTDATE             DATE                  DEFAULT sysdate,
  PMTINSTNUM              VARCHAR2(15 BYTE)     DEFAULT 0,
  PMTINSTTYPE             VARCHAR2(10 BYTE),
  KASLOANNUM              VARCHAR2(50 BYTE),
  TRANPARTCODE            VARCHAR2(20 BYTE),
  RPTCODE                 VARCHAR2(20 BYTE),
  SRLNUM                  VARCHAR2(20 BYTE),
  TRANPARTICULARS2        VARCHAR2(50 BYTE),
  SERIALNUM               VARCHAR2(20 BYTE),
  REFNUM                  VARCHAR2(20 BYTE),
  ENTITYTYPE              VARCHAR2(20 BYTE),
  KASNUMBER               VARCHAR2(50 BYTE),
  TRANSUBTYPE             VARCHAR2(2 BYTE),
  TRAN_ID                 VARCHAR2(1000 BYTE),
  ERRORMESSAGES           VARCHAR2(1000 BYTE),
  REQUESTTIME             DATE                  DEFAULT sysdate,
  RESPONSETIME            DATE                  DEFAULT sysdate,
  RESPONSEERRORMSG        VARCHAR2(1000 BYTE),
  RECORDDATE              DATE                  DEFAULT sysdate,
  FINACLEUSERID           VARCHAR2(30 BYTE),
  PROCESSED_FLG           VARCHAR2(1 BYTE)      DEFAULT 'N',
  NUMBEROFTIME            VARCHAR2(2 BYTE)      DEFAULT 0,
  APPLICATIONID           VARCHAR2(10 BYTE),
  FINACLEUPDATED          VARCHAR2(1 BYTE)      DEFAULT 'N',
  LIEN                    VARCHAR2(1 BYTE)      DEFAULT 'N',
  ID                      NUMBER,
  HALT_FLG                VARCHAR2(1 BYTE)      DEFAULT 'N'
);

CREATE INDEX CUSTOM.PKKASEOD ON CUSTOM.KASEOD
(REQUESTID, REFNUM, PROCESSED_FLG)
LOGGING
TABLESPACE USERS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

DROP SEQUENCE CUSTOM.KASEODSEQ;

CREATE SEQUENCE CUSTOM.KASEODSEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;

CREATE OR REPLACE TRIGGER CUSTOM.kaseodtrigger
 BEFORE INSERT ON CUSTOM.KASEOD  FOR EACH ROW
BEGIN
   Select CUSTOM.KASEODseq.NextVal into :NEW.id from sys.dual;
END kaseodtrigger;
/

GRANT INSERT, REFERENCES, SELECT, UPDATE ON CUSTOM.KASEOD TO PUBLIC;

--*****************Views****************************
DROP VIEW TBAADM.GAM_VIEW;

/* Formatted on 2017/08/07 19:47 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW tbaadm.gam_view (foracid,
                                              clr_bal_amt,
                                              lien_amt,
                                              sanct_lim,
                                              system_reserved_amt
                                             )
AS
   SELECT xx.foracid, NVL (xx.clr_bal_amt, 0) clr_bal_amt,
          NVL (xx.lien_amt, 0) lien_amt,
          CASE
             WHEN NVL (xx.drwng_power, 0) >= NVL (xx.sanct_lim, 0)
                THEN NVL (xx.sanct_lim, 0)
             ELSE NVL (xx.drwng_power, 0)
          END sanct_lim,
          NVL (xx.system_reserved_amt, 0) system_reserved_amt
     FROM tbaadm.gam xx;


GRANT SELECT ON TBAADM.GAM_VIEW TO PUBLIC;

--View for checking LienUpdate staging TABLE
DROP VIEW CUSTOM.KASLIENUPD_VW;

/* Formatted on 2017/11/27 12:16 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW custom.kaslienupd_vw (tran_id,
                                                   lien_id,
                                                   request_id,
                                                   response_code,
                                                   error_message,
                                                   resp_message,
                                                   request_date,
                                                   response_date,
                                                   status
                                                  )
AS
   SELECT REGEXP_SUBSTR (resp_message, '[[:digit:]-]+', 1, 1) AS tran_id,
          REGEXP_SUBSTR (resp_message, '[[:digit:]-]+', 1, 2) AS lien_id,
          k."REQUEST_ID", k."RESPONSE_CODE", k."ERROR_MESSAGE",
          k."RESP_MESSAGE", k."REQUEST_DATE", k."RESPONSE_DATE", k."STATUS"
     FROM custom.kas_lien_update_audit_tbl k;

--View for checking dtd and lien table for request not in staging table
DROP VIEW CUSTOM.KASREPAYMNT_VW;

/* Formatted on 2017/11/27 12:18 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW custom.kasrepaymnt_vw 
    (foracid,
        acid,
        tran_id,
        pstd_flg,
        tran_amt,
        loanno,
        creditdebitflg,
        tran_date,
        value_date,
        lien_id,
        lien_amt,
        lien_remarks
    )
AS
   SELECT foracid, acid, tran_id, pstd_flg, tran_amt, loanno, creditdebitflg,
          tran_date, value_date, lien_id, lien_amt, lien_remarks
     FROM (SELECT *
             FROM (SELECT g.foracid, g.acid, tran_id, pstd_flg, tran_amt,
                          REGEXP_SUBSTR (tran_particular,
                                         '[[:digit:]-]+',
                                         1,
                                         1
                                        ) AS loanno,
                          part_tran_type AS creditdebitflg, tran_date,
                          value_date
                     FROM tbaadm.dtd h, tbaadm.gam g
                    WHERE pstd_flg = 'Y' AND h.del_flg = 'N'
                          AND h.acid = g.acid) a
                  LEFT OUTER JOIN
                  (SELECT acid AS acid1, b2k_id AS lien_id, lien_amt,
                          REGEXP_SUBSTR (lien_remarks,
                                         '[[:digit:]-]+',
                                         1,
                                         1
                                        ) AS lien_remarks
                     FROM tbaadm.alt
                    WHERE lien_reason_code = 'KAS') b
                  ON a.acid = b.acid1 AND loanno = lien_remarks
                  );          

--*************Triggers*********************
1. Trigger to auto update FIVUSR date(Not needed on Production)
CREATE OR REPLACE TRIGGER tbaadm.fivuserdateupdate
  AFTER UPDATE OF db_stat_date ON TBAADM.SOL_GROUP_CONTROL_TABLE
DECLARE
    new_date date;
BEGIN
    SELECT db_stat_date
      INTO new_date 
      FROM TBAADM.SOL_GROUP_CONTROL_TABLE;
    update TBAADM.LOGIN_TABLE set bod_date = new_date,
    dc_bod_date = new_date,
    operating_date = new_date 
    where user_id = 'FIVUSR';
END;
/



