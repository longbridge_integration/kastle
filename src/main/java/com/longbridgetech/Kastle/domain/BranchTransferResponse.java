//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import java.io.Serializable;

public class BranchTransferResponse implements Serializable {

    private String RequestID;
    private String FromSol;
    private String ToSol;
    private String AccountNumber;
    private String errorMessage;
    private String responseCode;
    private String responseMessage;

    public BranchTransferResponse() {

    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public BranchTransferResponse(String RequestID,String AccountNumber,String FromSol, String ToSol,  String errorMessage, String responseCode,String responseMessage) {
        this.RequestID = RequestID;
        this.FromSol = FromSol;
        this.ToSol = ToSol;
        this.AccountNumber = AccountNumber;
        this.errorMessage = errorMessage;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
    }

    public String getRequestID() {
        return RequestID;
    }
    public String getFromSol() {
        return this.FromSol;
    }

    public void setFromSol(String FromSol) {
        this.FromSol = FromSol;
    }

    public String getAccountNumber() {
        return this.AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.AccountNumber = accountNumber;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return this.responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return this.responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getToSol() {
        return this.ToSol;
    }

    public void setToSol(String ToSol) {
        this.ToSol = ToSol;
    }

    public String toString() {
        return "BranchTransferResponse{RequestID='" + this.RequestID + '\''+"FromSol=\'" + this.FromSol + '\'' + ", ToSol=\'" + this.ToSol + '\'' + ", AccountNumber=\'" + this.AccountNumber + '\'' + ", errorMessage=\'" +
                this.errorMessage + '\'' + ", responseCode=\'" + this.responseCode + '\'' + ", responseMessage=\'" + this.responseMessage +  '}';
    }
}
