//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import java.io.Serializable;
import java.util.ArrayList;

public class BulkPaymentResponse implements Serializable {

    private ArrayList<BulkPaymentTran> bulkPaymentTran;
    private String responseMessage;
    private String errorMessage;
    private String responseCode;

    public BulkPaymentResponse() {
    }

    public BulkPaymentResponse(String errorMessage, String responseCode) {
        this.errorMessage = errorMessage;
        this.responseCode = responseCode;
    }

    public ArrayList<BulkPaymentTran> getBulkPaymentTran() {
        return bulkPaymentTran;
    }

    public void setBulkPaymentTran(ArrayList<BulkPaymentTran> bulkPaymentTran) {
        this.bulkPaymentTran = bulkPaymentTran;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        return "BulkPaymentResponse{" +
                "bulkPaymentTran=" + bulkPaymentTran +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", responseCode='" + responseCode + '\'' +
                '}';
    }
}