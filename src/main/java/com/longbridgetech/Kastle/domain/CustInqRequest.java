package com.longbridgetech.Kastle.domain;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by CHRISTIAN on 8/18/2016.
 */
public class CustInqRequest {
    @NotEmpty(message = " is empty. Enter valid value")
    private String RequestID;
    @NotEmpty(message = " is empty. Enter valid value")
    private String CIF_id;
//    @NotEmpty(message = " is empty. Enter valid value")
//    private String appCode;

    public CustInqRequest(){

    }

    public CustInqRequest( String CIF_id) {
        this.CIF_id =CIF_id;
        this.RequestID = RequestID;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public String getCIF_id() {
        return CIF_id;
    }

    public void setCIF_id(String cif_id) {
        CIF_id = cif_id;
    }


    @Override
    public String toString() {
        return "CustInqRequest{" +
                "RequestID='" + RequestID + '\'' +
                ", CIF_id='" + CIF_id + '\'' +
                '}';
    }
}
