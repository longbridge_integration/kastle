package com.longbridgetech.Kastle.domain;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by CHRISTIAN on 8/18/2016.
 */
public class AcctInqRequest {
    @NotEmpty(message = " is empty. Enter valid value")
    private String RequestID;
    @NotEmpty(message = " is empty. Enter valid value")
    private String AccountNumber;
    //@NotEmpty(message = " is empty. Enter valid value")
   // private String appCode;

    public AcctInqRequest(){

    }

    public AcctInqRequest(String AccountNumber) {
        this.AccountNumber = AccountNumber;
      //  this.RequestID = RequestID;
       // this.appCode = appCode;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    @Override
    public String toString() {
        return "AcctInqRequest{" +
                "RequestID='" + RequestID + '\'' +
                ", AccountNumber='" + AccountNumber + '\'' +
                '}';
    }
}
