//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import org.hibernate.validator.constraints.NotEmpty;

public class BulkPaymentRequest {
    @NotEmpty
    private String loanAcctNum;
    @NotEmpty
    private String Amount;
    @NotEmpty
    private String RequestID;

    public BulkPaymentRequest() {

    }

    public BulkPaymentRequest(String loanAcctNum, String amount){
        this.loanAcctNum = loanAcctNum;
        this.Amount = amount;
    }

    public String getLoanAcctNum() {
        return this.loanAcctNum;
    }

    public void setLoanAcctNum(String loanAcctNum) {
        this.loanAcctNum = loanAcctNum;
    }

    public String getAmount() {
        return this.Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }

    public String getRequestID() {
        return this.RequestID;
    }

    public void setRequestID(String RequestID) {
        this.RequestID = RequestID;
    }


    public String toString() {
        return "BulkPaymentRequest{loanAcctNum=\'" + this.loanAcctNum + '\'' + ", Amount=\'" + this.Amount + '\'' + ", RequestID=\'" + this.RequestID + '\'' + ", isSuccessfull=\'" +'}';
    }
}
