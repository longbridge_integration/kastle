package com.longbridgetech.Kastle.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by CHRISTIAN on 8/18/2016.
 */
@XmlRootElement
public class DrwPowerResponse {
    private String responseMessage;
    private String errorMessage;
    private String responseCode;



    public DrwPowerResponse() {
    }

    public DrwPowerResponse(String accountNumber, String responseCode) {
       // this.RequestID = RequestID;
        this.responseCode = responseCode;
    }




    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        return "AcctClosureResponse{" +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", responseCode='" + responseCode + '\'' +
                '}';
    }
}