package com.longbridgetech.Kastle.domain;

import com.longbridgetech.Kastle.domain.tdAcctInq.*;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
@XmlRootElement
public class TDAcctInqResponse {

    private OrigMaturityAmt OrigMaturityAmt;

    private TotalIntAmt TotalIntAmt;

    private InitialDeposit InitialDeposit;

    private TDAcctGenInfo TDAcctGenInfo;

    private CurrDeposit CurrDeposit;

    private String ModeOfOper;

    private DepositTerm DepositTerm;

    private String AccrIntDrCrInd;

    private RenewalDtls RenewalDtls;

    private String FreezeStatusCode;

    private String FreezeReasonCode;

    private String MaturityDt;

    private AcctBalAmt AcctBalAmt;

    private CustId CustId;

    private String AcctOpnDt;

    private NetIntRate NetIntRate;

    private TDAcctId TDAcctId;

    private RepayAcctId RepayAcctId;

    private RelPartyRec RelPartyRec;

    private AccrIntRate AccrIntRate;

    private String AcctBalCrDrInd;

    private String BankAcctStatusCode;

    private String NetIntCrDrInd;

    private MaturityAmt MaturityAmt;

    private String responseMessage;
    private String errorMessage;
    private String responseCode;


    public TDAcctInqResponse() {
    }

    public TDAcctInqResponse( String responseCode) {
        // this.RequestID = RequestID;
       // this.accountNumber = accountNumber;
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public OrigMaturityAmt getOrigMaturityAmt ()
    {
        return OrigMaturityAmt;
    }

    public void setOrigMaturityAmt (OrigMaturityAmt OrigMaturityAmt)
    {
        this.OrigMaturityAmt = OrigMaturityAmt;
    }

    public TotalIntAmt getTotalIntAmt ()
    {
        return TotalIntAmt;
    }

    public void setTotalIntAmt (TotalIntAmt TotalIntAmt)
    {
        this.TotalIntAmt = TotalIntAmt;
    }

    public InitialDeposit getInitialDeposit ()
    {
        return InitialDeposit;
    }

    public void setInitialDeposit (InitialDeposit InitialDeposit)
    {
        this.InitialDeposit = InitialDeposit;
    }

    public TDAcctGenInfo getTDAcctGenInfo ()
    {
        return TDAcctGenInfo;
    }

    public void setTDAcctGenInfo (TDAcctGenInfo TDAcctGenInfo)
    {
        this.TDAcctGenInfo = TDAcctGenInfo;
    }

    public CurrDeposit getCurrDeposit ()
    {
        return CurrDeposit;
    }

    public void setCurrDeposit (CurrDeposit CurrDeposit)
    {
        this.CurrDeposit = CurrDeposit;
    }

    public String getModeOfOper ()
    {
        return ModeOfOper;
    }

    public void setModeOfOper (String ModeOfOper)
    {
        this.ModeOfOper = ModeOfOper;
    }

    public DepositTerm getDepositTerm ()
    {
        return DepositTerm;
    }

    public void setDepositTerm (DepositTerm DepositTerm)
    {
        this.DepositTerm = DepositTerm;
    }

    public String getAccrIntDrCrInd ()
    {
        return AccrIntDrCrInd;
    }

    public void setAccrIntDrCrInd (String AccrIntDrCrInd)
    {
        this.AccrIntDrCrInd = AccrIntDrCrInd;
    }

    public RenewalDtls getRenewalDtls ()
    {
        return RenewalDtls;
    }

    public void setRenewalDtls (RenewalDtls RenewalDtls)
    {
        this.RenewalDtls = RenewalDtls;
    }

    public String getFreezeStatusCode ()
    {
        return FreezeStatusCode;
    }

    public void setFreezeStatusCode (String FreezeStatusCode)
    {
        this.FreezeStatusCode = FreezeStatusCode;
    }

    public String getFreezeReasonCode ()
    {
        return FreezeReasonCode;
    }

    public void setFreezeReasonCode (String FreezeReasonCode)
    {
        this.FreezeReasonCode = FreezeReasonCode;
    }

    public String getMaturityDt ()
    {
        return MaturityDt;
    }

    public void setMaturityDt (String MaturityDt)
    {
        this.MaturityDt = MaturityDt;
    }

    public AcctBalAmt getAcctBalAmt ()
    {
        return AcctBalAmt;
    }

    public void setAcctBalAmt (AcctBalAmt AcctBalAmt)
    {
        this.AcctBalAmt = AcctBalAmt;
    }

    public CustId getCustId ()
    {
        return CustId;
    }

    public void setCustId (CustId CustId)
    {
        this.CustId = CustId;
    }

    public String getAcctOpnDt ()
    {
        return AcctOpnDt;
    }

    public void setAcctOpnDt (String AcctOpnDt)
    {
        this.AcctOpnDt = AcctOpnDt;
    }

    public NetIntRate getNetIntRate ()
    {
        return NetIntRate;
    }

    public void setNetIntRate (NetIntRate NetIntRate)
    {
        this.NetIntRate = NetIntRate;
    }

    public TDAcctId getTDAcctId ()
    {
        return TDAcctId;
    }

    public void setTDAcctId (TDAcctId TDAcctId)
    {
        this.TDAcctId = TDAcctId;
    }

    public RepayAcctId getRepayAcctId ()
    {
        return RepayAcctId;
    }

    public void setRepayAcctId (RepayAcctId RepayAcctId)
    {
        this.RepayAcctId = RepayAcctId;
    }

    public RelPartyRec getRelPartyRec ()
    {
        return RelPartyRec;
    }

    public void setRelPartyRec (RelPartyRec RelPartyRec)
    {
        this.RelPartyRec = RelPartyRec;
    }

    public AccrIntRate getAccrIntRate ()
    {
        return AccrIntRate;
    }

    public void setAccrIntRate (AccrIntRate AccrIntRate)
    {
        this.AccrIntRate = AccrIntRate;
    }

    public String getAcctBalCrDrInd ()
    {
        return AcctBalCrDrInd;
    }

    public void setAcctBalCrDrInd (String AcctBalCrDrInd)
    {
        this.AcctBalCrDrInd = AcctBalCrDrInd;
    }

    public String getBankAcctStatusCode ()
    {
        return BankAcctStatusCode;
    }

    public void setBankAcctStatusCode (String BankAcctStatusCode)
    {
        this.BankAcctStatusCode = BankAcctStatusCode;
    }

    public String getNetIntCrDrInd ()
    {
        return NetIntCrDrInd;
    }

    public void setNetIntCrDrInd (String NetIntCrDrInd)
    {
        this.NetIntCrDrInd = NetIntCrDrInd;
    }

    public MaturityAmt getMaturityAmt ()
    {
        return MaturityAmt;
    }

    public void setMaturityAmt (MaturityAmt MaturityAmt)
    {
        this.MaturityAmt = MaturityAmt;
    }

    @Override
    public String toString() {
        return "TDAcctInqResponse{" +
                "OrigMaturityAmt=" + OrigMaturityAmt +
                ", TotalIntAmt=" + TotalIntAmt +
                ", InitialDeposit=" + InitialDeposit +
                ", TDAcctGenInfo=" + TDAcctGenInfo +
                ", CurrDeposit=" + CurrDeposit +
                ", ModeOfOper='" + ModeOfOper + '\'' +
                ", DepositTerm=" + DepositTerm +
                ", AccrIntDrCrInd='" + AccrIntDrCrInd + '\'' +
                ", RenewalDtls=" + RenewalDtls +
                ", FreezeStatusCode='" + FreezeStatusCode + '\'' +
                ", FreezeReasonCode='" + FreezeReasonCode + '\'' +
                ", MaturityDt='" + MaturityDt + '\'' +
                ", AcctBalAmt=" + AcctBalAmt +
                ", CustInfo=" + CustId +
                ", AcctOpnDt='" + AcctOpnDt + '\'' +
                ", NetIntRate=" + NetIntRate +
                ", TDAcctId=" + TDAcctId +
                ", RepayAcctId=" + RepayAcctId +
                ", RelPartyRec=" + RelPartyRec +
                ", AccrIntRate=" + AccrIntRate +
                ", AcctBalCrDrInd='" + AcctBalCrDrInd + '\'' +
                ", BankAcctStatusCode='" + BankAcctStatusCode + '\'' +
                ", NetIntCrDrInd='" + NetIntCrDrInd + '\'' +
                ", MaturityAmt=" + MaturityAmt +
                '}';
    }
}
