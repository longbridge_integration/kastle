//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.constraints.Size.List;
import java.io.Serializable;

public class BranchTransferRequest implements Serializable {



    @NotEmpty
    private String RequestID;
    @NotEmpty
    private String AccountNumber;
    @NotEmpty
    @Pattern(
            regexp = "(^$|[0-9]{3})",
            message = " field must contain valid SOL ID"
    )
    private String ToSoL;


    public BranchTransferRequest() {

    }

    public BranchTransferRequest(String ToSoL, String AccountNumber,String RequestID) {
        this.ToSoL = ToSoL;
        this.AccountNumber = AccountNumber;
        this.RequestID = RequestID;
        }



    public String getToSoL() {
        return this.ToSoL;
    }

    public String getAccountNumber() {
        return this.AccountNumber;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setAccountNumber(String AccountNumber) {
        this.AccountNumber = AccountNumber;
    }

    public void setToSoL(String ToSoL) {
        this.ToSoL = ToSoL;
    }
    public void setRequestID(String RequestID) {
        this.RequestID = RequestID;
    }



    public String toString() {
        StringBuilder sb = new StringBuilder("BranchClosureRequest{");
        sb.append("RequestID=\'").append(this.RequestID).append('\'');
        sb.append("ToSoL=\'").append(this.ToSoL).append('\'');
        sb.append("AccountNumber=\'").append(this.AccountNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
