package com.longbridgetech.Kastle.domain;

import com.longbridgetech.Kastle.domain.acctInq.AcctBal;
import com.longbridgetech.Kastle.domain.acctInq.AcctInfo;
import com.longbridgetech.Kastle.domain.acctInq.CustInfo;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;
/**
 * Created by CHRISTIAN on 8/18/2016.
 */
@XmlRootElement
public class AcctInqFiResponse {
    //private String RequestID;
    private AcctBal[] AcctBal;
    private String AcctOpenDt;
    private String BankAcctStatusCode;
    private AcctInfo AcctId;
    private CustInfo CustId;
    private String accountNumber;
    private String ODA;
    private String acctStatus;
    private String responseMessage;
    private String errorMessage;
    private String responseCode;



    public AcctInqFiResponse() {
    }

    public AcctInqFiResponse(String accountNumber, String responseCode) {
       // this.RequestID = RequestID;
        this.accountNumber = accountNumber;
        this.responseCode = responseCode;
    }

    public AcctBal[] getAcctBal() {
        return AcctBal;
    }

    public void setAcctBal(AcctBal[] acctBal) {
        AcctBal = acctBal;
    }

    public String getAcctOpenDt() {
        return AcctOpenDt;
    }

    public void setAcctOpenDt(String acctOpenDt) {
        AcctOpenDt = acctOpenDt;
    }

    public String getBankAcctStatusCode() {
        return BankAcctStatusCode;
    }

    public void setBankAcctStatusCode(String bankAcctStatusCode) {
        BankAcctStatusCode = bankAcctStatusCode;
    }

    public AcctInfo getAcctId() {
        return AcctId;
    }

    public void setAcctId(AcctInfo acctId) {
        AcctId = acctId;
    }

    public CustInfo getCustId() {
        return CustId;
    }

    public void setCustId(CustInfo custId) {
        CustId = custId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getODA() {
        return ODA;
    }

    public void setODA(String ODA) {
        this.ODA = ODA;
    }

    public String getAcctStatus() {
        return acctStatus;
    }

    public void setAcctStatus(String acctStatus) {
        this.acctStatus = acctStatus;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        return "AcctInqFiResponse{\n" +
                "AcctBal=" + Arrays.toString(AcctBal) +
                "\n, AcctOpenDt='" + AcctOpenDt + '\'' +
                "\n, BankAcctStatusCode='" + BankAcctStatusCode + '\'' +
                "\n, AcctInfo=" + AcctId +
                "\n, CustInfo=" + CustId +
                "\n, accountNumber='" + accountNumber + '\'' +
                "\n, ODA='" + ODA + '\'' +
                "\n, acctStatus='" + acctStatus + '\'' +
                "\n, responseMessage='" + responseMessage + '\'' +
                "\n, errorMessage='" + errorMessage + '\'' +
                "\n, responseCode='" + responseCode + '\'' +
                '}';
    }
}