package com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionResp;

/**
 * Created by LB-PRJ-020 on 1/4/2018.
 */
public class Body {
    GetLastNTransactionsWithPaginationResponse getLastNTransactionsWithPaginationResponse;

    public GetLastNTransactionsWithPaginationResponse getGetLastNTransactionsWithPaginationResponse() {
        return getLastNTransactionsWithPaginationResponse;
    }

    public void setGetLastNTransactionsWithPaginationResponse(GetLastNTransactionsWithPaginationResponse getLastNTransactionsWithPaginationResponse) {
        this.getLastNTransactionsWithPaginationResponse = getLastNTransactionsWithPaginationResponse;
    }

    @Override
    public String toString() {
        return "Body{" +
                "getLastNTransactionsWithPaginationResponse=" + getLastNTransactionsWithPaginationResponse +
                '}';
    }
}
