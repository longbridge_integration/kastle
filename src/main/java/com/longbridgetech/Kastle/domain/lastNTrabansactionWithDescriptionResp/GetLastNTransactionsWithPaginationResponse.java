
package com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionResp;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaginatedAccountStatement">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="accountBalances">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="acid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="availableBalance">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="branchId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="fFDBalance">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="floatingBalance">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="ledgerBalance">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="userDefinedBalance">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="field125" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="field126" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="field127" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="hasMoreData" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="transactionDetails" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="pstdDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="transactionSummary">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="instrumentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="txnAmt">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="txnDate">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="2017-06-30T00:00:00.000"/>
 *                                             &lt;enumeration value="2017-03-31T00:00:00.000"/>
 *                                             &lt;enumeration value="2016-12-30T00:00:00.000"/>
 *                                             &lt;enumeration value="2016-09-30T00:00:00.000"/>
 *                                             &lt;enumeration value="2016-06-30T00:00:00.000"/>
 *                                             &lt;enumeration value="2016-03-31T00:00:00.000"/>
 *                                             &lt;enumeration value="2016-01-29T00:00:00.000"/>
 *                                             &lt;enumeration value="2016-01-15T00:00:00.000"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="txnDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="txnType">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="C"/>
 *                                             &lt;enumeration value="D"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="txnBalance">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="txnCat">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="TIP"/>
 *                                   &lt;enumeration value="TBI"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="txnId">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="S16300556"/>
 *                                   &lt;enumeration value="S62517194"/>
 *                                   &lt;enumeration value="S15794621"/>
 *                                   &lt;enumeration value="S74005370"/>
 *                                   &lt;enumeration value="S36978369"/>
 *                                   &lt;enumeration value=" S2004023"/>
 *                                   &lt;enumeration value="  S837259"/>
 *                                   &lt;enumeration value="    S1233"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="txnSrlNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="valueDate">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="2017-06-30T00:00:00.000"/>
 *                                   &lt;enumeration value="2017-03-31T00:00:00.000"/>
 *                                   &lt;enumeration value="2016-12-31T00:00:00.000"/>
 *                                   &lt;enumeration value="2016-09-30T00:00:00.000"/>
 *                                   &lt;enumeration value="2016-06-30T00:00:00.000"/>
 *                                   &lt;enumeration value="2016-03-31T00:00:00.000"/>
 *                                   &lt;enumeration value="2016-01-29T00:00:00.000"/>
 *                                   &lt;enumeration value="2016-01-15T00:00:00.000"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="getLastNTransactionsWithPagination_CustomData">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="THB" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RequestUUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RespCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RespMssg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "paginatedAccountStatement",
    "getLastNTransactionsWithPaginationCustomData",
    "requestUUID",
    "respCode",
    "respMssg",
    "errorMessage"
})
@XmlRootElement(name = "getLastNTransactionsWithPaginationResponse")
public class GetLastNTransactionsWithPaginationResponse {

    @XmlElement(name = "PaginatedAccountStatement", required = true)
    protected GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement paginatedAccountStatement;
    @XmlElement(name = "getLastNTransactionsWithPagination_CustomData", required = true)
    protected GetLastNTransactionsWithPaginationResponse.GetLastNTransactionsWithPaginationCustomData getLastNTransactionsWithPaginationCustomData;
    @XmlElement(name = "RequestUUID", required = true)
    protected String requestUUID;
    @XmlElement(name = "RespCode", required = true)
    protected String respCode;
    @XmlElement(name = "RespMssg", required = true)
    protected String respMssg;
    @XmlElement(name = "ErrorMessage", required = true)
    protected String errorMessage;

    /**
     * Gets the value of the paginatedAccountStatement property.
     * 
     * @return
     *     possible object is
     *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement }
     *     
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement getPaginatedAccountStatement() {
        return paginatedAccountStatement;
    }

    /**
     * Sets the value of the paginatedAccountStatement property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement }
     *     
     */
    public void setPaginatedAccountStatement(GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement value) {
        this.paginatedAccountStatement = value;
    }

    /**
     * Gets the value of the getLastNTransactionsWithPaginationCustomData property.
     * 
     * @return
     *     possible object is
     *     {@link GetLastNTransactionsWithPaginationResponse.GetLastNTransactionsWithPaginationCustomData }
     *     
     */
    public GetLastNTransactionsWithPaginationResponse.GetLastNTransactionsWithPaginationCustomData getGetLastNTransactionsWithPaginationCustomData() {
        return getLastNTransactionsWithPaginationCustomData;
    }

    /**
     * Sets the value of the getLastNTransactionsWithPaginationCustomData property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetLastNTransactionsWithPaginationResponse.GetLastNTransactionsWithPaginationCustomData }
     *     
     */
    public void setGetLastNTransactionsWithPaginationCustomData(GetLastNTransactionsWithPaginationResponse.GetLastNTransactionsWithPaginationCustomData value) {
        this.getLastNTransactionsWithPaginationCustomData = value;
    }

    /**
     * Gets the value of the requestUUID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestUUID() {
        return requestUUID;
    }

    /**
     * Sets the value of the requestUUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestUUID(String value) {
        this.requestUUID = value;
    }

    /**
     * Gets the value of the respCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRespCode() {
        return respCode;
    }

    /**
     * Sets the value of the respCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRespCode(String value) {
        this.respCode = value;
    }

    /**
     * Gets the value of the respMssg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRespMssg() {
        return respMssg;
    }

    /**
     * Sets the value of the respMssg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRespMssg(String value) {
        this.respMssg = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="THB" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "thb"
    })
    public static class GetLastNTransactionsWithPaginationCustomData {

        @XmlElement(name = "THB", required = true)
        protected String thb;

        /**
         * Gets the value of the thb property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTHB() {
            return thb;
        }

        /**
         * Sets the value of the thb property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTHB(String value) {
            this.thb = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="accountBalances">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="acid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="availableBalance">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="branchId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="fFDBalance">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="floatingBalance">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="ledgerBalance">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="userDefinedBalance">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="field125" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="field126" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="field127" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="hasMoreData" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="transactionDetails" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="pstdDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="transactionSummary">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="instrumentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="txnAmt">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="txnDate">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="2017-06-30T00:00:00.000"/>
     *                                   &lt;enumeration value="2017-03-31T00:00:00.000"/>
     *                                   &lt;enumeration value="2016-12-30T00:00:00.000"/>
     *                                   &lt;enumeration value="2016-09-30T00:00:00.000"/>
     *                                   &lt;enumeration value="2016-06-30T00:00:00.000"/>
     *                                   &lt;enumeration value="2016-03-31T00:00:00.000"/>
     *                                   &lt;enumeration value="2016-01-29T00:00:00.000"/>
     *                                   &lt;enumeration value="2016-01-15T00:00:00.000"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="txnDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="txnType">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="C"/>
     *                                   &lt;enumeration value="D"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="txnBalance">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="txnCat">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="TIP"/>
     *                         &lt;enumeration value="TBI"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="txnId">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="S16300556"/>
     *                         &lt;enumeration value="S62517194"/>
     *                         &lt;enumeration value="S15794621"/>
     *                         &lt;enumeration value="S74005370"/>
     *                         &lt;enumeration value="S36978369"/>
     *                         &lt;enumeration value=" S2004023"/>
     *                         &lt;enumeration value="  S837259"/>
     *                         &lt;enumeration value="    S1233"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="txnSrlNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="valueDate">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="2017-06-30T00:00:00.000"/>
     *                         &lt;enumeration value="2017-03-31T00:00:00.000"/>
     *                         &lt;enumeration value="2016-12-31T00:00:00.000"/>
     *                         &lt;enumeration value="2016-09-30T00:00:00.000"/>
     *                         &lt;enumeration value="2016-06-30T00:00:00.000"/>
     *                         &lt;enumeration value="2016-03-31T00:00:00.000"/>
     *                         &lt;enumeration value="2016-01-29T00:00:00.000"/>
     *                         &lt;enumeration value="2016-01-15T00:00:00.000"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountBalances",
        "field125",
        "field126",
        "field127",
        "hasMoreData",
        "transactionDetails"
    })
    public static class PaginatedAccountStatement {

        @XmlElement(required = true)
        protected GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances accountBalances;
        @XmlElement(required = true)
        protected String field125;
        @XmlElement(required = true)
        protected String field126;
        @XmlElement(required = true)
        protected String field127;
        @XmlElement(required = true)
        protected String hasMoreData;
        protected List<GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails> transactionDetails;

        /**
         * Gets the value of the accountBalances property.
         * 
         * @return
         *     possible object is
         *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances }
         *     
         */
        public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances getAccountBalances() {
            return accountBalances;
        }

        /**
         * Sets the value of the accountBalances property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances }
         *     
         */
        public void setAccountBalances(GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances value) {
            this.accountBalances = value;
        }

        /**
         * Gets the value of the field125 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField125() {
            return field125;
        }

        /**
         * Sets the value of the field125 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField125(String value) {
            this.field125 = value;
        }

        /**
         * Gets the value of the field126 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField126() {
            return field126;
        }

        /**
         * Sets the value of the field126 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField126(String value) {
            this.field126 = value;
        }

        /**
         * Gets the value of the field127 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getField127() {
            return field127;
        }

        /**
         * Sets the value of the field127 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setField127(String value) {
            this.field127 = value;
        }

        /**
         * Gets the value of the hasMoreData property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHasMoreData() {
            return hasMoreData;
        }

        /**
         * Sets the value of the hasMoreData property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHasMoreData(String value) {
            this.hasMoreData = value;
        }

        /**
         * Gets the value of the transactionDetails property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the transactionDetails property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTransactionDetails().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails }
         * 
         * 
         */
        public List<GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails> getTransactionDetails() {
            if (transactionDetails == null) {
                transactionDetails = new ArrayList<GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails>();
            }
            return this.transactionDetails;
        }

        public  void setTransactionDetails(List<GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails> transactionDetails) {

                this.transactionDetails = transactionDetails;

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="acid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="availableBalance">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="branchId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="fFDBalance">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="floatingBalance">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="ledgerBalance">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="userDefinedBalance">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "acid",
            "availableBalance",
            "branchId",
            "currencyCode",
            "ffdBalance",
            "floatingBalance",
            "ledgerBalance",
            "userDefinedBalance"
        })
        public static class AccountBalances {

            @XmlElement(required = true)
            protected String acid;
            @XmlElement(required = true)
            protected GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.AvailableBalance availableBalance;
            @XmlElement(required = true)
            protected String branchId;
            @XmlElement(required = true)
            protected String currencyCode;
            @XmlElement(name = "fFDBalance", required = true)
            protected GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FFDBalance ffdBalance;
            @XmlElement(required = true)
            protected GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FloatingBalance floatingBalance;
            @XmlElement(required = true)
            protected GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.LedgerBalance ledgerBalance;
            @XmlElement(required = true)
            protected GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.UserDefinedBalance userDefinedBalance;

            /**
             * Gets the value of the acid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcid() {
                return acid;
            }

            /**
             * Sets the value of the acid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcid(String value) {
                this.acid = value;
            }

            /**
             * Gets the value of the availableBalance property.
             * 
             * @return
             *     possible object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.AvailableBalance }
             *     
             */
            public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.AvailableBalance getAvailableBalance() {
                return availableBalance;
            }

            /**
             * Sets the value of the availableBalance property.
             * 
             * @param value
             *     allowed object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.AvailableBalance }
             *     
             */
            public void setAvailableBalance(GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.AvailableBalance value) {
                this.availableBalance = value;
            }

            /**
             * Gets the value of the branchId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBranchId() {
                return branchId;
            }

            /**
             * Sets the value of the branchId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBranchId(String value) {
                this.branchId = value;
            }

            /**
             * Gets the value of the currencyCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrencyCode() {
                return currencyCode;
            }

            /**
             * Sets the value of the currencyCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrencyCode(String value) {
                this.currencyCode = value;
            }

            /**
             * Gets the value of the ffdBalance property.
             * 
             * @return
             *     possible object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FFDBalance }
             *     
             */
            public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FFDBalance getFFDBalance() {
                return ffdBalance;
            }

            /**
             * Sets the value of the ffdBalance property.
             * 
             * @param value
             *     allowed object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FFDBalance }
             *     
             */
            public void setFFDBalance(GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FFDBalance value) {
                this.ffdBalance = value;
            }

            /**
             * Gets the value of the floatingBalance property.
             * 
             * @return
             *     possible object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FloatingBalance }
             *     
             */
            public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FloatingBalance getFloatingBalance() {
                return floatingBalance;
            }

            /**
             * Sets the value of the floatingBalance property.
             * 
             * @param value
             *     allowed object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FloatingBalance }
             *     
             */
            public void setFloatingBalance(GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FloatingBalance value) {
                this.floatingBalance = value;
            }

            /**
             * Gets the value of the ledgerBalance property.
             * 
             * @return
             *     possible object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.LedgerBalance }
             *     
             */
            public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.LedgerBalance getLedgerBalance() {
                return ledgerBalance;
            }

            /**
             * Sets the value of the ledgerBalance property.
             * 
             * @param value
             *     allowed object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.LedgerBalance }
             *     
             */
            public void setLedgerBalance(GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.LedgerBalance value) {
                this.ledgerBalance = value;
            }

            /**
             * Gets the value of the userDefinedBalance property.
             * 
             * @return
             *     possible object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.UserDefinedBalance }
             *     
             */
            public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.UserDefinedBalance getUserDefinedBalance() {
                return userDefinedBalance;
            }

            /**
             * Sets the value of the userDefinedBalance property.
             * 
             * @param value
             *     allowed object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.UserDefinedBalance }
             *     
             */
            public void setUserDefinedBalance(GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.UserDefinedBalance value) {
                this.userDefinedBalance = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amountValue",
                "currencyCode"
            })
            public static class AvailableBalance {

                @XmlElement(required = true)
                protected String amountValue;
                @XmlElement(required = true)
                protected String currencyCode;

                /**
                 * Gets the value of the amountValue property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmountValue() {
                    return amountValue;
                }

                /**
                 * Sets the value of the amountValue property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmountValue(String value) {
                    this.amountValue = value;
                }

                /**
                 * Gets the value of the currencyCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Sets the value of the currencyCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amountValue",
                "currencyCode"
            })
            public static class FFDBalance {

                @XmlElement(required = true)
                protected String amountValue;
                @XmlElement(required = true)
                protected String currencyCode;

                /**
                 * Gets the value of the amountValue property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmountValue() {
                    return amountValue;
                }

                /**
                 * Sets the value of the amountValue property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmountValue(String value) {
                    this.amountValue = value;
                }

                /**
                 * Gets the value of the currencyCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Sets the value of the currencyCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amountValue",
                "currencyCode"
            })
            public static class FloatingBalance {

                @XmlElement(required = true)
                protected String amountValue;
                @XmlElement(required = true)
                protected String currencyCode;

                /**
                 * Gets the value of the amountValue property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmountValue() {
                    return amountValue;
                }

                /**
                 * Sets the value of the amountValue property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmountValue(String value) {
                    this.amountValue = value;
                }

                /**
                 * Gets the value of the currencyCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Sets the value of the currencyCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amountValue",
                "currencyCode"
            })
            public static class LedgerBalance {

                @XmlElement(required = true)
                protected String amountValue;
                @XmlElement(required = true)
                protected String currencyCode;

                /**
                 * Gets the value of the amountValue property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmountValue() {
                    return amountValue;
                }

                /**
                 * Sets the value of the amountValue property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmountValue(String value) {
                    this.amountValue = value;
                }

                /**
                 * Gets the value of the currencyCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Sets the value of the currencyCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amountValue",
                "currencyCode"
            })
            public static class UserDefinedBalance {

                @XmlElement(required = true)
                protected String amountValue;
                @XmlElement(required = true)
                protected String currencyCode;

                /**
                 * Gets the value of the amountValue property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmountValue() {
                    return amountValue;
                }

                /**
                 * Sets the value of the amountValue property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmountValue(String value) {
                    this.amountValue = value;
                }

                /**
                 * Gets the value of the currencyCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Sets the value of the currencyCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="pstdDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="transactionSummary">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="instrumentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="txnAmt">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="txnDate">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="2017-06-30T00:00:00.000"/>
         *                         &lt;enumeration value="2017-03-31T00:00:00.000"/>
         *                         &lt;enumeration value="2016-12-30T00:00:00.000"/>
         *                         &lt;enumeration value="2016-09-30T00:00:00.000"/>
         *                         &lt;enumeration value="2016-06-30T00:00:00.000"/>
         *                         &lt;enumeration value="2016-03-31T00:00:00.000"/>
         *                         &lt;enumeration value="2016-01-29T00:00:00.000"/>
         *                         &lt;enumeration value="2016-01-15T00:00:00.000"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="txnDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="txnType">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="C"/>
         *                         &lt;enumeration value="D"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="txnBalance">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="txnCat">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="TIP"/>
         *               &lt;enumeration value="TBI"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="txnId">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="S16300556"/>
         *               &lt;enumeration value="S62517194"/>
         *               &lt;enumeration value="S15794621"/>
         *               &lt;enumeration value="S74005370"/>
         *               &lt;enumeration value="S36978369"/>
         *               &lt;enumeration value=" S2004023"/>
         *               &lt;enumeration value="  S837259"/>
         *               &lt;enumeration value="    S1233"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="txnSrlNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="valueDate">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="2017-06-30T00:00:00.000"/>
         *               &lt;enumeration value="2017-03-31T00:00:00.000"/>
         *               &lt;enumeration value="2016-12-31T00:00:00.000"/>
         *               &lt;enumeration value="2016-09-30T00:00:00.000"/>
         *               &lt;enumeration value="2016-06-30T00:00:00.000"/>
         *               &lt;enumeration value="2016-03-31T00:00:00.000"/>
         *               &lt;enumeration value="2016-01-29T00:00:00.000"/>
         *               &lt;enumeration value="2016-01-15T00:00:00.000"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pstdDate",
            "transactionSummary",
            "txnBalance",
            "txnCat",
            "txnId",
            "txnSrlNo",
            "valueDate"
        })
        public static class TransactionDetails {

            @XmlElement(required = true)
            protected String pstdDate;
            @XmlElement(required = true)
            protected GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary transactionSummary;
            @XmlElement(required = true)
            protected GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance txnBalance;
            @XmlElement(required = true)
            protected String txnCat;
            @XmlElement(required = true)
            protected String txnId;
            @XmlElement(required = true)
            protected String txnSrlNo;
            @XmlElement(required = true)
            protected String valueDate;

            /**
             * Gets the value of the pstdDate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPstdDate() {
                return pstdDate;
            }

            /**
             * Sets the value of the pstdDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPstdDate(String value) {
                this.pstdDate = value;
            }

            /**
             * Gets the value of the transactionSummary property.
             * 
             * @return
             *     possible object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary }
             *     
             */
            public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary getTransactionSummary() {
                return transactionSummary;
            }

            /**
             * Sets the value of the transactionSummary property.
             * 
             * @param value
             *     allowed object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary }
             *     
             */
            public void setTransactionSummary(GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary value) {
                this.transactionSummary = value;
            }

            /**
             * Gets the value of the txnBalance property.
             * 
             * @return
             *     possible object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance }
             *     
             */
            public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance getTxnBalance() {
                return txnBalance;
            }

            /**
             * Sets the value of the txnBalance property.
             * 
             * @param value
             *     allowed object is
             *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance }
             *     
             */
            public void setTxnBalance(GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance value) {
                this.txnBalance = value;
            }

            /**
             * Gets the value of the txnCat property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTxnCat() {
                return txnCat;
            }

            /**
             * Sets the value of the txnCat property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTxnCat(String value) {
                this.txnCat = value;
            }

            /**
             * Gets the value of the txnId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTxnId() {
                return txnId;
            }

            /**
             * Sets the value of the txnId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTxnId(String value) {
                this.txnId = value;
            }

            /**
             * Gets the value of the txnSrlNo property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTxnSrlNo() {
                return txnSrlNo;
            }

            /**
             * Sets the value of the txnSrlNo property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTxnSrlNo(String value) {
                this.txnSrlNo = value;
            }

            /**
             * Gets the value of the valueDate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValueDate() {
                return valueDate;
            }

            /**
             * Sets the value of the valueDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValueDate(String value) {
                this.valueDate = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="instrumentId" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="txnAmt">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="txnDate">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="2017-06-30T00:00:00.000"/>
             *               &lt;enumeration value="2017-03-31T00:00:00.000"/>
             *               &lt;enumeration value="2016-12-30T00:00:00.000"/>
             *               &lt;enumeration value="2016-09-30T00:00:00.000"/>
             *               &lt;enumeration value="2016-06-30T00:00:00.000"/>
             *               &lt;enumeration value="2016-03-31T00:00:00.000"/>
             *               &lt;enumeration value="2016-01-29T00:00:00.000"/>
             *               &lt;enumeration value="2016-01-15T00:00:00.000"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="txnDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="txnType">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="C"/>
             *               &lt;enumeration value="D"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "instrumentId",
                "txnAmt",
                "txnDate",
                "txnDesc",
                "txnType"
            })
            public static class TransactionSummary {

                @XmlElement(required = true)
                protected String instrumentId;
                @XmlElement(required = true)
                protected GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary.TxnAmt txnAmt;
                @XmlElement(required = true)
                protected String txnDate;
                @XmlElement(required = true)
                protected String txnDesc;
                @XmlElement(required = true)
                protected String txnType;

                /**
                 * Gets the value of the instrumentId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getInstrumentId() {
                    return instrumentId;
                }

                /**
                 * Sets the value of the instrumentId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setInstrumentId(String value) {
                    this.instrumentId = value;
                }

                /**
                 * Gets the value of the txnAmt property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary.TxnAmt }
                 *     
                 */
                public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary.TxnAmt getTxnAmt() {
                    return txnAmt;
                }

                /**
                 * Sets the value of the txnAmt property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary.TxnAmt }
                 *     
                 */
                public void setTxnAmt(GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary.TxnAmt value) {
                    this.txnAmt = value;
                }

                /**
                 * Gets the value of the txnDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTxnDate() {
                    return txnDate;
                }

                /**
                 * Sets the value of the txnDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTxnDate(String value) {
                    this.txnDate = value;
                }

                /**
                 * Gets the value of the txnDesc property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTxnDesc() {
                    return txnDesc;
                }

                /**
                 * Sets the value of the txnDesc property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTxnDesc(String value) {
                    this.txnDesc = value;
                }

                /**
                 * Gets the value of the txnType property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTxnType() {
                    return txnType;
                }

                /**
                 * Sets the value of the txnType property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTxnType(String value) {
                    this.txnType = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "amountValue",
                    "currencyCode"
                })
                public static class TxnAmt {

                    @XmlElement(required = true)
                    protected String amountValue;
                    @XmlElement(required = true)
                    protected String currencyCode;

                    /**
                     * Gets the value of the amountValue property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAmountValue() {
                        return amountValue;
                    }

                    /**
                     * Sets the value of the amountValue property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAmountValue(String value) {
                        this.amountValue = value;
                    }

                    /**
                     * Gets the value of the currencyCode property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                    /**
                     * Sets the value of the currencyCode property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="amountValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amountValue",
                "currencyCode"
            })
            public static class TxnBalance {

                @XmlElement(required = true)
                protected String amountValue;
                @XmlElement(required = true)
                protected String currencyCode;

                /**
                 * Gets the value of the amountValue property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmountValue() {
                    return amountValue;
                }

                /**
                 * Sets the value of the amountValue property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmountValue(String value) {
                    this.amountValue = value;
                }

                /**
                 * Gets the value of the currencyCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Sets the value of the currencyCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

            }

        }

    }

}
