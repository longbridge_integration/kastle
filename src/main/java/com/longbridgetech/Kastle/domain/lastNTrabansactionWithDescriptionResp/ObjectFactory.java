
package com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionResp;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionResp package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionResp
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse createGetLastNTransactionsWithPaginationResponse() {
        return new GetLastNTransactionsWithPaginationResponse();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement createGetLastNTransactionsWithPaginationResponsePaginatedAccountStatement() {
        return new GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails createGetLastNTransactionsWithPaginationResponsePaginatedAccountStatementTransactionDetails() {
        return new GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary createGetLastNTransactionsWithPaginationResponsePaginatedAccountStatementTransactionDetailsTransactionSummary() {
        return new GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances createGetLastNTransactionsWithPaginationResponsePaginatedAccountStatementAccountBalances() {
        return new GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.GetLastNTransactionsWithPaginationCustomData }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.GetLastNTransactionsWithPaginationCustomData createGetLastNTransactionsWithPaginationResponseGetLastNTransactionsWithPaginationCustomData() {
        return new GetLastNTransactionsWithPaginationResponse.GetLastNTransactionsWithPaginationCustomData();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance createGetLastNTransactionsWithPaginationResponsePaginatedAccountStatementTransactionDetailsTxnBalance() {
        return new GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TxnBalance();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary.TxnAmt }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary.TxnAmt createGetLastNTransactionsWithPaginationResponsePaginatedAccountStatementTransactionDetailsTransactionSummaryTxnAmt() {
        return new GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails.TransactionSummary.TxnAmt();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.AvailableBalance }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.AvailableBalance createGetLastNTransactionsWithPaginationResponsePaginatedAccountStatementAccountBalancesAvailableBalance() {
        return new GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.AvailableBalance();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FFDBalance }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FFDBalance createGetLastNTransactionsWithPaginationResponsePaginatedAccountStatementAccountBalancesFFDBalance() {
        return new GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FFDBalance();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FloatingBalance }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FloatingBalance createGetLastNTransactionsWithPaginationResponsePaginatedAccountStatementAccountBalancesFloatingBalance() {
        return new GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.FloatingBalance();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.LedgerBalance }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.LedgerBalance createGetLastNTransactionsWithPaginationResponsePaginatedAccountStatementAccountBalancesLedgerBalance() {
        return new GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.LedgerBalance();
    }

    /**
     * Create an instance of {@link GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.UserDefinedBalance }
     * 
     */
    public GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.UserDefinedBalance createGetLastNTransactionsWithPaginationResponsePaginatedAccountStatementAccountBalancesUserDefinedBalance() {
        return new GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.AccountBalances.UserDefinedBalance();
    }

}
