package com.longbridgetech.Kastle.domain.acctInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class AcctType {
    private String SchmCode;
    private String SchmType;

    public String getSchmCode() {
        return SchmCode;
    }

    public void setSchmCode(String schmCode) {
        SchmCode = schmCode;
    }

    public String getSchmType() {
        return SchmType;
    }

    public void setSchmType(String schmType) {
        SchmType = schmType;
    }

    @Override
    public String toString() {
        return "AcctType{" +
                "SchmCode='" + SchmCode + '\'' +
                ", SchmType='" + SchmType + '\'' +
                '}';
    }
}
