package com.longbridgetech.Kastle.domain.acctInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class PostAddr {
    private String Addr1;
    private String Addr2;
    private String Addr3;
    private String AddrCity;
    private String AddrStateProv;
    private String PostalCode;
    private String Country;
    private String AddrType;

    public String getAddr1() {
        return Addr1;
    }

    public void setAddr1(String addr1) {
        Addr1 = addr1;
    }

    public String getAddr2() {
        return Addr2;
    }

    public void setAddr2(String addr2) {
        Addr2 = addr2;
    }

    public String getAddr3() {
        return Addr3;
    }

    public void setAddr3(String addr3) {
        Addr3 = addr3;
    }

    public String getAddrCity() {
        return AddrCity;
    }

    public void setAddrCity(String addrCity) {
        AddrCity = addrCity;
    }

    public String getAddrStateProv() {
        return AddrStateProv;
    }

    public void setAddrStateProv(String addrStateProv) {
        AddrStateProv = addrStateProv;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getAddrType() {
        return AddrType;
    }

    public void setAddrType(String addrType) {
        AddrType = addrType;
    }

    @Override
    public String toString() {
        return "PostAddr{" +
                "Addr1='" + Addr1 + '\'' +
                ", Addr2='" + Addr2 + '\'' +
                ", Addr3='" + Addr3 + '\'' +
                ", AddrCity='" + AddrCity + '\'' +
                ", AddrStateProv='" + AddrStateProv + '\'' +
                ", PostalCode='" + PostalCode + '\'' +
                ", Country='" + Country + '\'' +
                ", AddrType='" + AddrType + '\'' +
                '}';
    }
}
