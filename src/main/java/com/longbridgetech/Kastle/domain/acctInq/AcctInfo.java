package com.longbridgetech.Kastle.domain.acctInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */

public class AcctInfo {
    private String AcctId;
    private AcctType AcctType;
    private String AcctCurr;
    private String AOC;
    private BankInfo BankInfo;

    public String getAcctId() {
        return AcctId;
    }

    public void setAcctId(String acctId) {
        AcctId = acctId;
    }

    public AcctType getAcctType() {
        return AcctType;
    }

    public void setAcctType(AcctType acctType) {
        this.AcctType = acctType;
    }

    public String getAOC() {
        return AOC;
    }

    public void setAOC(String AOC) {
        this.AOC = AOC;
    }

    public String getAcctCurr() {
        return AcctCurr;
    }

    public void setAcctCurr(String acctCurr) {
        AcctCurr = acctCurr;
    }

    public BankInfo getBankInfo() {
        return BankInfo;
    }

    public void setBankInfo(BankInfo bankInfo) {
        this.BankInfo = bankInfo;
    }

    @Override
    public String toString() {
        return "AcctInfo{" +
                "AcctInfo='" + AcctId + '\'' +
                ", AcctType=" + AcctType +
                ", AcctCurr='" + AcctCurr + '\'' +
                ", AOC='" + AOC + '\'' +
                ", BankInfo=" + BankInfo +
                '}';
    }
}
