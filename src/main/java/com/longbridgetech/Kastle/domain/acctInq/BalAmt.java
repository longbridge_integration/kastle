package com.longbridgetech.Kastle.domain.acctInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class BalAmt {

    private String AmountValue;
    private String CurrencyCode;

    public String getAmountValue() {
        return AmountValue;
    }

    public void setAmountValue(String amountValue) {
        AmountValue = amountValue;
    }

    public String getCurrencyCode() {
        return CurrencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        CurrencyCode = currencyCode;
    }

    @Override
    public String toString() {
        return "BalAmt{" +
                "AmountValue='" + AmountValue + '\'' +
                ", CurrencyCode='" + CurrencyCode + '\'' +
                '}';
    }
}
