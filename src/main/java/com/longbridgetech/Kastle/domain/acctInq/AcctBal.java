package com.longbridgetech.Kastle.domain.acctInq;

/**
 * Created by CHRISTIAN on 8/18/2016.
 */
public class AcctBal {
    private String BalType;
    private BalAmt balAmt;

    public String getBalType() {
        return BalType;
    }

    public void setBalType(String balType) {
        BalType = balType;
    }

    public BalAmt getBalAmt() {
        return balAmt;
    }

    public void setBalAmt(BalAmt balAmt) {
        this.balAmt = balAmt;
    }

    @Override
    public String toString() {
        return "AcctBal{" +
                "BalType='" + BalType + '\'' +
                ", balAmt=" + balAmt +
                '}';
    }
}

