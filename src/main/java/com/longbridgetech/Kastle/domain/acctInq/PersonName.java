package com.longbridgetech.Kastle.domain.acctInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class PersonName {

    private String LastName;
    private String FirstName;
    private String MiddleName;
    private String Name;
    private String TitlePrefix;

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTitlePrefix() {
        return TitlePrefix;
    }

    public void setTitlePrefix(String titlePrefix) {
        TitlePrefix = titlePrefix;
    }

    @Override
    public String toString() {
        return "PersonName{" +
                "LastName='" + LastName + '\'' +
                ", FirstName='" + FirstName + '\'' +
                ", MiddleName='" + MiddleName + '\'' +
                ", Name='" + Name + '\'' +
                ", TitlePrefix='" + TitlePrefix + '\'' +
                '}';
    }
}
