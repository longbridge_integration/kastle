package com.longbridgetech.Kastle.domain.acctInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
//CustInfo
public class CustInfo {
    private String CustId;
    private PersonName personName;
    private String dateOfBirth;

    public String getCustId() {
        return CustId;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setCustId(String custId) {
        CustId = custId;
    }

    public PersonName getPersonName() {
        return personName;
    }

    public void setPersonName(PersonName personName) {
        this.personName = personName;
    }

    @Override
    public String toString() {
        return "AccInfo{" +
                "CustInfo='" + CustId + '\'' +
                ", personName=" + personName +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                '}';
    }
}
