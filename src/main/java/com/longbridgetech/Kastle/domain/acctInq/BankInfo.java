package com.longbridgetech.Kastle.domain.acctInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class BankInfo {
    private String BankId;
    private String BankName;
    private String BranchId;
    private String BranchName;
    private PostAddr postAddr;

    public String getBankId() {
        return BankId;
    }

    public void setBankId(String bankId) {
        BankId = bankId;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBranchId() {
        return BranchId;
    }

    public void setBranchId(String branchId) {
        BranchId = branchId;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public PostAddr getPostAddr() {
        return postAddr;
    }

    public void setPostAddr(PostAddr postAddr) {
        this.postAddr = postAddr;
    }

    @Override
    public String toString() {
        return "BankInfo{" +
                "BankId='" + BankId + '\'' +
                ", BankName='" + BankName + '\'' +
                ", BranchId='" + BranchId + '\'' +
                ", BranchName='" + BranchName + '\'' +
                ", postAddr=" + postAddr +
                '}';
    }
}
