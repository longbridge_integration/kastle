//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.constraints.Size.List;
import java.io.Serializable;

public class AverageBalanceRequest implements Serializable {
    @NotEmpty
    private String RequestID;
    @NotEmpty
    private String AccountNumber;
    @NotEmpty
    private String noOfYears;

    public AverageBalanceRequest(){

    }

    public AverageBalanceRequest(String noOfYears, String AccountNumber,String RequestID) {
        this.noOfYears = noOfYears;
        this.AccountNumber = AccountNumber;
        this.RequestID = RequestID;
        }

    public String getnoOfYears() {
        return this.noOfYears;
    }

    public String getAccountNumber() {
        return this.AccountNumber;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setAccountNumber(String AccountNumber) {
        this.AccountNumber = AccountNumber;
    }

    public void setnoOfYears(String noOfYears) {
        this.noOfYears = noOfYears;
    }
    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("AveraBalanceRequest{");
        sb.append("RequestID=\'").append(this.RequestID).append('\'');
        sb.append("noOfYears=\'").append(this.noOfYears).append('\'');
        sb.append("AccountNumber=\'").append(this.AccountNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
