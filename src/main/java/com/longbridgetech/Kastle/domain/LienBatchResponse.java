package com.longbridgetech.Kastle.domain;

import com.longbridgetech.Kastle.domain.lienUpdate.LienUpdateResponse;

import java.util.ArrayList;

/**
 * Created by LB-PRJ-020 on 7/4/2017.
 */
public class LienBatchResponse {
    private String responseCode;
    private String responseMssg;
    private String errorMssg;
    private ArrayList<LienUpdateResponse> lienUpdateResponses;

    public ArrayList<LienUpdateResponse> getLienUpdateResponses() {
        return lienUpdateResponses;
    }

    public void setLienUpdateResponses(ArrayList<LienUpdateResponse> lienUpdateResponses) {
        this.lienUpdateResponses = lienUpdateResponses;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMssg() {
        return responseMssg;
    }


    public void setResponseMssg(String responseMssg) {
        this.responseMssg = responseMssg;
    }

    public String getErrorMssg() {
        return errorMssg;
    }

    @Override
    public String toString() {
        return "LienBatchResponse{" +
                "responseCode='" + responseCode + '\'' +
                ", responseMssg='" + responseMssg + '\'' +
                ", errorMssg='" + errorMssg + '\'' +
                ", lienUpdateResponses=" + lienUpdateResponses +
                '}';
    }

    public void setErrorMssg(String errorMssg) {
        this.errorMssg = errorMssg;
    }

}
