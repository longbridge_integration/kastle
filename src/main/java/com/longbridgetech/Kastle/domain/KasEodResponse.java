package com.longbridgetech.Kastle.domain;

/**
 * Created by LB-PRJ-020 on 10/2/2017.
 */
public class KasEodResponse {
    private String responseCode;
    private String responseDesc;
    private String inQueue;
    private String notProcessed;
    private String failed;
    private String successFullyPosted;
    private String stalkedProcess;
    private String totalRecord;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public String getInQueue() {
        return inQueue;
    }

    public void setInQueue(String inQueue) {
        this.inQueue = inQueue;
    }

    public String getNotProcessed() {
        return notProcessed;
    }

    public void setNotProcessed(String notProcessed) {
        this.notProcessed = notProcessed;
    }

    public String getFailed() {
        return failed;
    }

    public void setFailed(String failed) {
        this.failed = failed;
    }

    public String getSuccessFullyPosted() {
        return successFullyPosted;
    }

    public void setSuccessFullyPosted(String successFullyPosted) {
        this.successFullyPosted = successFullyPosted;
    }

    public String getStalkedProcess() {
        return stalkedProcess;
    }

    public void setStalkedProcess(String stalkedProcess) {
        this.stalkedProcess = stalkedProcess;
    }

    public String getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(String totalRecord) {
        this.totalRecord = totalRecord;
    }



    @Override
    public String toString() {
        return "KasEodResponse{" +
                "responseCode='" + responseCode + '\'' +
                ", responseDesc='" + responseDesc + '\'' +
                ", inQueue='" + inQueue + '\'' +
                ", notProcessed='" + notProcessed + '\'' +
                ", failed='" + failed + '\'' +
                ", successFullyPosted='" + successFullyPosted + '\'' +
                ", stalkedProcess='" + stalkedProcess + '\'' +
                ", totalRecord='" + totalRecord + '\'' +
                '}';
    }
}
