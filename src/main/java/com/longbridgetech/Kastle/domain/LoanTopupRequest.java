package com.longbridgetech.Kastle.domain;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by CHRISTIAN on 8/18/2016.
 */
public class LoanTopupRequest {
    @NotEmpty(message = " is empty. Enter valid value")
    private String RequestID;
    @NotEmpty(message = " is empty. Enter valid value")
    private String AccountNumber;
    @NotEmpty(message = " is empty. Enter valid value")
    private String topup;
    //@NotEmpty(message = " is empty. Enter valid value")
   // private String appCode;

    public LoanTopupRequest(){

    }

    public LoanTopupRequest(String AccountNumber,String topup) {
        this.AccountNumber = AccountNumber;
        this.topup = topup;
      //  this.RequestID = RequestID;
       // this.appCode = appCode;
    }

    public String getTopup() {
        return topup;
    }

    public void setTopup(String topup) {
        this.topup = topup;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    @Override
    public String toString() {
        return "LoanTopupRequest{" +
                "RequestID='" + RequestID + '\'' +
                ", AccountNumber='" + AccountNumber + '\'' +
                ", topup='" + topup + '\'' +
                '}';
    }
}
