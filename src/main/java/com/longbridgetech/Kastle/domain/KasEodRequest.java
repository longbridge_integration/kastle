package com.longbridgetech.Kastle.domain;

/**
 * Created by LB-PRJ-020 on 10/2/2017.
 */
public class KasEodRequest {
    private String eodOperatn;
    private String ThreadNum;
    private String eodType;
    private String tranNotfrmSolThreads;

    public String getEodOperatn() {
        return eodOperatn;
    }

    public void setEodOperatn(String eodOperatn) {
        this.eodOperatn = eodOperatn;
    }

    public String getThreadNum() {
        return ThreadNum;
    }

    public void setThreadNum(String threadNum) {
        this.ThreadNum = threadNum;
    }

    public String getEodType() {
        return eodType;
    }

    public void setEodType(String eodType) {
        this.eodType = eodType;
    }

    public String getTranNotfrmSolThreads() {
        return tranNotfrmSolThreads;
    }

    public void setTranNotfrmSolThreads(String tranNotfrmSolThreads) {
        this.tranNotfrmSolThreads = tranNotfrmSolThreads;
    }

    @Override
    public String toString() {
        return "KasEodRequest{" +
                "eodOperatn='" + eodOperatn + '\'' +
                ", ThreadNum='" + ThreadNum + '\'' +
                ", eodType='" + eodType + '\'' +
                ", tranNotfrmSolThreads='" + tranNotfrmSolThreads + '\'' +
                '}';
    }
}
