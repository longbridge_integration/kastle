package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class PostAddr {

    private String PostalCode;

    private String StateProv;

    private String Addr3;

    private String Addr1;

    private String Addr2;

    private String Country;

    private String City;

    private String AddrType;

    public String getPostalCode ()
    {
        return PostalCode;
    }

    public void setPostalCode (String PostalCode)
    {
        this.PostalCode = PostalCode;
    }

    public String getStateProv ()
    {
        return StateProv;
    }

    public void setStateProv (String StateProv)
    {
        this.StateProv = StateProv;
    }

    public String getAddr3 ()
    {
        return Addr3;
    }

    public void setAddr3 (String Addr3)
    {
        this.Addr3 = Addr3;
    }

    public String getAddr1 ()
    {
        return Addr1;
    }

    public void setAddr1 (String Addr1)
    {
        this.Addr1 = Addr1;
    }

    public String getAddr2 ()
    {
        return Addr2;
    }

    public void setAddr2 (String Addr2)
    {
        this.Addr2 = Addr2;
    }

    public String getCountry ()
    {
        return Country;
    }

    public void setCountry (String Country)
    {
        this.Country = Country;
    }

    public String getCity ()
    {
        return City;
    }

    public void setCity (String City)
    {
        this.City = City;
    }

    public String getAddrType ()
    {
        return AddrType;
    }

    public void setAddrType (String AddrType)
    {
        this.AddrType = AddrType;
    }

    @Override
    public String toString() {
        return "PostAddr{" +
                "PostalCode='" + PostalCode + '\'' +
                ", StateProv='" + StateProv + '\'' +
                ", Addr3='" + Addr3 + '\'' +
                ", Addr1='" + Addr1 + '\'' +
                ", Addr2='" + Addr2 + '\'' +
                ", Country='" + Country + '\'' +
                ", City='" + City + '\'' +
                ", AddrType='" + AddrType + '\'' +
                '}';
    }
}
