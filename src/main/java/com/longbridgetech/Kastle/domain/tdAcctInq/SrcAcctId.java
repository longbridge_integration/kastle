package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class SrcAcctId {

    private BankInfo BankInfo;

    private String AcctCurr;

    private AcctType AcctType;

    private String AcctId;

    public BankInfo getBankInfo ()
    {
        return BankInfo;
    }

    public void setBankInfo (BankInfo BankInfo)
    {
        this.BankInfo = BankInfo;
    }

    public String getAcctCurr ()
    {
        return AcctCurr;
    }

    public void setAcctCurr (String AcctCurr)
    {
        this.AcctCurr = AcctCurr;
    }

    public AcctType getAcctType ()
    {
        return AcctType;
    }

    public void setAcctType (AcctType AcctType)
    {
        this.AcctType = AcctType;
    }

    public String getAcctId ()
    {
        return AcctId;
    }

    public void setAcctId (String AcctId)
    {
        this.AcctId = AcctId;
    }


    @Override
    public String toString() {
        return "SrcAcctId{" +
                "BankInfo=" + BankInfo +
                ", AcctCurr='" + AcctCurr + '\'' +
                ", AcctType=" + AcctType +
                ", AcctInfo='" + AcctId + '\'' +
                '}';
    }
}
