package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class RelPartyRec {

    private String RelPartyCodeDesc;

    private String RecDelFlg;

    private String RelPartyType;

    private RelPartyContactInfo RelPartyContactInfo;

    private String RelPartyTypeDesc;

    private CustId CustId;

    private String RelPartyCode;

    public String getRelPartyCodeDesc ()
    {
        return RelPartyCodeDesc;
    }

    public void setRelPartyCodeDesc (String RelPartyCodeDesc)
    {
        this.RelPartyCodeDesc = RelPartyCodeDesc;
    }

    public String getRecDelFlg ()
    {
        return RecDelFlg;
    }

    public void setRecDelFlg (String RecDelFlg)
    {
        this.RecDelFlg = RecDelFlg;
    }

    public String getRelPartyType ()
    {
        return RelPartyType;
    }

    public void setRelPartyType (String RelPartyType)
    {
        this.RelPartyType = RelPartyType;
    }

    public RelPartyContactInfo getRelPartyContactInfo ()
    {
        return RelPartyContactInfo;
    }

    public void setRelPartyContactInfo (RelPartyContactInfo RelPartyContactInfo)
    {
        this.RelPartyContactInfo = RelPartyContactInfo;
    }

    public String getRelPartyTypeDesc ()
    {
        return RelPartyTypeDesc;
    }

    public void setRelPartyTypeDesc (String RelPartyTypeDesc)
    {
        this.RelPartyTypeDesc = RelPartyTypeDesc;
    }

    public CustId getCustId ()
    {
        return CustId;
    }

    public void setCustId (CustId CustId)
    {
        this.CustId = CustId;
    }

    public String getRelPartyCode ()
    {
        return RelPartyCode;
    }

    public void setRelPartyCode (String RelPartyCode)
    {
        this.RelPartyCode = RelPartyCode;
    }


    @Override
    public String toString() {
        return "RelPartyRec{" +
                "RelPartyCodeDesc='" + RelPartyCodeDesc + '\'' +
                ", RecDelFlg='" + RecDelFlg + '\'' +
                ", RelPartyType='" + RelPartyType + '\'' +
                ", RelPartyContactInfo=" + RelPartyContactInfo +
                ", RelPartyTypeDesc='" + RelPartyTypeDesc + '\'' +
                ", CustInfo=" + CustId +
                ", RelPartyCode='" + RelPartyCode + '\'' +
                '}';
    }
}
