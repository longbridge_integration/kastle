package com.longbridgetech.Kastle.domain.tdAcctInq;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by CHRISTIAN on 8/18/2016.
 */
@XmlRootElement
public class LoanTopupResponse {
    private String accountNumber;
    private String responseMessage;
    private String errorMessage;
    private String responseCode;



    public LoanTopupResponse() {
    }

    public LoanTopupResponse(String accountNumber, String responseCode) {
        this.accountNumber = accountNumber;
        this.responseCode = responseCode;
    }


    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }


    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        return "AcctClosureResponse{" +
                "accountNumber='" + accountNumber + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", responseCode='" + responseCode + '\'' +
                '}';
    }
}