package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class CustId {

    private PersonName PersonName;

    private String CustId;

    public PersonName getPersonName ()
    {
        return PersonName;
    }

    public void setPersonName (PersonName PersonName)
    {
        this.PersonName = PersonName;
    }

    public String getCustId ()
    {
        return CustId;
    }

    public void setCustId (String CustId)
    {
        this.CustId = CustId;
    }

    @Override
    public String toString() {
        return "CustInfo{" +
                "PersonName=" + PersonName +
                ", CustInfo='" + CustId + '\'' +
                '}';
    }
}
