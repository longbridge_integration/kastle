package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class DepositTerm {

    private String Months;

    private String Days;

    public String getMonths ()
    {
        return Months;
    }

    public void setMonths (String Months)
    {
        this.Months = Months;
    }

    public String getDays ()
    {
        return Days;
    }

    public void setDays (String Days)
    {
        this.Days = Days;
    }

    @Override
    public String toString() {
        return "DepositTerm{" +
                "Months='" + Months + '\'' +
                ", Days='" + Days + '\'' +
                '}';
    }
}
