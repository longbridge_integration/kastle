package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class RenewalDtls {

    private String AutoRenewalflg;

    private String AutoCloseOnMaturityFlg;

    private String RenewalOption;

    private SrcAcctId SrcAcctId;

    private String IntTblCode;

    private RenewalSchm RenewalSchm;

    private GenLedgerSubHead GenLedgerSubHead;

    private String RenewalCurCode;

    private RenewalAmt RenewalAmt;

    private String MaxNumOfRenewalAllwd;

    private RenewalTerm RenewalTerm;

    private RenewalAddnlAmt RenewalAddnlAmt;

    public String getAutoRenewalflg ()
    {
        return AutoRenewalflg;
    }

    public void setAutoRenewalflg (String AutoRenewalflg)
    {
        this.AutoRenewalflg = AutoRenewalflg;
    }

    public String getAutoCloseOnMaturityFlg ()
    {
        return AutoCloseOnMaturityFlg;
    }

    public void setAutoCloseOnMaturityFlg (String AutoCloseOnMaturityFlg)
    {
        this.AutoCloseOnMaturityFlg = AutoCloseOnMaturityFlg;
    }

    public String getRenewalOption ()
    {
        return RenewalOption;
    }

    public void setRenewalOption (String RenewalOption)
    {
        this.RenewalOption = RenewalOption;
    }

    public SrcAcctId getSrcAcctId ()
    {
        return SrcAcctId;
    }

    public void setSrcAcctId (SrcAcctId SrcAcctId)
    {
        this.SrcAcctId = SrcAcctId;
    }

    public String getIntTblCode ()
    {
        return IntTblCode;
    }

    public void setIntTblCode (String IntTblCode)
    {
        this.IntTblCode = IntTblCode;
    }

    public RenewalSchm getRenewalSchm ()
    {
        return RenewalSchm;
    }

    public void setRenewalSchm (RenewalSchm RenewalSchm)
    {
        this.RenewalSchm = RenewalSchm;
    }

    public GenLedgerSubHead getGenLedgerSubHead ()
    {
        return GenLedgerSubHead;
    }

    public void setGenLedgerSubHead (GenLedgerSubHead GenLedgerSubHead)
    {
        this.GenLedgerSubHead = GenLedgerSubHead;
    }

    public String getRenewalCurCode ()
    {
        return RenewalCurCode;
    }

    public void setRenewalCurCode (String RenewalCurCode)
    {
        this.RenewalCurCode = RenewalCurCode;
    }

    public RenewalAmt getRenewalAmt ()
    {
        return RenewalAmt;
    }

    public void setRenewalAmt (RenewalAmt RenewalAmt)
    {
        this.RenewalAmt = RenewalAmt;
    }

    public String getMaxNumOfRenewalAllwd ()
    {
        return MaxNumOfRenewalAllwd;
    }

    public void setMaxNumOfRenewalAllwd (String MaxNumOfRenewalAllwd)
    {
        this.MaxNumOfRenewalAllwd = MaxNumOfRenewalAllwd;
    }

    public RenewalTerm getRenewalTerm ()
    {
        return RenewalTerm;
    }

    public void setRenewalTerm (RenewalTerm RenewalTerm)
    {
        this.RenewalTerm = RenewalTerm;
    }

    public RenewalAddnlAmt getRenewalAddnlAmt ()
    {
        return RenewalAddnlAmt;
    }

    public void setRenewalAddnlAmt (RenewalAddnlAmt RenewalAddnlAmt)
    {
        this.RenewalAddnlAmt = RenewalAddnlAmt;
    }

    @Override
    public String toString() {
        return "RenewalDtls{" +
                "AutoRenewalflg='" + AutoRenewalflg + '\'' +
                ", AutoCloseOnMaturityFlg='" + AutoCloseOnMaturityFlg + '\'' +
                ", RenewalOption='" + RenewalOption + '\'' +
                ", SrcAcctId=" + SrcAcctId +
                ", IntTblCode='" + IntTblCode + '\'' +
                ", RenewalSchm=" + RenewalSchm +
                ", GenLedgerSubHead=" + GenLedgerSubHead +
                ", RenewalCurCode='" + RenewalCurCode + '\'' +
                ", RenewalAmt=" + RenewalAmt +
                ", MaxNumOfRenewalAllwd='" + MaxNumOfRenewalAllwd + '\'' +
                ", RenewalTerm=" + RenewalTerm +
                ", RenewalAddnlAmt=" + RenewalAddnlAmt +
                '}';
    }
}
