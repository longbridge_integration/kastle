package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class PhoneNum {

    private String TelexNum;

    private String FaxNum;

    private String TelephoneNum;

    public String getTelexNum ()
    {
        return TelexNum;
    }

    public void setTelexNum (String TelexNum)
    {
        this.TelexNum = TelexNum;
    }

    public String getFaxNum ()
    {
        return FaxNum;
    }

    public void setFaxNum (String FaxNum)
    {
        this.FaxNum = FaxNum;
    }

    public String getTelephoneNum ()
    {
        return TelephoneNum;
    }

    public void setTelephoneNum (String TelephoneNum)
    {
        this.TelephoneNum = TelephoneNum;
    }

    @Override
    public String toString() {
        return "PhoneNum{" +
                "TelexNum='" + TelexNum + '\'' +
                ", FaxNum='" + FaxNum + '\'' +
                ", TelephoneNum='" + TelephoneNum + '\'' +
                '}';
    }
}
