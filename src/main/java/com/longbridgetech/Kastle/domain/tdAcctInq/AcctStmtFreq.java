package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class AcctStmtFreq {

    private String HolStat;

    private String Type;

    private String WeekNum;

    private String StartDt;

    private String Cal;

    private String WeekDay;

    public String getHolStat ()
    {
        return HolStat;
    }

    public void setHolStat (String HolStat)
    {
        this.HolStat = HolStat;
    }

    public String getType ()
    {
        return Type;
    }

    public void setType (String Type)
    {
        this.Type = Type;
    }

    public String getWeekNum ()
    {
        return WeekNum;
    }

    public void setWeekNum (String WeekNum)
    {
        this.WeekNum = WeekNum;
    }

    public String getStartDt ()
    {
        return StartDt;
    }

    public void setStartDt (String StartDt)
    {
        this.StartDt = StartDt;
    }

    public String getCal ()
    {
        return Cal;
    }

    public void setCal (String Cal)
    {
        this.Cal = Cal;
    }

    public String getWeekDay ()
    {
        return WeekDay;
    }

    public void setWeekDay (String WeekDay)
    {
        this.WeekDay = WeekDay;
    }

    @Override
    public String toString() {
        return "AcctStmtFreq{" +
                "HolStat='" + HolStat + '\'' +
                ", Type='" + Type + '\'' +
                ", WeekNum='" + WeekNum + '\'' +
                ", StartDt='" + StartDt + '\'' +
                ", Cal='" + Cal + '\'' +
                ", WeekDay='" + WeekDay + '\'' +
                '}';
    }
}
