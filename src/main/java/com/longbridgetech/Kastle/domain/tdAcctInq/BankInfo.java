package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class BankInfo {

    private String Name;

    private String BranchName;

    private String BankId;

    private PostAddr PostAddr;

    private String BranchId;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getBranchName ()
    {
        return BranchName;
    }

    public void setBranchName (String BranchName)
    {
        this.BranchName = BranchName;
    }

    public String getBankId ()
    {
        return BankId;
    }

    public void setBankId (String BankId)
    {
        this.BankId = BankId;
    }

    public PostAddr getPostAddr ()
    {
        return PostAddr;
    }

    public void setPostAddr (PostAddr PostAddr)
    {
        this.PostAddr = PostAddr;
    }

    public String getBranchId ()
    {
        return BranchId;
    }

    public void setBranchId (String BranchId)
    {
        this.BranchId = BranchId;
    }

    @Override
    public String toString() {
        return "BankInfo{" +
                "Name='" + Name + '\'' +
                ", BranchName='" + BranchName + '\'' +
                ", BankId='" + BankId + '\'' +
                ", PostAddr=" + PostAddr +
                ", BranchId='" + BranchId + '\'' +
                '}';
    }
}
