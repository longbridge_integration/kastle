package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class TDAcctGenInfo {

    private String AcctShortName;

    private String AcctName;

    private String DrIntMethodInd;

    private String EotEnabled;

    private GenLedgerSubHead GenLedgerSubHead;

    private AcctStmtFreq AcctStmtFreq;

    private String AcctStmtMode;

    private String DespatchMode;

    public String getAcctShortName ()
    {
        return AcctShortName;
    }

    public void setAcctShortName (String AcctShortName)
    {
        this.AcctShortName = AcctShortName;
    }

    public String getAcctName ()
    {
        return AcctName;
    }

    public void setAcctName (String AcctName)
    {
        this.AcctName = AcctName;
    }

    public String getDrIntMethodInd ()
    {
        return DrIntMethodInd;
    }

    public void setDrIntMethodInd (String DrIntMethodInd)
    {
        this.DrIntMethodInd = DrIntMethodInd;
    }

    public String getEotEnabled ()
    {
        return EotEnabled;
    }

    public void setEotEnabled (String EotEnabled)
    {
        this.EotEnabled = EotEnabled;
    }

    public GenLedgerSubHead getGenLedgerSubHead ()
    {
        return GenLedgerSubHead;
    }

    public void setGenLedgerSubHead (GenLedgerSubHead GenLedgerSubHead)
    {
        this.GenLedgerSubHead = GenLedgerSubHead;
    }

    public AcctStmtFreq getAcctStmtFreq ()
    {
        return AcctStmtFreq;
    }

    public void setAcctStmtFreq (AcctStmtFreq AcctStmtFreq)
    {
        this.AcctStmtFreq = AcctStmtFreq;
    }

    public String getAcctStmtMode ()
    {
        return AcctStmtMode;
    }

    public void setAcctStmtMode (String AcctStmtMode)
    {
        this.AcctStmtMode = AcctStmtMode;
    }

    public String getDespatchMode ()
    {
        return DespatchMode;
    }

    public void setDespatchMode (String DespatchMode)
    {
        this.DespatchMode = DespatchMode;
    }

    @Override
    public String toString() {
        return "TDAcctGenInfo{" +
                "AcctShortName='" + AcctShortName + '\'' +
                ", AcctName='" + AcctName + '\'' +
                ", DrIntMethodInd='" + DrIntMethodInd + '\'' +
                ", EotEnabled='" + EotEnabled + '\'' +
                ", GenLedgerSubHead=" + GenLedgerSubHead +
                ", AcctStmtFreq=" + AcctStmtFreq +
                ", AcctStmtMode='" + AcctStmtMode + '\'' +
                ", DespatchMode='" + DespatchMode + '\'' +
                '}';
    }
}
