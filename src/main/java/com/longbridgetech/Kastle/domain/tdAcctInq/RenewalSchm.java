package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class RenewalSchm {


    private String SchmCode;

    private String SchmType;

    public String getSchmCode ()
    {
        return SchmCode;
    }

    public void setSchmCode (String SchmCode)
    {
        this.SchmCode = SchmCode;
    }

    public String getSchmType ()
    {
        return SchmType;
    }

    public void setSchmType (String SchmType)
    {
        this.SchmType = SchmType;
    }


    @Override
    public String toString() {
        return "RenewalSchm{" +
                "SchmCode='" + SchmCode + '\'' +
                ", SchmType='" + SchmType + '\'' +
                '}';
    }
}
