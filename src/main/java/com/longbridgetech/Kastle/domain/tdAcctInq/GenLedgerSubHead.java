package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class GenLedgerSubHead {

    private String GenLedgerSubHeadCode;

    private String CurCode;

    public String getGenLedgerSubHeadCode ()
    {
        return GenLedgerSubHeadCode;
    }

    public void setGenLedgerSubHeadCode (String GenLedgerSubHeadCode)
    {
        this.GenLedgerSubHeadCode = GenLedgerSubHeadCode;
    }

    public String getCurCode ()
    {
        return CurCode;
    }

    public void setCurCode (String CurCode)
    {
        this.CurCode = CurCode;
    }

    @Override
    public String toString() {
        return "GenLedgerSubHead{" +
                "GenLedgerSubHeadCode='" + GenLedgerSubHeadCode + '\'' +
                ", CurCode='" + CurCode + '\'' +
                '}';
    }
}
