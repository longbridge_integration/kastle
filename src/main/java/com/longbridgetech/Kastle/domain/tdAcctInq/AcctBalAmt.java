package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class AcctBalAmt {

    private String amountValue;

    private String currencyCode;

    public String getAmountValue ()
    {
        return amountValue;
    }

    public void setAmountValue (String amountValue)
    {
        this.amountValue = amountValue;
    }

    public String getCurrencyCode ()
    {
        return currencyCode;
    }

    public void setCurrencyCode (String currencyCode)
    {
        this.currencyCode = currencyCode;
    }

    @Override
    public String toString() {
        return "AcctBalAmt{" +
                "amountValue='" + amountValue + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                '}';
    }
}
