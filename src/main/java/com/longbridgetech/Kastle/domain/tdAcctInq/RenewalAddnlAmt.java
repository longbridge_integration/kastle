package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class RenewalAddnlAmt {

    private String amountValue;

    private String currencyCode;

    public String getAmountValue ()
    {
        return amountValue;
    }

    public void setAmountValue (String amountValue)
    {
        this.amountValue = amountValue;
    }

    public String getCurrencyCode ()
    {
        return currencyCode;
    }

    public void setCurrencyCode (String currencyCode)
    {
        this.currencyCode = currencyCode;
    }

    @Override
    public String toString() {
        return "RenewalAddnlAmt{" +
                "amountValue='" + amountValue + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                '}';
    }
}
