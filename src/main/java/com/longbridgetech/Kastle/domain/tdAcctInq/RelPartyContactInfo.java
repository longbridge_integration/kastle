package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class RelPartyContactInfo {

    private String EmailAddr;

    private PostAddr PostAddr;

    private PhoneNum PhoneNum;

    public String getEmailAddr ()
    {
        return EmailAddr;
    }

    public void setEmailAddr (String EmailAddr)
    {
        this.EmailAddr = EmailAddr;
    }

    public PostAddr getPostAddr ()
    {
        return PostAddr;
    }

    public void setPostAddr (PostAddr PostAddr)
    {
        this.PostAddr = PostAddr;
    }

    public PhoneNum getPhoneNum ()
    {
        return PhoneNum;
    }

    public void setPhoneNum (PhoneNum PhoneNum)
    {
        this.PhoneNum = PhoneNum;
    }

    @Override
    public String toString() {
        return "RelPartyContactInfo{" +
                "EmailAddr='" + EmailAddr + '\'' +
                ", PostAddr=" + PostAddr +
                ", PhoneNum=" + PhoneNum +
                '}';
    }
}
