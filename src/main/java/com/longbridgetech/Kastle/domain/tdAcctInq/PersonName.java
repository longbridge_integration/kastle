package com.longbridgetech.Kastle.domain.tdAcctInq;

/**
 * Created by LONGBRIDGE on 11/24/2016.
 */
public class PersonName {

    private String Name;

    private String MiddleName;

    private String TitlePrefix;

    private String FirstName;

    private String LastName;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getMiddleName ()
    {
        return MiddleName;
    }

    public void setMiddleName (String MiddleName)
    {
        this.MiddleName = MiddleName;
    }

    public String getTitlePrefix ()
    {
        return TitlePrefix;
    }

    public void setTitlePrefix (String TitlePrefix)
    {
        this.TitlePrefix = TitlePrefix;
    }

    public String getFirstName ()
    {
        return FirstName;
    }

    public void setFirstName (String FirstName)
    {
        this.FirstName = FirstName;
    }

    public String getLastName ()
    {
        return LastName;
    }

    public void setLastName (String LastName)
    {
        this.LastName = LastName;
    }

    @Override
    public String toString() {
        return "PersonName{" +
                "Name='" + Name + '\'' +
                ", MiddleName='" + MiddleName + '\'' +
                ", TitlePrefix='" + TitlePrefix + '\'' +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                '}';
    }
}
