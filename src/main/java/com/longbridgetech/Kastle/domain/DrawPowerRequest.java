package com.longbridgetech.Kastle.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

/**
 * Created by CHRISTIAN on 8/18/2016.
 */
public class DrawPowerRequest {
    @NotEmpty(message = " is empty. Enter valid value")
    private String RequestID;
    @NotEmpty(message = " is empty. Enter valid value")
    private String foracid;
    @Pattern(
            regexp = "\\d{4}-(\\d{2}|\\d{1})-(\\d{2}|\\d{1})",
            message = "field appDate should be of the format yyyy-mm-dd")
    private String  appDate;
    @NotEmpty(message = " is empty. Enter valid value")
    private String  event;
    @NotEmpty(message = " is empty. Enter valid value")
    private String  drwngPowerIndCode;
    @NotEmpty(message = " is empty. Enter valid value")
    private String  drwngPower;
    @NotEmpty(message = " is empty. Enter valid value")
    private String  remark;

    public DrawPowerRequest(){

    }

    public DrawPowerRequest(String AccountNumber) {
        this.foracid = AccountNumber;
      //  this.RequestID = RequestID;
       // this.appCode = appCode;
    }

    public String getForacid() {
        return foracid;
    }

    public void setForacid(String foracid) {
        this.foracid = foracid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getDrwngPowerIndCode() {
        return drwngPowerIndCode;
    }

    public void setDrwngPowerIndCode(String drwngPowerIndCode) {
        this.drwngPowerIndCode = drwngPowerIndCode;
    }

    public String getDrwngPower() {
        return drwngPower;
    }

    public void setDrwngPower(String drwngPower) {
        this.drwngPower = drwngPower;
    }

    public void setAccountNumber(String accountNumber) {
        foracid = accountNumber;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    @Override
    public String toString() {
        return "DrawPowerRequest{" +
                "RequestID='" + RequestID + '\'' +
                ", foracid='" + foracid + '\'' +
                ", appDate='" + appDate + '\'' +
                ", event='" + event + '\'' +
                ", drwngPowerIndCode='" + drwngPowerIndCode + '\'' +
                ", drwngPower='" + drwngPower + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
