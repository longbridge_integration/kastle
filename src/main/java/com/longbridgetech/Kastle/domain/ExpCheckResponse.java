//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import com.longbridgetech.Kastle.domain.expCheck.ExpChechInfo;

import java.io.Serializable;
import java.util.ArrayList;

public class ExpCheckResponse implements Serializable {

    private String RequestID;
    private String customerID;
    private ArrayList<ExpChechInfo> resultInfos;
    private String responseMessage;
    private String errorMessage;
    private String responseCode;


    public ExpCheckResponse() {
    }
    //For Errors
    public ExpCheckResponse(String RequestID, String customerID) {
        this.RequestID = RequestID;
        this.customerID = customerID;
    }
    public ExpCheckResponse(String RequestID, String customerID, String responseMessage, String errorMessage, String responseCode) {
        this.RequestID = RequestID;
        this.customerID = customerID;
        this.responseMessage = responseMessage;
        this.errorMessage = errorMessage;
        this.responseCode = responseCode;
    }
    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public ArrayList<ExpChechInfo> getResultInfos() {
        return resultInfos;
    }

    public void setResultInfos(ArrayList<ExpChechInfo> resultInfos) {
        this.resultInfos = resultInfos;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        return "ExpCheckResponse_old{" +
                "RequestID='" + RequestID + '\'' +
                ", customerID='" + customerID + '\'' +
                ", resultInfos=" + resultInfos +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", responseCode='" + responseCode + '\'' +
                '}';
    }
}