//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import com.longbridgetech.Kastle.domain.lienUpdate.LienUpdateRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class TransactionAudit {
    private long id;
    private String RequestID;
    private String appCode;
    private String cifID;
    private String AccountNumber;
    private String[] BulkAcctNumber;
    private String[] Amount;
    private String FromSol="";
    private String ToSOL="";
    private String NumOfYears="";
    public String amount="";
    public String Balance="";
    private String ipAddress="";
    private String lienAmt;
    private String unlienAmt;
    private String lienID;
    private String responseCode="";
    private String responseMessage="";
    private String errorMessage="";
    private Date requestDate = new Date();
    private Date responseDate = new Date();
    private String status = "";
    private Logger logger = LoggerFactory.getLogger(TransactionAudit.class);

    public TransactionAudit() {

    }

    public TransactionAudit(ExpCheckRequest requestDetails) {
        this.RequestID = requestDetails.getRequestID();
        this.appCode = "KASEXPCHK";
        this.cifID = requestDetails.getCustomerID();
    }
    public TransactionAudit(CustInqRequest requestDetails) {
        this.RequestID = "KASCIFINQ";
        this.appCode = "KASCIFINQ";
        this.cifID = requestDetails.getCIF_id();
    }

    public TransactionAudit(LienUpdateRequest requestDetails) {
        this.RequestID = "KASLIENUPDT";
        this.appCode = "KASLIENUPDT";
        this.lienAmt = requestDetails.getLienInfo().getFreshLien().getLienAmt();
        this.unlienAmt = requestDetails.getLienInfo().getModifyLien().getUnLienAmt();
        this.lienID = requestDetails.getLienInfo().getModifyLien().getLienID();
    }

    public TransactionAudit(AcctInqRequest requestDetails) {
        this.RequestID ="KASACCTINQ";
        this.appCode = "KASACCTINQ";
        this.AccountNumber = requestDetails.getAccountNumber();
    }
    public TransactionAudit(AverageBalanceRequest requestDetails) {
        this.RequestID = requestDetails.getRequestID();
        this.appCode = "KASBALENQ";
        this.AccountNumber = requestDetails.getAccountNumber();
        this.NumOfYears = requestDetails.getnoOfYears();
        this.Balance = "";
    }

    public TransactionAudit(BranchTransferRequest requestDetails) {
        this.RequestID = requestDetails.getRequestID();
        this.appCode = "KASBRXFER";
        this.AccountNumber = requestDetails.getAccountNumber();
        this.FromSol = "000";
        this.ToSOL = requestDetails.getToSoL();

    }

    public TransactionAudit(BulkPaymentRequest requestDetails) {
        this.RequestID = requestDetails.getRequestID();
//        this.appCode = "KASBULKPAY";
//        this.BulkAcctNumber = requestDetails.getLoanAcctNum();
//        this.Amount = requestDetails.getAmount();

    }



    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCifID() {
        return this.cifID;
    }
    public void setCifID(String  cifID) {
        this.cifID = cifID;
    }


    public String getAppCode() {
        return this.appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getRequestID() {
        return this.RequestID;
    }
    public void setRequestID(String integrifyID) {
        this.RequestID = integrifyID;
    }

    public String getFromSol() {
        return this.FromSol;
    }

    public void setFromSol(String middleName) {
        this.FromSol = middleName;
    }

    public String getToSOL() {
        return this.ToSOL;
    }

    public void setBalance(String Balance) {
        this.Balance = Balance;
    }
    public String getBalance() {
        return this.Balance;
    }

    public void setToSOL(String lastName) {
        this.ToSOL = lastName;
    }

    public String getNumOfYears() {
        return this.NumOfYears;
    }

    public void setNumOfYears(String phoneNumber) {
        this.NumOfYears = phoneNumber;
    }


    public String getAmount() {
        return this.amount;
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAccountNumber() {
        return this.AccountNumber;
    }
    public void setAccountNumber(String AccountNumber) {
        this.AccountNumber = AccountNumber;
    }


    public String getIpAddress() {
        return this.ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getResponseCode() {
        return this.responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return this.responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


    public Date getResponseDate() {
        return this.responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return this.requestDate;
    }
    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public int hashCode() {
        int result = this.RequestID.hashCode();
        result = 31 * result + this.appCode.hashCode();
        result = 31 * result + this.AccountNumber.hashCode();
        result = 31 * result + this.ToSOL.hashCode();
        result = 31 * result + this.NumOfYears.hashCode();
        result = 31 * result + this.Amount.hashCode();
        result = 31 * result + this.ipAddress.hashCode();
        result = 31 * result + (this.responseCode != null ? this.responseCode.hashCode() : 0);
        result = 31 * result + (this.FromSol != null ? this.FromSol.hashCode() : 0);
        result = 31 * result + (this.responseMessage != null ? this.responseMessage.hashCode() : 0);
        result = 31 * result + (this.errorMessage != null ? this.errorMessage.hashCode() : 0);
        result = 31 * result + this.requestDate.hashCode();
        result = 31 * result + (this.responseDate != null ? this.responseDate.hashCode() : 0);
        result = 31 * result + this.status.hashCode();
        return result;
    }
}
