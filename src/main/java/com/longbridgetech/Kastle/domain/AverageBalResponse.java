//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import java.io.Serializable;

public class AverageBalResponse implements Serializable {


    private String RequestID;
    private String accountNumber;
    private String NumOfYears;
    private String responseMessage;
    private String errorMessage;
    private String responseCode;

    public AverageBalResponse() {

    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public AverageBalResponse(String RequestID,String accountNumber, String NumOfYears, String responseMessage, String errorMessage, String responseCode) {
        this.RequestID = RequestID;
        this.accountNumber = accountNumber;
        this.NumOfYears = NumOfYears;
        this.responseMessage = responseMessage;
        this.errorMessage = errorMessage;
        this.responseCode = responseCode;
    }

    public String getRequestID() {
        return RequestID;
    }

    public String getResponseMessage() {
        return this.responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return this.responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String toString() {
        return "AverageBalanceResponse{RequestID=\'" + this.RequestID + '\'' + ", accountNumber=\'" + this.accountNumber + '\'' +  ", NumOfYears=\'" + this.NumOfYears + '\'' + ", responseMessage=\'" + this.responseMessage + '\'' + ", errorMessage=\'" + this.errorMessage + '\'' + ", responseCode=\'" + this.responseCode + '\'' + '}';
    }
}
