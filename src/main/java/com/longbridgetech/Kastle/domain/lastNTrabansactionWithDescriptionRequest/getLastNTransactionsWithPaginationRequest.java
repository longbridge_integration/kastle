package com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionRequest;

public class getLastNTransactionsWithPaginationRequest {
    public String getRequestUUID() {
        return RequestUUID;
    }

    public void setRequestUUID(String requestUUID) {
        RequestUUID = requestUUID;
    }

    private String RequestUUID;
    private String acid;
    private String branchId;
    private String lastNTransactions;

    public String getAcid() {
        return acid;
    }

    public void setAcid(String acid) {
        this.acid = acid;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getLastNTransactions() {
        return lastNTransactions;
    }

    public void setLastNTransactions(String lastNTransactions) {
        this.lastNTransactions = lastNTransactions;
    }

    @Override
    public String toString() {
        return "getLastNTransactionsWithPaginationRequest{" +
                "RequestUUID='" + RequestUUID + '\'' +
                ", acid='" + acid + '\'' +
                ", branchId='" + branchId + '\'' +
                ", lastNTransactions='" + lastNTransactions + '\'' +
                '}';
    }
}