//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import java.io.Serializable;

public class ExpCheckResponse_old implements Serializable {

    private String RequestID;
    private String customerID;
    private String customerloanAccNo;
    private String loanRefCreationDate;
    private String creditLimit;
    private String productCode;
    private String balanceOutstanding;
    private String balanceOutstandingCur;
    private String installmentAmt;
    private String installmentAmtCurrency;
    private String installmentFrequency;
    private String accConduct;
    private String custRelationship;
    private String totalSecurityValAttached;
    private String tenor;
    private String approvedLimit;
    private String expiryDate;
    private String servicingBranch;
    private String responseMessage;
    private String errorMessage;
    private String responseCode;


    public ExpCheckResponse_old() {
    }
    //For Errors
    public ExpCheckResponse_old(String RequestID, String customerID, String responseMessage, String errorMessage, String responseCode) {
        this.RequestID = RequestID;
        this.customerID = customerID;
        this.responseMessage = responseMessage;
        this.errorMessage = errorMessage;
        this.responseCode = responseCode;
    }

    public String getRequestID() {
        return RequestID;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getBalanceOutstanding() {
        return balanceOutstanding;
    }

    public void setBalanceOutstanding(String balanceOutstanding) {
        this.balanceOutstanding = balanceOutstanding;
    }

    public String getBalanceOutstandingCur() {
        return balanceOutstandingCur;
    }

    public void setBalanceOutstandingCur(String balanceOutstandingCur) {
        this.balanceOutstandingCur = balanceOutstandingCur;
    }

    public String getInstallmentAmt() {
        return installmentAmt;
    }

    public void setInstallmentAmt(String installmentAmt) {
        this.installmentAmt = installmentAmt;
    }

    public String getInstallmentAmtCurrency() {
        return installmentAmtCurrency;
    }

    public void setInstallmentAmtCurrency(String installmentAmtCurrency) {
        this.installmentAmtCurrency = installmentAmtCurrency;
    }

    public String getInstallmentFrequency() {
        return installmentFrequency;
    }

    public void setInstallmentFrequency(String installmentFrequency) {
        this.installmentFrequency = installmentFrequency;
    }

    public String getAccConduct() {
        return accConduct;
    }

    public void setAccConduct(String accConduct) {
        this.accConduct = accConduct;
    }

    public String getCustRelationship() {
        return custRelationship;
    }

    public void setCustRelationship(String custRelationship) {
        this.custRelationship = custRelationship;
    }

    public String getTotalSecurityValAttached() {
        return totalSecurityValAttached;
    }

    public void setTotalSecurityValAttached(String totalSecurityValAttached) {
        this.totalSecurityValAttached = totalSecurityValAttached;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getApprovedLimit() {
        return approvedLimit;
    }

    public void setApprovedLimit(String approvedLimit) {
        this.approvedLimit = approvedLimit;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getServicingBranch() {
        return servicingBranch;
    }

    public void setServicingBranch(String servicingBranch) {
        this.servicingBranch = servicingBranch;
    }


    public String getCustomerloanAccNo() {
        return this.customerloanAccNo;
    }

    public void setCustomerloanAccNo(String customerloanAccNo) {
        this.customerloanAccNo = customerloanAccNo;
    }

    public String getResponseMessage() {
        return this.responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return this.responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getLoanRefCreationDate() {
        return this.loanRefCreationDate;
    }

    public void setLoanRefCreationDate(String NumOfYears) {
        this.loanRefCreationDate = NumOfYears;
    }

    public String toString() {
        return "ExpCheckResponse_old{RequestID ='"+this.RequestID+'\''+"customerloanAccNo=\'" + this.customerloanAccNo + '\'' + ", loanRefCreationDate=\'" + this.loanRefCreationDate + '\'' +
                ", productCode=\'" + this.productCode + '\'' + ", balanceOutstanding=\'" + this.balanceOutstanding + '\'' +
                ", balanceOutstandingCur=\'" + this.balanceOutstandingCur + '\'' + ", installmentAmt=\'" + this.installmentAmt + '\'' +
                ", installmentAmtCurrency=\'" + this.installmentAmtCurrency + '\'' + ", installmentFrequency=\'" + this.installmentFrequency + '\'' +
                ", accConduct=\'" + this.accConduct + '\'' + ", custRelationship=\'" + this.custRelationship + '\'' +
                ", totalSecurityValAttached=\'" + this.totalSecurityValAttached + '\'' + ", tenor=\'" + this.tenor + '\'' +
                ", approvedLimit=\'" + this.approvedLimit + '\'' + ", expiryDate=\'" + this.expiryDate + '\'' +
                ", creditLimit=\'" + this.creditLimit + '\'' + ", servicingBranch=\'" + this.servicingBranch + '\'' +
                ", responseMessage=\'" + this.responseMessage + '\'' + ", errorMessage=\'" + this.errorMessage + '\'' + ", responseCode=\'" + this.responseCode + '\'' + '}';
    }
}