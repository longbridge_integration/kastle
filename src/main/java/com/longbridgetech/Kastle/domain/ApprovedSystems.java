//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import java.io.Serializable;
import java.util.Date;

public class ApprovedSystems implements Serializable {
    private long seqNum;
    private String ipAddress;
    private String systemDesc;
    private boolean isApproved = false;
    private Date approvedDate;

    public ApprovedSystems() {
    }

    public ApprovedSystems(String ipAddress, String systemDesc) {
        this.ipAddress = ipAddress;
        this.systemDesc = systemDesc;
    }

    public long getSeqNum() {
        return this.seqNum;
    }

    public void setSeqNum(long seqNum) {
        this.seqNum = seqNum;
    }

    public void setIsApproved(boolean isApproved) {
        this.isApproved = isApproved;
    }
    public boolean getIsApproved() {
        return this.isApproved;
    }


    public String getIpAddress() {
        return this.ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getSystemDesc() {
        return this.systemDesc;
    }

    public void setSystemDesc(String appCode) {
        this.systemDesc = appCode;
    }

    public Date getApprovedDate() {
        return this.approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof ApprovedSystems)) {
            return false;
        } else {
            ApprovedSystems that = (ApprovedSystems) o;
            if (this.seqNum != that.seqNum) {
                return false;
            } else if (!this.systemDesc.equals(that.systemDesc)) {
                return false;
            } else {
                if (this.approvedDate != null) {
                    if (this.approvedDate.equals(that.approvedDate)) {
                        return this.ipAddress.equals(that.ipAddress);
                    }
                } else if (that.approvedDate == null) {
                    return this.ipAddress.equals(that.ipAddress);
                }

                return false;
            }
        }
    }

    public int hashCode() {
        int result = this.ipAddress.hashCode();
        result = 31 * result + this.systemDesc.hashCode();
        return result;
    }

    public String toString() {
        return "ApprovedSystems{seqNum=" + this.seqNum + ", ipAddress=\'" + this.ipAddress + '\'' + ", appCode=\'" + this.systemDesc + '\'' + ", approvedDate=" + this.approvedDate + '}';
    }
}
