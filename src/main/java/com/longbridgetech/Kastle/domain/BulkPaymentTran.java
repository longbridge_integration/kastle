//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import java.io.Serializable;

public class BulkPaymentTran implements Serializable {

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    private String RequestID;
    private String TransactionResult;
    private String responseMessage = "";
    private String errorMessage="";
    private String responseCode ="96";

    public BulkPaymentTran() {

    }

    public BulkPaymentTran(String RequestID, String TransactionResult, String responseMessage, String errorMessage, String responseCode) {

        this.RequestID = RequestID;
        this.TransactionResult = TransactionResult;
        this.responseMessage = responseMessage;
        this.errorMessage = errorMessage;
        this.responseCode = responseCode;
    }

    public String getRequestID() {
        return RequestID;
    }
    public String getTransactionResult() {
        return this.TransactionResult;
    }

    public void setTransactionResult(String transactionResult) {
        this.TransactionResult = transactionResult;
    }

    public String getResponseMessage() {
        return this.responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return this.responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }


    public String toString() {
        return "BulkPaymentResponse{RequestID='"+this.RequestID+'\''+ "TransactionResult=\'" + this.TransactionResult + '\'' + ", responseMessage=\'" + this.responseMessage + '\'' + ", errorMessage=\'" + this.errorMessage + '\'' + ", responseCode=\'" + this.responseCode + '\'' + '}';
    }
}
