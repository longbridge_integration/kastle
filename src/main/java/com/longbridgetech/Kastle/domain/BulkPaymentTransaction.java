//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

public class BulkPaymentTransaction implements Serializable {
    @NotEmpty
    private String RequestID;
    @NotEmpty
    private String AccountNumber;
    @NotEmpty
    private String Amount;

    public BulkPaymentTransaction() {

    }

    public BulkPaymentTransaction(String AccountNumber, String Amount, String RequestID) {
        this.AccountNumber = AccountNumber;
        this.Amount = Amount;
        this.RequestID = RequestID;
    }

    public void setLoanAcctNum(String AccountNumber) {
        this.AccountNumber = AccountNumber;
    }

    public String getLoanAcctNum() {
        return this.AccountNumber;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }

    public String getAmount() {
        return this.Amount;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }





    public String toString() {
        StringBuilder sb = new StringBuilder("PaymentDetails{");
        sb.append(", RequestID=\'").append(this.RequestID).append('\'');
        sb.append(", AccountNumber=\'").append(this.AccountNumber).append('\'');
        sb.append(", Amount=\'").append(this.Amount).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
