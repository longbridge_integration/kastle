package com.longbridgetech.Kastle.domain.expCheck;

/**
 * Created by LONGBRIDGE on 11/3/2016.
 */
public class ExpChechInfo {

    private String foracid;
    private String schmCode;
    private String accountStatus;
    private String accountOpenDate;
    private String outstandingBalance;
    private String ODLimit;
    private String currency;
    private String maturityDate;
    private String accountOfficerCode;
    private String servicingBranch;

    public String getForacid() {
        return foracid;
    }

    public void setForacid(String foracid) {
        this.foracid = foracid;
    }

    public String getSchmCode() {
        return schmCode;
    }

    public void setSchmCode(String schmCode) {
        this.schmCode = schmCode;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getAccountOpenDate() {
        return accountOpenDate;
    }

    public void setAccountOpenDate(String accountOpenDate) {
        this.accountOpenDate = accountOpenDate;
    }

    public String getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(String outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    public String getODLimit() {
        return ODLimit;
    }

    public void setODLimit(String ODLimit) {
        this.ODLimit = ODLimit;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(String maturityDate) {
        this.maturityDate = maturityDate;
    }

    public String getAccountOfficerCode() {
        return accountOfficerCode;
    }

    public void setAccountOfficerCode(String accountOfficerCode) {
        this.accountOfficerCode = accountOfficerCode;
    }

    public String getServicingBranch() {
        return servicingBranch;
    }

    public void setServicingBranch(String servicingBranch) {
        this.servicingBranch = servicingBranch;
    }

    @Override
    public String toString() {
        return "ExpChechInfo{" +
                "foracid='" + foracid + '\'' +
                ", schmCode='" + schmCode + '\'' +
                ", accountStatus='" + accountStatus + '\'' +
                ", accountOpenDate='" + accountOpenDate + '\'' +
                ", outstandingBalance='" + outstandingBalance + '\'' +
                ", ODLimit='" + ODLimit + '\'' +
                ", currency='" + currency + '\'' +
                ", maturityDate='" + maturityDate + '\'' +
                ", accountOfficerCode='" + accountOfficerCode + '\'' +
                ", servicingBranch='" + servicingBranch + '\'' +
                '}';
    }
}
