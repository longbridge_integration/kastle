package com.longbridgetech.Kastle.domain;

import com.longbridgetech.Kastle.domain.acctInq.AcctBal;
import com.longbridgetech.Kastle.domain.acctInq.AcctInfo;
import com.longbridgetech.Kastle.domain.acctInq.CustInfo;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

/**
 * Created by CHRISTIAN on 8/18/2016.
 */
@XmlRootElement
public class AcctInqResponse {
    //private String RequestID;
    private ArrayList<AcctBal> AcctBal;
    private String AcctOpenDt;
    private String BankAcctStatusCode;
    private  AcctInfo AcctInfo;
    private CustInfo CustInfo;
    private String accountNumber;
    private String ODA;
    private String acctStatus;
    private String responseMessage;
    private String errorMessage;
    private String responseCode;



    public AcctInqResponse() {
    }

    public AcctInqResponse(String accountNumber, String responseCode) {
       // this.RequestID = RequestID;
        this.accountNumber = accountNumber;
        this.responseCode = responseCode;
    }

    public ArrayList<AcctBal> getAcctBal() {
        return AcctBal;
    }

    public void setAcctBal(ArrayList<AcctBal> acctBal) {
        AcctBal = acctBal;
    }

    public String getAcctOpenDt() {
        return AcctOpenDt;
    }

    public void setAcctOpenDt(String acctOpenDt) {
        AcctOpenDt = acctOpenDt;
    }

    public String getBankAcctStatusCode() {
        return BankAcctStatusCode;
    }

    public void setBankAcctStatusCode(String bankAcctStatusCode) {
        BankAcctStatusCode = bankAcctStatusCode;
    }

    public com.longbridgetech.Kastle.domain.acctInq.AcctInfo getAcctInfo() {
        return AcctInfo;
    }

    public void setAcctInfo(com.longbridgetech.Kastle.domain.acctInq.AcctInfo acctInfo) {
        AcctInfo = acctInfo;
    }

    public com.longbridgetech.Kastle.domain.acctInq.CustInfo getCustInfo() {
        return CustInfo;
    }

    public void setCustInfo(com.longbridgetech.Kastle.domain.acctInq.CustInfo custInfo) {
        CustInfo = custInfo;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getODA() {
        return ODA;
    }

    public void setODA(String ODA) {
        this.ODA = ODA;
    }

    public String getAcctStatus() {
        return acctStatus;
    }

    public void setAcctStatus(String acctStatus) {
        this.acctStatus = acctStatus;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        return "AcctInqResponse{" +
                "AcctBal=" + AcctBal +
                ", AcctOpenDt='" + AcctOpenDt + '\'' +
                ", BankAcctStatusCode='" + BankAcctStatusCode + '\'' +
                ", AcctInfo=" + AcctInfo +
                ", CustInfo=" + CustInfo +
                ", accountNumber='" + accountNumber + '\'' +
                ", ODA='" + ODA + '\'' +
                ", acctStatus='" + acctStatus + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", responseCode='" + responseCode + '\'' +
                '}';
    }
}