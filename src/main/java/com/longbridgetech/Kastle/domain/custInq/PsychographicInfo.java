package com.longbridgetech.Kastle.domain.custInq;

import java.util.Arrays;

/**
 * Created by LONGBRIDGE on 11/23/2016.
 */
public class PsychographicInfo {

    private CampaignDependencyData[] CampaignDependencyData;

    public CampaignDependencyData[] getCampaignDependencyData() {
        return CampaignDependencyData;
    }

    public void setCampaignDependencyData(CampaignDependencyData[] campaignDependencyData) {
        CampaignDependencyData = campaignDependencyData;
    }

    @Override
    public String toString() {
        return "PsychographicInfo{" +
                "CampaignDependencyData=" + Arrays.toString(CampaignDependencyData) +
                '}';
    }
}
