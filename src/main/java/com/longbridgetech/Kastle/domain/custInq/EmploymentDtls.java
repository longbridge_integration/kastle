package com.longbridgetech.Kastle.domain.custInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class EmploymentDtls {

    private String EmployementStatus;
    private String NameOfEmployer;
    private String JobTitleCode;
    private String JobTitleDesc;
    private String OccupationCode;
    private String OccupationDesc;
    private String PeriodOfEmployment;

    public String getEmployementStatus() {
        return EmployementStatus;
    }

    public void setEmployementStatus(String employementStatus) {
        EmployementStatus = employementStatus;
    }

    public String getNameOfEmployer() {
        return NameOfEmployer;
    }

    public void setNameOfEmployer(String nameOfEmployer) {
        NameOfEmployer = nameOfEmployer;
    }

    public String getJobTitleCode() {
        return JobTitleCode;
    }

    public void setJobTitleCode(String jobTitleCode) {
        JobTitleCode = jobTitleCode;
    }

    public String getJobTitleDesc() {
        return JobTitleDesc;
    }

    public void setJobTitleDesc(String jobTitleDesc) {
        JobTitleDesc = jobTitleDesc;
    }

    public String getOccupationCode() {
        return OccupationCode;
    }

    public void setOccupationCode(String occupationCode) {
        OccupationCode = occupationCode;
    }

    public String getOccupationDesc() {
        return OccupationDesc;
    }

    public void setOccupationDesc(String occupationDesc) {
        OccupationDesc = occupationDesc;
    }

    public String getPeriodOfEmployment() {
        return PeriodOfEmployment;
    }

    public void setPeriodOfEmployment(String periodOfEmployment) {
        PeriodOfEmployment = periodOfEmployment;
    }

    @Override
    public String toString() {
        return "EmploymentDtls{" +
                "EmployementStatus='" + EmployementStatus + '\'' +
                ", NameOfEmployer='" + NameOfEmployer + '\'' +
                ", JobTitleCode='" + JobTitleCode + '\'' +
                ", JobTitleDesc='" + JobTitleDesc + '\'' +
                ", OccupationCode='" + OccupationCode + '\'' +
                ", OccupationDesc='" + OccupationDesc + '\'' +
                ", PeriodOfEmployment='" + PeriodOfEmployment + '\'' +
                '}';
    }
}
