package com.longbridgetech.Kastle.domain.custInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */

public class RetailCustInfo {

    private String  AdditionalName;
    private String  FirstName;
    private String  LastName;
    private String  BirthDt;
    private EmploymentDtls employmentDtls;
    private String Gender;
    private String MaritalStatusCode;
    private String MaritalStatusDesc;
    private String NationalityCode;
    private String NationalityDesc;
    private String OptOutInd;
    private String Race;
    private String ResidingCountryCode;
    private String ResidingCountryDesc;
    private String RaceDesc;

    public String getAdditionalName() {
        return AdditionalName;
    }

    public void setAdditionalName(String additionalName) {
        AdditionalName = additionalName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getBirthDt() {
        return BirthDt;
    }

    public void setBirthDt(String birthDt) {
        BirthDt = birthDt;
    }

    public EmploymentDtls getEmploymentDtls() {
        return employmentDtls;
    }

    public void setEmploymentDtls(EmploymentDtls employmentDtls) {
        this.employmentDtls = employmentDtls;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getMaritalStatusCode() {
        return MaritalStatusCode;
    }

    public void setMaritalStatusCode(String maritalStatusCode) {
        MaritalStatusCode = maritalStatusCode;
    }

    public String getMaritalStatusDesc() {
        return MaritalStatusDesc;
    }

    public void setMaritalStatusDesc(String maritalStatusDesc) {
        MaritalStatusDesc = maritalStatusDesc;
    }

    public String getNationalityCode() {
        return NationalityCode;
    }

    public void setNationalityCode(String nationalityCode) {
        NationalityCode = nationalityCode;
    }

    public String getNationalityDesc() {
        return NationalityDesc;
    }

    public void setNationalityDesc(String nationalityDesc) {
        NationalityDesc = nationalityDesc;
    }

    public String getOptOutInd() {
        return OptOutInd;
    }

    public void setOptOutInd(String optOutInd) {
        OptOutInd = optOutInd;
    }

    public String getRace() {
        return Race;
    }

    public void setRace(String race) {
        Race = race;
    }

    public String getResidingCountryCode() {
        return ResidingCountryCode;
    }

    public void setResidingCountryCode(String residingCountryCode) {
        ResidingCountryCode = residingCountryCode;
    }

    public String getResidingCountryDesc() {
        return ResidingCountryDesc;
    }

    public void setResidingCountryDesc(String residingCountryDesc) {
        ResidingCountryDesc = residingCountryDesc;
    }

    public String getRaceDesc() {
        return RaceDesc;
    }

    public void setRaceDesc(String raceDesc) {
        RaceDesc = raceDesc;
    }

    @Override
    public String toString() {
        return "RetailCustInfo{" +
                "AdditionalName='" + AdditionalName + '\'' +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", BirthDt='" + BirthDt + '\'' +
                ", employmentDtls=" + employmentDtls +
                ", Gender='" + Gender + '\'' +
                ", MaritalStatusCode='" + MaritalStatusCode + '\'' +
                ", MaritalStatusDesc='" + MaritalStatusDesc + '\'' +
                ", NationalityCode='" + NationalityCode + '\'' +
                ", NationalityDesc='" + NationalityDesc + '\'' +
                ", OptOutInd='" + OptOutInd + '\'' +
                ", Race='" + Race + '\'' +
                ", ResidingCountryCode='" + ResidingCountryCode + '\'' +
                ", ResidingCountryDesc='" + ResidingCountryDesc + '\'' +
                ", RaceDesc='" + RaceDesc + '\'' +
                '}';
    }
}
