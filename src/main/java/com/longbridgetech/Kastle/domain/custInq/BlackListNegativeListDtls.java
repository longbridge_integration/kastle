package com.longbridgetech.Kastle.domain.custInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class BlackListNegativeListDtls {

   private String IsBlacklisted;
    private String BlacklistNotes;
    private BlacklistReasonCode BlacklistReasonCode;
    private String IsNegated;
    private String NegationNotes;
    private NegationReasonCode NegationReasonCode;

 public String getIsBlacklisted() {
  return IsBlacklisted;
 }

 public void setIsBlacklisted(String isBlacklisted) {
  IsBlacklisted = isBlacklisted;
 }

 public String getBlacklistNotes() {
  return BlacklistNotes;
 }

 public void setBlacklistNotes(String blacklistNotes) {
  BlacklistNotes = blacklistNotes;
 }

 public BlacklistReasonCode getBlacklistReasonCode() {
  return BlacklistReasonCode;
 }

 public void setBlacklistReasonCode(BlacklistReasonCode blacklistReasonCode) {
  this.BlacklistReasonCode = blacklistReasonCode;
 }

 public String getIsNegated() {
  return IsNegated;
 }

 public void setIsNegated(String isNegated) {
  this.IsNegated = isNegated;
 }

 public String getNegationNotes() {
  return NegationNotes;
 }

 public void setNegationNotes(String negationNotes) {
  NegationNotes = negationNotes;
 }

 public NegationReasonCode getNegationReasonCode() {
  return NegationReasonCode;
 }

 public void setNegationReasonCode(NegationReasonCode negationReasonCode) {
  this.NegationReasonCode = negationReasonCode;
 }


 @Override
 public String toString() {
  return "BlackListNegativeListDtls{" +
          "IsBlacklisted='" + IsBlacklisted + '\'' +
          ", BlacklistNotes='" + BlacklistNotes + '\'' +
          ", BlacklistReasonCode=" + BlacklistReasonCode +
          ", IsNegated='" + IsNegated + '\'' +
          ", NegationNotes='" + NegationNotes + '\'' +
          ", NegationReasonCode=" + NegationReasonCode +
          '}';
 }
}
