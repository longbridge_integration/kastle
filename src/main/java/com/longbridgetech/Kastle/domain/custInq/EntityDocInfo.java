package com.longbridgetech.Kastle.domain.custInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class EntityDocInfo {

    private  String CountryOfIssue;
    private  String DocCode;
    private  String ExpDt;
    private  String IssueDt;
    private  String Rmks;
    private  String TypeCode;
    private  String IsMandatory;
    private  String PlaceOfIssue;
    private  String IsScanReqd;
    private  String UniqueId;

    public String getCountryOfIssue() {
        return CountryOfIssue;
    }

    public void setCountryOfIssue(String countryOfIssue) {
        CountryOfIssue = countryOfIssue;
    }

    public String getDocCode() {
        return DocCode;
    }

    public void setDocCode(String docCode) {
        DocCode = docCode;
    }

    public String getExpDt() {
        return ExpDt;
    }

    public void setExpDt(String expDt) {
        ExpDt = expDt;
    }

    public String getIssueDt() {
        return IssueDt;
    }

    public void setIssueDt(String issueDt) {
        IssueDt = issueDt;
    }

    public String getRmks() {
        return Rmks;
    }

    public void setRmks(String rmks) {
        Rmks = rmks;
    }

    public String getTypeCode() {
        return TypeCode;
    }

    public void setTypeCode(String typeCode) {
        TypeCode = typeCode;
    }

    public String getIsMandatory() {
        return IsMandatory;
    }

    public void setIsMandatory(String isMandatory) {
        IsMandatory = isMandatory;
    }

    public String getPlaceOfIssue() {
        return PlaceOfIssue;
    }

    public void setPlaceOfIssue(String placeOfIssue) {
        PlaceOfIssue = placeOfIssue;
    }

    public String getIsScanReqd() {
        return IsScanReqd;
    }

    public void setIsScanReqd(String isScanReqd) {
        IsScanReqd = isScanReqd;
    }

    public String getUniqueId() {
        return UniqueId;
    }

    public void setUniqueId(String uniqueId) {
        UniqueId = uniqueId;
    }

    @Override
    public String toString() {
        return "EntityDocInfo{" +
                "CountryOfIssue='" + CountryOfIssue + '\'' +
                ", DocCode='" + DocCode + '\'' +
                ", ExpDt='" + ExpDt + '\'' +
                ", IssueDt='" + IssueDt + '\'' +
                ", Rmks='" + Rmks + '\'' +
                ", TypeCode='" + TypeCode + '\'' +
                ", IsMandatory='" + IsMandatory + '\'' +
                ", PlaceOfIssue='" + PlaceOfIssue + '\'' +
                ", IsScanReqd='" + IsScanReqd + '\'' +
                ", UniqueId='" + UniqueId + '\'' +
                '}';
    }
}
