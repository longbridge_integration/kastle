package com.longbridgetech.Kastle.domain.custInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class CorpCustInfo {

    private  String CorpName;
    private  String CitryOfIncorporation;
    private  String LegalEntityType;
    private  String PrincipalNatureOfBusiness;
    private  String PrincipalPlaceOfOperation;
    private  String PrimaryRelMgrId;

    public String getCorpName() {
        return CorpName;
    }

    public void setCorpName(String corpName) {
        CorpName = corpName;
    }

    public String getCitryOfIncorporation() {
        return CitryOfIncorporation;
    }

    public void setCitryOfIncorporation(String citryOfIncorporation) {
        CitryOfIncorporation = citryOfIncorporation;
    }

    public String getLegalEntityType() {
        return LegalEntityType;
    }

    public void setLegalEntityType(String legalEntityType) {
        LegalEntityType = legalEntityType;
    }

    public String getPrincipalNatureOfBusiness() {
        return PrincipalNatureOfBusiness;
    }

    public void setPrincipalNatureOfBusiness(String principalNatureOfBusiness) {
        PrincipalNatureOfBusiness = principalNatureOfBusiness;
    }

    public String getPrincipalPlaceOfOperation() {
        return PrincipalPlaceOfOperation;
    }

    public void setPrincipalPlaceOfOperation(String principalPlaceOfOperation) {
        PrincipalPlaceOfOperation = principalPlaceOfOperation;
    }

    public String getPrimaryRelMgrId() {
        return PrimaryRelMgrId;
    }

    public void setPrimaryRelMgrId(String primaryRelMgrId) {
        PrimaryRelMgrId = primaryRelMgrId;
    }

    @Override
    public String toString() {
        return "CorpCustInfo{" +
                "CorpName='" + CorpName + '\'' +
                ", CitryOfIncorporation='" + CitryOfIncorporation + '\'' +
                ", LegalEntityType='" + LegalEntityType + '\'' +
                ", PrincipalNatureOfBusiness='" + PrincipalNatureOfBusiness + '\'' +
                ", PrincipalPlaceOfOperation='" + PrincipalPlaceOfOperation + '\'' +
                ", PrimaryRelMgrId='" + PrimaryRelMgrId + '\'' +
                '}';
    }
}
