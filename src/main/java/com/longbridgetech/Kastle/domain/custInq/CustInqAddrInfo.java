package com.longbridgetech.Kastle.domain.custInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class CustInqAddrInfo {
    private  String AddrEndDt;
    private  String AddrStartDt;
    private  String AddrType;
    private  String AddrLine1;
    private  String AddrLine2;
    private  String AddrLine3;
    private  String BuildingLevel;
    private  String CityCode;
    private  String Country;
    private  String CountryCode;
    private  String Domicile;
    private  String FreeTextAddr;
    private  String HouseNum;
    private  String LocalityName;
    private  String PremiseName;
    private  String StateCode;
    private  String StreetName;
    private  String StreetNum;
    private  String Suburb;
    private  String Town;
    private  String PostalCode;

    public String getAddrEndDt() {
        return AddrEndDt;
    }

    public void setAddrEndDt(String addrEndDt) {
        AddrEndDt = addrEndDt;
    }

    public String getAddrStartDt() {
        return AddrStartDt;
    }

    public void setAddrStartDt(String addrStartDt) {
        AddrStartDt = addrStartDt;
    }

    public String getAddrType() {
        return AddrType;
    }

    public void setAddrType(String addrType) {
        AddrType = addrType;
    }

    public String getAddrLine1() {
        return AddrLine1;
    }

    public void setAddrLine1(String addrLine1) {
        AddrLine1 = addrLine1;
    }

    public String getAddrLine2() {
        return AddrLine2;
    }

    public void setAddrLine2(String addrLine2) {
        AddrLine2 = addrLine2;
    }

    public String getAddrLine3() {
        return AddrLine3;
    }

    public void setAddrLine3(String addrLine3) {
        AddrLine3 = addrLine3;
    }

    public String getBuildingLevel() {
        return BuildingLevel;
    }

    public void setBuildingLevel(String buildingLevel) {
        BuildingLevel = buildingLevel;
    }

    public String getCityCode() {
        return CityCode;
    }

    public void setCityCode(String cityCode) {
        CityCode = cityCode;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getDomicile() {
        return Domicile;
    }

    public void setDomicile(String domicile) {
        Domicile = domicile;
    }

    public String getFreeTextAddr() {
        return FreeTextAddr;
    }

    public void setFreeTextAddr(String freeTextAddr) {
        FreeTextAddr = freeTextAddr;
    }

    public String getHouseNum() {
        return HouseNum;
    }

    public void setHouseNum(String houseNum) {
        HouseNum = houseNum;
    }

    public String getLocalityName() {
        return LocalityName;
    }

    public void setLocalityName(String localityName) {
        LocalityName = localityName;
    }

    public String getPremiseName() {
        return PremiseName;
    }

    public void setPremiseName(String premiseName) {
        PremiseName = premiseName;
    }

    public String getStateCode() {
        return StateCode;
    }

    public void setStateCode(String stateCode) {
        StateCode = stateCode;
    }

    public String getStreetName() {
        return StreetName;
    }

    public void setStreetName(String streetName) {
        StreetName = streetName;
    }

    public String getStreetNum() {
        return StreetNum;
    }

    public void setStreetNum(String streetNum) {
        StreetNum = streetNum;
    }

    public String getSuburb() {
        return Suburb;
    }

    public void setSuburb(String suburb) {
        Suburb = suburb;
    }

    public String getTown() {
        return Town;
    }

    public void setTown(String town) {
        Town = town;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    @Override
    public String toString() {
        return "CustInqAddrInfo{" +
                "AddrEndDt='" + AddrEndDt + '\'' +
                ", AddrStartDt='" + AddrStartDt + '\'' +
                ", AddrType='" + AddrType + '\'' +
                ", AddrLine1='" + AddrLine1 + '\'' +
                ", AddrLine2='" + AddrLine2 + '\'' +
                ", AddrLine3='" + AddrLine3 + '\'' +
                ", BuildingLevel='" + BuildingLevel + '\'' +
                ", CityCode='" + CityCode + '\'' +
                ", Country='" + Country + '\'' +
                ", CountryCode='" + CountryCode + '\'' +
                ", Domicile='" + Domicile + '\'' +
                ", FreeTextAddr='" + FreeTextAddr + '\'' +
                ", HouseNum='" + HouseNum + '\'' +
                ", LocalityName='" + LocalityName + '\'' +
                ", PremiseName='" + PremiseName + '\'' +
                ", StateCode='" + StateCode + '\'' +
                ", StreetName='" + StreetName + '\'' +
                ", StreetNum='" + StreetNum + '\'' +
                ", Suburb='" + Suburb + '\'' +
                ", Town='" + Town + '\'' +
                ", PostalCode='" + PostalCode + '\'' +
                '}';
    }
}
