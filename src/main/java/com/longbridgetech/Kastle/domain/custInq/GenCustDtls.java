package com.longbridgetech.Kastle.domain.custInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class GenCustDtls {

    private String ChannelId;
    private String ChannelCustId;
    private String CustId;
    private String CustTypeCode;
    private String DefaultAddrType;
    private String DSAId;
    private String EmployeeId;
    private String GroupIdCode;
    private String GSTFlag;
    private String LanguageCode;
    private String NameInNativeLanguage;
    private String PrimaryDocType;
    private String PrimaryRMId;
    private String RatingCode;
    private String SalutationCode;
    private String SecondaryRMId;
    private String SectorCode;
    private String Segment;
    private String SegmentLevel;
    private String SegmentNum;
    private String SegmentType;
    private String ShortName;
    private String SICCode;
    private String IsStaff;
    private String SubSectorCode;
    private String IsSuspended;
    private String TertiaryRMId;
    private String BranchId;

    public String getChannelId() {
        return ChannelId;
    }

    public void setChannelId(String channelId) {
        ChannelId = channelId;
    }

    public String getChannelCustId() {
        return ChannelCustId;
    }

    public void setChannelCustId(String channelCustId) {
        ChannelCustId = channelCustId;
    }

    public String getCustId() {
        return CustId;
    }

    public void setCustId(String custId) {
        CustId = custId;
    }

    public String getCustTypeCode() {
        return CustTypeCode;
    }

    public void setCustTypeCode(String custTypeCode) {
        CustTypeCode = custTypeCode;
    }

    public String getDefaultAddrType() {
        return DefaultAddrType;
    }

    public void setDefaultAddrType(String defaultAddrType) {
        DefaultAddrType = defaultAddrType;
    }

    public String getDSAId() {
        return DSAId;
    }

    public void setDSAId(String DSAId) {
        this.DSAId = DSAId;
    }

    public String getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(String employeeId) {
        EmployeeId = employeeId;
    }

    public String getGroupIdCode() {
        return GroupIdCode;
    }

    public void setGroupIdCode(String groupIdCode) {
        GroupIdCode = groupIdCode;
    }

    public String getGSTFlag() {
        return GSTFlag;
    }

    public void setGSTFlag(String GSTFlag) {
        this.GSTFlag = GSTFlag;
    }

    public String getLanguageCode() {
        return LanguageCode;
    }

    public void setLanguageCode(String languageCode) {
        LanguageCode = languageCode;
    }

    public String getNameInNativeLanguage() {
        return NameInNativeLanguage;
    }

    public void setNameInNativeLanguage(String nameInNativeLanguage) {
        NameInNativeLanguage = nameInNativeLanguage;
    }

    public String getPrimaryDocType() {
        return PrimaryDocType;
    }

    public void setPrimaryDocType(String primaryDocType) {
        PrimaryDocType = primaryDocType;
    }

    public String getPrimaryRMId() {
        return PrimaryRMId;
    }

    public void setPrimaryRMId(String primaryRMId) {
        PrimaryRMId = primaryRMId;
    }

    public String getRatingCode() {
        return RatingCode;
    }

    public void setRatingCode(String ratingCode) {
        RatingCode = ratingCode;
    }

    public String getSalutationCode() {
        return SalutationCode;
    }

    public void setSalutationCode(String salutationCode) {
        SalutationCode = salutationCode;
    }

    public String getSecondaryRMId() {
        return SecondaryRMId;
    }

    public void setSecondaryRMId(String secondaryRMId) {
        SecondaryRMId = secondaryRMId;
    }

    public String getSectorCode() {
        return SectorCode;
    }

    public void setSectorCode(String sectorCode) {
        SectorCode = sectorCode;
    }

    public String getSegment() {
        return Segment;
    }

    public void setSegment(String segment) {
        Segment = segment;
    }

    public String getSegmentLevel() {
        return SegmentLevel;
    }

    public void setSegmentLevel(String segmentLevel) {
        SegmentLevel = segmentLevel;
    }

    public String getSegmentNum() {
        return SegmentNum;
    }

    public void setSegmentNum(String segmentNum) {
        SegmentNum = segmentNum;
    }

    public String getSegmentType() {
        return SegmentType;
    }

    public void setSegmentType(String segmentType) {
        SegmentType = segmentType;
    }

    public String getShortName() {
        return ShortName;
    }

    public void setShortName(String shortName) {
        ShortName = shortName;
    }

    public String getSICCode() {
        return SICCode;
    }

    public void setSICCode(String SICCode) {
        this.SICCode = SICCode;
    }

    public String getIsStaff() {
        return IsStaff;
    }

    public void setIsStaff(String isStaff) {
        IsStaff = isStaff;
    }

    public String getSubSectorCode() {
        return SubSectorCode;
    }

    public void setSubSectorCode(String subSectorCode) {
        SubSectorCode = subSectorCode;
    }

    public String getIsSuspended() {
        return IsSuspended;
    }

    public void setIsSuspended(String isSuspended) {
        IsSuspended = isSuspended;
    }

    public String getTertiaryRMId() {
        return TertiaryRMId;
    }

    public void setTertiaryRMId(String tertiaryRMId) {
        TertiaryRMId = tertiaryRMId;
    }

    public String getBranchId() {
        return BranchId;
    }

    public void setBranchId(String branchId) {
        BranchId = branchId;
    }

    @Override
    public String toString() {
        return "GenCustDtls{" +
                "ChannelId='" + ChannelId + '\'' +
                ", ChannelCustId='" + ChannelCustId + '\'' +
                ", CustInfo='" + CustId + '\'' +
                ", CustTypeCode='" + CustTypeCode + '\'' +
                ", DefaultAddrType='" + DefaultAddrType + '\'' +
                ", DSAId='" + DSAId + '\'' +
                ", EmployeeId='" + EmployeeId + '\'' +
                ", GroupIdCode='" + GroupIdCode + '\'' +
                ", GSTFlag='" + GSTFlag + '\'' +
                ", LanguageCode='" + LanguageCode + '\'' +
                ", NameInNativeLanguage='" + NameInNativeLanguage + '\'' +
                ", PrimaryDocType='" + PrimaryDocType + '\'' +
                ", PrimaryRMId='" + PrimaryRMId + '\'' +
                ", RatingCode='" + RatingCode + '\'' +
                ", SalutationCode='" + SalutationCode + '\'' +
                ", SecondaryRMId='" + SecondaryRMId + '\'' +
                ", SectorCode='" + SectorCode + '\'' +
                ", Segment='" + Segment + '\'' +
                ", SegmentLevel='" + SegmentLevel + '\'' +
                ", SegmentNum='" + SegmentNum + '\'' +
                ", SegmentType='" + SegmentType + '\'' +
                ", ShortName='" + ShortName + '\'' +
                ", SICCode='" + SICCode + '\'' +
                ", IsStaff='" + IsStaff + '\'' +
                ", SubSectorCode='" + SubSectorCode + '\'' +
                ", IsSuspended='" + IsSuspended + '\'' +
                ", TertiaryRMId='" + TertiaryRMId + '\'' +
                ", BranchId='" + BranchId + '\'' +
                '}';
    }
}
