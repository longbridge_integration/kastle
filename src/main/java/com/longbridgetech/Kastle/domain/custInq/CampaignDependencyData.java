package com.longbridgetech.Kastle.domain.custInq;

/**
 * Created by LONGBRIDGE on 11/23/2016.
 */
public class CampaignDependencyData {

    private String ContactFrequency;
    private String NoOfTimesCanContact;
    private String DoNotContactType;
    private String DoNotContact;
    private String NoOfTimeContacted;
    private String OrgID;
    private String CampaignDependencyID;

    public String getContactFrequency() {
        return ContactFrequency;
    }

    public void setContactFrequency(String contactFrequency) {
        ContactFrequency = contactFrequency;
    }

    public String getNoOfTimesCanContact() {
        return NoOfTimesCanContact;
    }

    public void setNoOfTimesCanContact(String noOfTimesCanContact) {
        NoOfTimesCanContact = noOfTimesCanContact;
    }

    public String getDoNotContactType() {
        return DoNotContactType;
    }

    public void setDoNotContactType(String doNotContactType) {
        DoNotContactType = doNotContactType;
    }

    public String getDoNotContact() {
        return DoNotContact;
    }

    public void setDoNotContact(String doNotContact) {
        DoNotContact = doNotContact;
    }

    public String getNoOfTimeContacted() {
        return NoOfTimeContacted;
    }

    public void setNoOfTimeContacted(String noOfTimeContacted) {
        NoOfTimeContacted = noOfTimeContacted;
    }

    public String getOrgID() {
        return OrgID;
    }

    public void setOrgID(String orgID) {
        OrgID = orgID;
    }

    public String getCampaignDependencyID() {
        return CampaignDependencyID;
    }

    public void setCampaignDependencyID(String campaignDependencyID) {
        CampaignDependencyID = campaignDependencyID;
    }

    @Override
    public String toString() {
        return "CampaignDependencyData{" +
                "ContactFrequency='" + ContactFrequency + '\'' +
                ", NoOfTimesCanContact='" + NoOfTimesCanContact + '\'' +
                ", DoNotContactType='" + DoNotContactType + '\'' +
                ", DoNotContact='" + DoNotContact + '\'' +
                ", NoOfTimeContacted='" + NoOfTimeContacted + '\'' +
                ", OrgID='" + OrgID + '\'' +
                ", CampaignDependencyID='" + CampaignDependencyID + '\'' +
                '}';
    }
}
