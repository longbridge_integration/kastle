package com.longbridgetech.Kastle.domain.custInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class CustPhoneEmailInfo {

    private  String Email;
    private  String EmailPalm;
    private  String PhoneEmailType;
    private  String PhoneNum;
    private  String PhoneNumCityCode;
    private  String PhoneNumCountryCode;
    private  String PhoneNumLocalCode;
    private  String PhoneOrEmail;
    private  String PrefFlag;
    private  String WorkExtNum;
    private String StartDt;

    public String getStartDt() {
        return StartDt;
    }

    public void setStartDt(String startDt) {
        StartDt = startDt;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getEmailPalm() {
        return EmailPalm;
    }

    public void setEmailPalm(String emailPalm) {
        EmailPalm = emailPalm;
    }

    public String getPhoneEmailType() {
        return PhoneEmailType;
    }

    public void setPhoneEmailType(String phoneEmailType) {
        PhoneEmailType = phoneEmailType;
    }

    public String getPhoneNum() {
        return PhoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        PhoneNum = phoneNum;
    }

    public String getPhoneNumCityCode() {
        return PhoneNumCityCode;
    }

    public void setPhoneNumCityCode(String phoneNumCityCode) {
        PhoneNumCityCode = phoneNumCityCode;
    }

    public String getPhoneNumCountryCode() {
        return PhoneNumCountryCode;
    }

    public void setPhoneNumCountryCode(String phoneNumCountryCode) {
        PhoneNumCountryCode = phoneNumCountryCode;
    }

    public String getPhoneNumLocalCode() {
        return PhoneNumLocalCode;
    }

    public void setPhoneNumLocalCode(String phoneNumLocalCode) {
        PhoneNumLocalCode = phoneNumLocalCode;
    }

    public String getPhoneOrEmail() {
        return PhoneOrEmail;
    }

    public void setPhoneOrEmail(String phoneOrEmail) {
        PhoneOrEmail = phoneOrEmail;
    }

    public String getPrefFlag() {
        return PrefFlag;
    }

    public void setPrefFlag(String prefFlag) {
        PrefFlag = prefFlag;
    }

    public String getWorkExtNum() {
        return WorkExtNum;
    }

    public void setWorkExtNum(String workExtNum) {
        WorkExtNum = workExtNum;
    }

    @Override
    public String toString() {
        return "CustPhoneEmailInfo{" +
                "Email='" + Email + '\'' +
                ", EmailPalm='" + EmailPalm + '\'' +
                ", PhoneEmailType='" + PhoneEmailType + '\'' +
                ", PhoneNum='" + PhoneNum + '\'' +
                ", PhoneNumCityCode='" + PhoneNumCityCode + '\'' +
                ", PhoneNumCountryCode='" + PhoneNumCountryCode + '\'' +
                ", PhoneNumLocalCode='" + PhoneNumLocalCode + '\'' +
                ", PhoneOrEmail='" + PhoneOrEmail + '\'' +
                ", PrefFlag='" + PrefFlag + '\'' +
                ", WorkExtNum='" + WorkExtNum + '\'' +
                ", StartDt='" + StartDt + '\'' +
                '}';
    }
}
