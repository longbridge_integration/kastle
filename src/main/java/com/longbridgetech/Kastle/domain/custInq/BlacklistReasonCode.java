package com.longbridgetech.Kastle.domain.custInq;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */
public class BlacklistReasonCode {

    private String  ReasonCode;

    public String getReasonCode() {
        return ReasonCode;
    }

    public void setReasonCode(String reasonCode) {
        ReasonCode = reasonCode;
    }

    @Override
    public String toString() {
        return "BlacklistReasonCode{" +
                "ReasonCode='" + ReasonCode + '\'' +
                '}';
    }
}
