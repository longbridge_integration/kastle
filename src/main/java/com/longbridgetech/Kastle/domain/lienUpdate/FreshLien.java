package com.longbridgetech.Kastle.domain.lienUpdate;

import javax.validation.constraints.Pattern;

/**
 * Created by LONGBRIDGE on 11/29/2016.
 */
public class FreshLien {

    private String foracid;
    private String lienAmt;
    private String lienReasonCode;
    private String lienRemark;
    @Pattern(
            regexp = "^$|\\d{4}-\\d{2}-\\d{2}",
            message = "Expects date of format yyyy-MM-dd")
    private String  expiredDate;

    public String getForacid() {
        return foracid;
    }

    public void setForacid(String foracid) {
        this.foracid = foracid;
    }

    public String getLienAmt() {
        return lienAmt;
    }

    public void setLienAmt(String lienAmt) {
        this.lienAmt = lienAmt;
    }

    public String getLienReasonCode() {
        return lienReasonCode;
    }

    public void setLienReasonCode(String lienReasonCode) {
        this.lienReasonCode = lienReasonCode;
    }

    public String getLienRemark() {
        return lienRemark;
    }

    public void setLienRemark(String lienRemark) {
        this.lienRemark = lienRemark;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    @Override
    public String toString() {
        return "FreshLien{" +
                "foracid='" + foracid + '\'' +
                ", lienAmt='" + lienAmt + '\'' +
                ", lienReasonCode='" + lienReasonCode + '\'' +
                ", lienRemark='" + lienRemark + '\'' +
                ", expiredDate='" + expiredDate + '\'' +
                '}';
    }
}
