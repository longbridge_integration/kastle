package com.longbridgetech.Kastle.domain.lienUpdate;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * Created by CHRISTIAN on 9/1/2016.
 */
public class AccInfo implements Serializable {
    private String foracid;
    private String allowFreezeAccnt;
    private String allowFalseDebit;
    private TrnAmt trnAmt;
    private PartitionDetails partitionDetails;
    private String trnParticulars;
    private String tranParticularCode;

    @Pattern(
            regexp = "(?i)^$|^[CD]$",
            message = "Invalid, Enter valid value (C or D)")
    private String creditDebitFlg;
    @Pattern(
            regexp = "^$|\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}",
            message = "Expects date of format yyyy-MM-ddTHH:mm:ss.SSS")
    private String valueDt;
    private String rate;

    public String getForacid() {
        return foracid;
    }

    public void setForacid(String foracid) {
        this.foracid = foracid;
    }

    public TrnAmt getTrnAmt() {
        return trnAmt;
    }

    public void setTrnAmt(TrnAmt trnAmt) {
        this.trnAmt = trnAmt;
    }

    public String getTrnParticulars() {
        return trnParticulars;
    }

    public void setTrnParticulars(String trnParticulars) {
        this.trnParticulars = trnParticulars;
    }

    public String getValueDt() {
        return valueDt;
    }

    public void setValueDt(String valueDt) {
        this.valueDt = valueDt;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getCreditDebitFlg() {
        return creditDebitFlg;
    }

    public void setCreditDebitFlg(String creditDebitFlg) {
        this.creditDebitFlg = creditDebitFlg;
    }

    public PartitionDetails getPartitionDetails() {
        return partitionDetails;
    }

    public void setPartitionDetails(PartitionDetails partitionDetails) {
        this.partitionDetails = partitionDetails;
    }

    public String getTranParticularCode() {
        return tranParticularCode;
    }

    public void setTranParticularCode(String tranParticularCode) {
        this.tranParticularCode = tranParticularCode;
    }

    public String getAllowFreezeAccnt() {
        return allowFreezeAccnt;
    }

    public void setAllowFreezeAccnt(String allowFreezeAccnt) {
        this.allowFreezeAccnt = allowFreezeAccnt;
    }

    public String getAllowFalseDebit() {
        return allowFalseDebit;
    }

    public void setAllowFalseDebit(String allowFalseDebit) {
        this.allowFalseDebit = allowFalseDebit;
    }

    @Override
    public String toString() {
        return "AccInfo{" +
                "foracid='" + foracid + '\'' +
                ", allowFreezeAccnt=" + allowFreezeAccnt +
                ", allowFalseDebit=" + allowFalseDebit +
                ", trnAmt=" + trnAmt +
                ", partitionDetails=" + partitionDetails +
                ", trnParticulars='" + trnParticulars + '\'' +
                ", tranParticularCode='" + tranParticularCode + '\'' +
                ", creditDebitFlg='" + creditDebitFlg + '\'' +
                ", valueDt='" + valueDt + '\'' +
                ", rate='" + rate + '\'' +
                '}';
    }

}
