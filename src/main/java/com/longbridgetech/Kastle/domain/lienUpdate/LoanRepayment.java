package com.longbridgetech.Kastle.domain.lienUpdate;

/**
 * Created by LONGBRIDGE on 11/29/2016.
 */
public class LoanRepayment {


    private String loanAcctNum;
    private TrnAmt trnAmt;
    private String OpAcctNum;


    public String getLoanAcctNum() {
        return loanAcctNum;
    }

    public void setLoanAcctNum(String loanAcctNum) {
        this.loanAcctNum = loanAcctNum;
    }

    public TrnAmt getTrnAmt() {
        return trnAmt;
    }

    public void setTrnAmt(TrnAmt trnAmt) {
        this.trnAmt = trnAmt;
    }

    public String getOpAcctNum() {
        return OpAcctNum;
    }

    public void setOpAcctNum(String opAcctNum) {
        OpAcctNum = opAcctNum;
    }

    @Override
    public String toString() {
        return "LoanRepayment{" +
                "loanAcctNum='" + loanAcctNum + '\'' +
                ", trnAmt=" + trnAmt +
                ", OpAcctNum='" + OpAcctNum + '\'' +
                '}';
    }
}
