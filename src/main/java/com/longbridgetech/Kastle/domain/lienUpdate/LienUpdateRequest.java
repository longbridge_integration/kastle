//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain.lienUpdate;

;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.ArrayList;

public class LienUpdateRequest implements Serializable {

    @NotEmpty
    private String requestId;
    @Valid
    private LienInfo lienInfo;
    @Valid
    private ArrayList<AccInfo> accInfo;
    //private  LoanRepayment loanRepayment;

    public LienUpdateRequest() {

    }

    public LienUpdateRequest(ArrayList<AccInfo> accInfo) {
       // RequestID = requestID;
       // this.appCode = appCode;
        this.accInfo = accInfo;
    }

    public LienUpdateRequest(LienInfo lienInfo, ArrayList<AccInfo> accInfo) {
        this.lienInfo = lienInfo;
        this.accInfo = accInfo;
    }

    public LienInfo getLienInfo() {
        return lienInfo;
    }

    public void setLienInfo(LienInfo lienInfo) {
        this.lienInfo = lienInfo;
    }

    public ArrayList<AccInfo> getAccInfo() {
        return accInfo;
    }

    public void setAccInfo(ArrayList<AccInfo> accInfo) {
        this.accInfo = accInfo;
    }

    public String getRequestId() {
        return requestId;
    }

    @Override
    public String toString() {
        return "LienUpdateRequest{" +
                "requestId='" + requestId + '\'' +
                ", lienInfo=" + lienInfo +
                ", accInfo=" + accInfo +
                '}';
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

//    public LoanRepayment getLoanRepayment() {
//        return loanRepayment;
//    }
//
//    public void setLoanRepayment(LoanRepayment loanRepayment) {
//        this.loanRepayment = loanRepayment;
//    }


}