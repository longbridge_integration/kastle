//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain.lienUpdate;

import java.io.Serializable;

public class LienUpdateResponse implements Serializable {


   // private String RequestID;
    private String LienADDResult;
    private String LienModResult;
    private String AcountingResult;
    private String LoanRepayResult;
    private String LienADDErrMsg;
    private String LienModErrMsg;
    private String AcountingErrMsg;
    private String LoanRepayErrMsg;
    private String errorMessage;
    private String responseCode;
    private String responseMessage;
    private String requestId;

    public LienUpdateResponse() {

    }



    public String getLienADDErrMsg() {
        return LienADDErrMsg;
    }

    public void setLienADDErrMsg(String lienADDErrMsg) {
        LienADDErrMsg = lienADDErrMsg;
    }

    public String getLienModErrMsg() {
        return LienModErrMsg;
    }

    public void setLienModErrMsg(String lienModErrMsg) {
        LienModErrMsg = lienModErrMsg;
    }

    public String getAcountingErrMsg() {
        return AcountingErrMsg;
    }

    public void setAcountingErrMsg(String acountingErrMsg) {
        AcountingErrMsg = acountingErrMsg;
    }

    public String getLoanRepayErrMsg() {
        return LoanRepayErrMsg;
    }

    public void setLoanRepayErrMsg(String loanRepayErrMsg) {
        LoanRepayErrMsg = loanRepayErrMsg;
    }

    public String getLienADDResult() {
        return LienADDResult;
    }

    public void setLienADDResult(String lienADDResult) {
        LienADDResult = lienADDResult;
    }

    public String getLienModResult() {
        return LienModResult;
    }

    public void setLienModResult(String lienModResult) {
        LienModResult = lienModResult;
    }

    public String getAcountingResult() {
        return AcountingResult;
    }

    public void setAcountingResult(String acountingResult) {
        AcountingResult = acountingResult;
    }

    public String getLoanRepayResult() {
        return LoanRepayResult;
    }

    public void setLoanRepayResult(String loanRepayResult) {
        LoanRepayResult = loanRepayResult;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return this.responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    @Override
    public String toString() {
        return "LienUpdateResponse{" +
                "LienADDResult='" + LienADDResult + '\'' +
                ", LienModResult='" + LienModResult + '\'' +
                ", AcountingResult='" + AcountingResult + '\'' +
                ", LoanRepayResult='" + LoanRepayResult + '\'' +
                ", LienADDErrMsg='" + LienADDErrMsg + '\'' +
                ", LienModErrMsg='" + LienModErrMsg + '\'' +
                ", AcountingErrMsg='" + AcountingErrMsg + '\'' +
                ", LoanRepayErrMsg='" + LoanRepayErrMsg + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", requestId='" + requestId + '\'' +
                '}';
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

}
