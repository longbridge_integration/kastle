package com.longbridgetech.Kastle.domain.lienUpdate;

import javax.validation.Valid;

/**
 * Created by LONGBRIDGE on 11/29/2016.
 */
public class LienInfo {

    @Valid
    private FreshLien freshLien;
    @Valid
    private ModifyLien modifyLien;

    public FreshLien getFreshLien() {
        return freshLien;
    }

    public void setFreshLien(FreshLien freshLien) {
        this.freshLien = freshLien;

    }

    public ModifyLien getModifyLien() {
        return modifyLien;
    }

    public void setModifyLien(ModifyLien modifyLien) {
        this.modifyLien = modifyLien;
    }

    @Override
    public String toString() {
        return "LienInfo{" +
                "freshLien=" + freshLien +
                ", modifyLien=" + modifyLien +
                '}';
    }
}
