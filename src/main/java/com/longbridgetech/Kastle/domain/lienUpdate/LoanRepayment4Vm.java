package com.longbridgetech.Kastle.domain.lienUpdate;

/**
 * Created by LONGBRIDGE on 11/29/2016.
 */
public class LoanRepayment4Vm {


    private String loanAcctNum;
    private String amountValue;
    private String currencyCode;
    private String sn;
    private String OPAcctNum;
    private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getLoanAcctNum() {
        return loanAcctNum;
    }

    public void setLoanAcctNum(String loanAcctNum) {
        this.loanAcctNum = loanAcctNum;
    }

    public String getAmountValue() {
        return amountValue;
    }

    public void setAmountValue(String amountValue) {
        this.amountValue = amountValue;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

      public String getOPAcctNum() {
        return OPAcctNum;
    }

    public void setOPAcctNum(String OPAcctNum) {
        this.OPAcctNum = OPAcctNum;
    }

    @Override
    public String toString() {
        return "LoanRepayment4Vm{" +
                "loanAcctNum='" + loanAcctNum + '\'' +
                ", amountValue='" + amountValue + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                ", sn='" + sn + '\'' +
                ", OPAcctNum='" + OPAcctNum + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
