package com.longbridgetech.Kastle.domain.lienUpdate;

import java.io.Serializable;

/**
 * Created by CHRISTIAN on 9/1/2016.
 */
public class CustInfo4Vm implements Serializable {

    private String foracid;
    private String amountValue;
    private String allowFreezeAccnt;
    private String allowFalseDebit;
    private String currencyCode;
    private String trnParticulars;
    private String trnParticularCode;
    private String creditDebitFlg;
    private String sn;
    private String valueDt;
    private String rate;

    public String getValueDt() {
        return valueDt;
    }

    public void setValueDt(String valueDt) {
        this.valueDt = valueDt;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getForacid() {
        return foracid;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public void setForacid(String foracid) {
        this.foracid = foracid;
    }

    public String getAmountValue() {
        return amountValue;
    }

    public void setAmountValue(String amountValue) {
        this.amountValue = amountValue;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getTrnParticulars() {
        return trnParticulars;
    }

    public void setTrnParticulars(String trnParticulars) {
        this.trnParticulars = trnParticulars;
    }

    public String getCreditDebitFlg() {
        return creditDebitFlg;
    }

    public void setCreditDebitFlg(String creditDebitFlg) {
        this.creditDebitFlg = creditDebitFlg;
    }

    public String getTrnParticularCode() {
        return trnParticularCode;
    }

    public void setTrnParticularCode(String trnParticularCode) {
        this.trnParticularCode = trnParticularCode;
    }

    public String getAllowFreezeAccnt() {
        return allowFreezeAccnt;
    }

    public void setAllowFreezeAccnt(String allowFreezeAccnt) {
        this.allowFreezeAccnt = allowFreezeAccnt;
    }

    public String getAllowFalseDebit() {
        return allowFalseDebit;
    }

    public void setAllowFalseDebit(String allowFalseDebit) {
        this.allowFalseDebit = allowFalseDebit;
    }

    @Override
    public String toString() {
        return "CustInfo4Vm{" +
                "foracid='" + foracid + '\'' +
                ", amountValue='" + amountValue + '\'' +
                ", allowFreezeAccnt=" + allowFreezeAccnt +
                ", allowFalseDebit=" + allowFalseDebit +
                ", currencyCode='" + currencyCode + '\'' +
                ", trnParticulars='" + trnParticulars + '\'' +
                ", trnParticularCode='" + trnParticularCode + '\'' +
                ", creditDebitFlg='" + creditDebitFlg + '\'' +
                ", sn='" + sn + '\'' +
                ", valueDt='" + valueDt + '\'' +
                ", rate='" + rate + '\'' +
                '}';
    }

}