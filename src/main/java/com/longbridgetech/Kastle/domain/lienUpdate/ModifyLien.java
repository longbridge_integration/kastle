package com.longbridgetech.Kastle.domain.lienUpdate;

/**
 * Created by LONGBRIDGE on 11/29/2016.
 */
public class ModifyLien {

   private String lienID;
   private String unLienAmt;
   private String newExpDate;
   private String newLienRmks;

    public String getLienID() {
        return lienID;
    }

    public void setLienID(String lienID) {
        this.lienID = lienID;
    }

    public String getUnLienAmt() {
        return unLienAmt;
    }

    public void setUnLienAmt(String unLienAmt) {
        this.unLienAmt = unLienAmt;
    }

    public String getNewExpDate() {
        return newExpDate;
    }

    public void setNewExpDate(String newExpDate) {
        this.newExpDate = newExpDate;
    }

    public String getNewLienRmks() {
        return newLienRmks;
    }

    public void setNewLienRmks(String newLienRmks) {
        this.newLienRmks = newLienRmks;
    }

    @Override
    public String toString() {
        return "ModifyLien{" +
                "lienID='" + lienID + '\'' +
                ", unLienAmt='" + unLienAmt + '\'' +
                ", newExpDate='" + newExpDate + '\'' +
                ", newLienRmks='" + newLienRmks + '\'' +
                '}';
    }
}
