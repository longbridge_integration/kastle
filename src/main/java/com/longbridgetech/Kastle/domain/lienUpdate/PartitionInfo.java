package com.longbridgetech.Kastle.domain.lienUpdate;

/**
 * Created by LB-PRJ-020 on 7/4/2017.
 */
public class PartitionInfo {
    private String KasNumber;
    private String EntityType;
    private String KasLoanNumber;
    private int Sn;


    public String getKasNumber() {
        return KasNumber;
    }

    public void setKasNumber(String kasNumber) {
        KasNumber = kasNumber;
    }

    public String getEntityType() {
        return EntityType;
    }

    public void setEntityType(String entityType) {
        EntityType = entityType;
    }

    public String getKasLoanNumber() {
        return KasLoanNumber;
    }

    public void setKasLoanNumber(String kasLoanNumber) {
        KasLoanNumber = kasLoanNumber;
    }

    public int getSn() {
        return Sn;
    }

    public void setSn(int sn) {
        Sn = sn;
    }


    @Override
    public String toString() {
        return "PartitionInfo{" +
                "KasNumber='" + KasNumber + '\'' +
                ", EntityType='" + EntityType + '\'' +
                ", KasLoanNumber='" + KasLoanNumber + '\'' +
                ", Sn=" + Sn +
                '}';
    }


}
