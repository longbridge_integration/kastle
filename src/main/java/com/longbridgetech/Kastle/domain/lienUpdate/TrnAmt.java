package com.longbridgetech.Kastle.domain.lienUpdate;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

/**
 * Created by CHRISTIAN on 9/1/2016.
 */
public class TrnAmt implements Serializable {

    private String amountValue;
    private String currencyCode = "NGN";


    public String getAmountValue() {
        return amountValue;
    }

    public void setAmountValue(String amountValue) {
        this.amountValue = amountValue;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public String toString() {
        return "TrnAmt{" +
                "amountValue='" + amountValue + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                '}';
    }
}
