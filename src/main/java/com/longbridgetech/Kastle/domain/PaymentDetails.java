//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;

public class PaymentDetails implements Serializable {
    @Valid
    @NotNull
    ArrayList<BulkPaymentRequest> BulkPayRequest;

    public PaymentDetails() {

    }

    public ArrayList<BulkPaymentRequest> getBulkPayRequest() {
        return BulkPayRequest;
    }

    public void setBulkPayRequest(ArrayList<BulkPaymentRequest> bulkPayRequest) {
        BulkPayRequest = bulkPayRequest;
    }

    @Override
    public String toString() {
        return "PaymentDetails{" +
                "BulkPayRequest=" + BulkPayRequest +
                '}';
    }
}
