package com.longbridgetech.Kastle.domain;

import com.longbridgetech.Kastle.domain.lienUpdate.LienUpdateRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * Created by LB-PRJ-020 on 7/4/2017.
 */
public class LienBatchRequest {
    @Valid
    private ArrayList<LienUpdateRequest> lienUpdateRequests;


    public ArrayList<LienUpdateRequest> getLienUpdateRequests() {
        return lienUpdateRequests;
    }

    public void setLienUpdateRequests(ArrayList<LienUpdateRequest> lienUpdateRequests) {
        this.lienUpdateRequests = lienUpdateRequests;
    }


    @Override
    public String toString() {
        return "LienBatchRequest{" +
                "lienUpdateRequests=" + lienUpdateRequests +
                '}';
    }



}
