//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.domain;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

public class ExpCheckRequest implements Serializable {



    @NotEmpty
    private String RequestID;

    private String CustomerID;

    public ExpCheckRequest(){
    }

    public ExpCheckRequest(String customerID) {
        CustomerID = customerID;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }

    @Override
    public String toString() {
        return "ExpCheckRequest{" +
                "RequestID='" + RequestID + '\'' +
                ", CustomerID='" + CustomerID + '\'' +
                '}';
    }
}
