package com.longbridgetech.Kastle.domain;

import com.longbridgetech.Kastle.domain.acctInq.AcctBal;
import com.longbridgetech.Kastle.domain.acctInq.AcctInfo;
import com.longbridgetech.Kastle.domain.acctInq.CustInfo;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

/**
 * Created by CHRISTIAN on 8/18/2016.
 */
@XmlRootElement
public class AcctClosureResponse {
    private String accountNumber;
    private String responseMessage;
    private String errorMessage;
    private String responseCode;



    public AcctClosureResponse() {
    }

    public AcctClosureResponse(String accountNumber, String responseCode) {
       // this.RequestID = RequestID;
        this.accountNumber = accountNumber;
        this.responseCode = responseCode;
    }


    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }


    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        return "AcctClosureResponse{" +
                "accountNumber='" + accountNumber + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", responseCode='" + responseCode + '\'' +
                '}';
    }
}