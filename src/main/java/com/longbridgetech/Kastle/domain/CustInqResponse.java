package com.longbridgetech.Kastle.domain;


import com.longbridgetech.Kastle.domain.custInq.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

/**
 * Created by CHRISTIAN on 8/19/2016.
 */

@XmlRootElement
public class CustInqResponse {

   // private String RequestID;
    private ArrayList<CustInqAddrInfo> CustInqAddrInfo;
    private BlackListNegativeListDtls blackListNegativeListDtls;
    private CorpCustInfo corpCustInfo;
    private ArrayList<EntityDocInfo> EntityDocInfo;
    private GenCustDtls genCustDtls;
    private ArrayList<CustPhoneEmailInfo> custPhoneEmailInfos;
    private PsychographicInfo PsychographicInfo;
    private RetailCustInfo retailCustInfo;
    private String BVN;
    private String responseMessage;
    private String errorMessage;
    private String responseCode;
    private String requestID;


    public CustInqResponse() {
    }

    public CustInqResponse(String responseCode) {
        //this.RequestID = RequestID;
        this.responseCode = responseCode;
    }

    public ArrayList<com.longbridgetech.Kastle.domain.custInq.CustInqAddrInfo> getCustInqAddrInfo() {
        return CustInqAddrInfo;
    }

    public void setCustInqAddrInfo(ArrayList<com.longbridgetech.Kastle.domain.custInq.CustInqAddrInfo> custInqAddrInfo) {
        CustInqAddrInfo = custInqAddrInfo;
    }

    public BlackListNegativeListDtls getBlackListNegativeListDtls() {
        return blackListNegativeListDtls;
    }

    public void setBlackListNegativeListDtls(BlackListNegativeListDtls blackListNegativeListDtls) {
        this.blackListNegativeListDtls = blackListNegativeListDtls;
    }

    public CorpCustInfo getCorpCustInfo() {
        return corpCustInfo;
    }

    public void setCorpCustInfo(CorpCustInfo corpCustInfo) {
        this.corpCustInfo = corpCustInfo;
    }

    public ArrayList<com.longbridgetech.Kastle.domain.custInq.EntityDocInfo> getEntityDocInfo() {
        return EntityDocInfo;
    }

    public void setEntityDocInfo(ArrayList<com.longbridgetech.Kastle.domain.custInq.EntityDocInfo> entityDocInfo) {
        EntityDocInfo = entityDocInfo;
    }

    public GenCustDtls getGenCustDtls() {
        return genCustDtls;
    }

    public void setGenCustDtls(GenCustDtls genCustDtls) {
        this.genCustDtls = genCustDtls;
    }

    public ArrayList<CustPhoneEmailInfo> getCustPhoneEmailInfos() {
        return custPhoneEmailInfos;
    }

    public void setCustPhoneEmailInfos(ArrayList<CustPhoneEmailInfo> custPhoneEmailInfos) {
        this.custPhoneEmailInfos = custPhoneEmailInfos;
    }

    public com.longbridgetech.Kastle.domain.custInq.PsychographicInfo getPsychographicInfo() {
        return PsychographicInfo;
    }

    public void setPsychographicInfo(com.longbridgetech.Kastle.domain.custInq.PsychographicInfo psychographicInfo) {
        PsychographicInfo = psychographicInfo;
    }

    public RetailCustInfo getRetailCustInfo() {
        return retailCustInfo;
    }

    public void setRetailCustInfo(RetailCustInfo retailCustInfo) {
        this.retailCustInfo = retailCustInfo;
    }

    public String getBVN() {
        return BVN;
    }

    public void setBVN(String BVN) {
        this.BVN = BVN;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    @Override
    public String toString() {
        return "CustInqResponse{" +
                "CustInqAddrInfo=" + CustInqAddrInfo +
                ", blackListNegativeListDtls=" + blackListNegativeListDtls +
                ", corpCustInfo=" + corpCustInfo +
                ", EntityDocInfo=" + EntityDocInfo +
                ", genCustDtls=" + genCustDtls +
                ", custPhoneEmailInfos=" + custPhoneEmailInfos +
                ", PsychographicInfo=" + PsychographicInfo +
                ", retailCustInfo=" + retailCustInfo +
                ", BVN='" + BVN + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", requestID='" + requestID + '\'' +
                '}';
    }
}
