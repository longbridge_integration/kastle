package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.domain.*;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import com.longbridgetech.Kastle.persistence.ApprovedSystemsDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by CHRISTIAN on 8/15/2016.
 */
@WebService
public class CustInquiry {
    @Resource
    WebServiceContext wsContext;

    private Logger logger = LoggerFactory.getLogger(CustInquiry.class);

    public CustInquiry() {
    }

    @WebMethod
    public CustInqResponse doCustInq(@WebParam(
            name = "EnquiryDetails"
    ) CustInqRequest requestDetails) {

        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        String ipAddress = req.getRemoteAddr();
        CustInqRequest custInqRequest = new CustInqRequest( requestDetails.getCIF_id());
        this.logger.info("Lien Update Service Request With Data {}", custInqRequest.toString());
        CustInqResponse custInqResponse = new CustInqResponse();
        custInqResponse.setRequestID(requestDetails.getRequestID());
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set constraintViolations = validator.validate(requestDetails);
        Iterator constraintViolationIterator = constraintViolations.iterator();

        if (constraintViolationIterator.hasNext()) {
            ConstraintViolation Process = (ConstraintViolation) constraintViolationIterator.next();
            this.logger.info("Const Violation on  {}", Process.getPropertyPath());
            String responseMessage = Process.getPropertyPath() + " " + Process.getMessage();
            custInqResponse = new CustInqResponse("96");
            custInqResponse.setErrorMessage(responseMessage);
            return custInqResponse;
        }

            String isSysApproved = Helper.isSystemApproved(ipAddress);
            if(isSysApproved.equals("ERROR")){
                custInqResponse.setResponseCode("96");
                custInqResponse.setErrorMessage("Could  not Connect to DB");
                return custInqResponse;
            }else if(isSysApproved.equals("N")){
                custInqResponse.setResponseCode("96");
                custInqResponse.setErrorMessage(" System is Not  Approved");
                return custInqResponse;
            }
            FI fiexecuter = new FI();
            custInqResponse = fiexecuter.postCustInq(requestDetails);
            if(custInqResponse!=null){
                custInqResponse.setRequestID(requestDetails.getRequestID());
                this.logger.info("CustInquiry finished with response {}", custInqResponse.toString());
            }
        return custInqResponse;
    }
}
