

package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.domain.LienBatchRequest;
import com.longbridgetech.Kastle.domain.LienBatchResponse;
import com.longbridgetech.Kastle.Entities.LienUpdAudit;
import com.longbridgetech.Kastle.domain.lienUpdate.LienUpdateRequest;
import com.longbridgetech.Kastle.domain.lienUpdate.LienUpdateResponse;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import com.longbridgetech.Kastle.persistence.LienUpdAuditDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

@WebService
public class LienUpdate {
    @Resource
    private WebServiceContext wsContext;
    private MessageContext mc ;
    private HttpServletRequest req ;
    private Logger logger = LoggerFactory.getLogger(LienUpdate.class);

    public LienUpdate() {
    }

    @WebMethod
    public LienBatchResponse doLienUpdate(@WebParam(name = "EnquiryDetails") LienBatchRequest requestDetails) {
        mc = this.wsContext.getMessageContext();
        req = (HttpServletRequest)mc.get("javax.xml.ws.servlet.request");
        String ipAddress = req.getRemoteAddr();
        this.logger.info("Lien Update Started with Request Data {}", requestDetails.toString());
        LienBatchResponse lienBatchResponse = new LienBatchResponse();
        String isSysApproved = Helper.isSystemApproved(ipAddress);
        if(!isSysApproved.equals("Y")&&!isSysApproved.equals("N")){
            lienBatchResponse.setResponseCode("96");
            lienBatchResponse.setErrorMssg("Error encountered is : "+isSysApproved);
            return lienBatchResponse;
        }else if(isSysApproved.equals("N")){
            lienBatchResponse.setResponseCode("96");
            lienBatchResponse.setErrorMssg(" System is Not  Approved ");
            return lienBatchResponse;
        }
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set constraintViolations = validator.validate(requestDetails);
        Iterator constraintViolationIterator = constraintViolations.iterator();
        if(constraintViolations.size()>0){
            String errMssg=" ";
            int cnt = 1;
            while(constraintViolationIterator.hasNext() && cnt <= constraintViolations.size()){
                ConstraintViolation constraintViolation = (ConstraintViolation)constraintViolationIterator.next();
                logger.error("Issue is: {} {} ",constraintViolation.getPropertyPath(),constraintViolation.getMessage());
                errMssg = errMssg +  constraintViolation.getPropertyPath()+":"+constraintViolation.getMessage()+" ";
                cnt++;
            }
            lienBatchResponse.setResponseCode("96");
            lienBatchResponse.setErrorMssg(errMssg);
            return lienBatchResponse;
        }

        lienBatchResponse = this.beginBatchProcess(requestDetails);

        if(lienBatchResponse==null){
            lienBatchResponse.setResponseCode("96");
            lienBatchResponse.setErrorMssg("Fatal Error Occurred");
            return lienBatchResponse;
        }

        lienBatchResponse.setResponseCode("00");
        lienBatchResponse.setResponseMssg("SUCCESS");
        this.logger.info("Lien Update finished with response {}", lienBatchResponse.toString());
        return lienBatchResponse;
    }


    private LienBatchResponse beginBatchProcess(LienBatchRequest lienBatchRequest)  {
        LienUpdateResponse lienUpdResponse = null;
        LienBatchResponse lienBatchResp = new LienBatchResponse();
        ArrayList<LienUpdateResponse> lienUpdRespArray = new ArrayList<LienUpdateResponse>();
        LienUpdAuditDAOImpl transactnAuditDAO = new LienUpdAuditDAOImpl();
        Iterator i$ = lienBatchRequest.getLienUpdateRequests().iterator();

        while(i$.hasNext()) {

            LienUpdateRequest lienUpdateRequest = (LienUpdateRequest)i$.next();
            lienUpdResponse    = new LienUpdAuditDAOImpl().findTransactionByRequestID(lienUpdateRequest.getRequestId().trim());
            LienUpdAudit audit = new LienUpdAudit(lienUpdateRequest);
            audit.setRequestDate(new Timestamp((new Date()).getTime()));
            if(lienUpdResponse!=null){
                lienUpdResponse.setResponseMessage("Inquired From Previous Transaction");
                lienUpdRespArray.add(lienUpdResponse);
                continue;
            }else{
                this.logger.info("*******************INITIATING LIEN UPDATE ****Starts at {}******************", new Timestamp((new Date()).getTime()));
                lienUpdResponse = new FI().postLienUpdate(lienUpdateRequest);
                lienUpdRespArray.add(lienUpdResponse);
            }

            if(lienUpdResponse.getResponseCode()=="00"){
                audit.setStatus("SUCCESS");
            }else{
                audit.setStatus("FAILED");
            }
            String err = lienUpdResponse.getAcountingErrMsg() + " | " + lienUpdResponse.getLienADDErrMsg() + " | " + lienUpdResponse.getLienModErrMsg() + " | " + lienUpdResponse.getLoanRepayErrMsg();
            String resp = lienUpdResponse.getAcountingResult() + " | " + lienUpdResponse.getLienADDResult() + " | " + lienUpdResponse.getLienModResult() + " | " + lienUpdResponse.getLoanRepayResult();
            audit.setResponseMessage(resp);
            audit.setResponseCode(lienUpdResponse.getResponseCode());
            audit.setErrorMessage(lienUpdResponse.getErrorMessage() + " | " + err);
            audit.setResponseDate(new Timestamp((new Date()).getTime()));
            audit.setAcctInfoResp(lienUpdResponse.toString());
            this.logger.info("*******************ENDING LIEN UPDATE ****Starts at {}*********************", new Timestamp((new Date()).getTime()));

            try {
                transactnAuditDAO.saveRecordinTable(audit);
            } catch (Exception var11) {
                this.logger.info("Unable to save this due to" + var11.getMessage());
                return null;
            }
            audit=null;

        }
        if(lienUpdRespArray!=null){
            lienBatchResp.setLienUpdateResponses(lienUpdRespArray);
        }
        lienUpdResponse=null;
        lienUpdRespArray = null;
        return lienBatchResp;
    }
}
