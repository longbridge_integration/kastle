//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.domain.*;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.util.Iterator;
import java.util.Set;


@WebService
public class ExpCheck {
  @Resource
  WebServiceContext wsContext;

  private Logger logger = LoggerFactory.getLogger(ExpCheck.class);

  public ExpCheck() {
  }

  @WebMethod
  public ExpCheckResponse doExposureCheck(@WebParam(
          name = "EnquiryDetails"
  ) ExpCheckRequest RequestDetails) {

    MessageContext mc = this.wsContext.getMessageContext();
    HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
    String ipAddress = req.getRemoteAddr();
    ExpCheckRequest  requestSetter = new ExpCheckRequest(RequestDetails.getCustomerID());
    this.logger.info("Exposure Check Service Request Processed With Data {}", requestSetter.toString());

    ExpCheckResponse chkResponse;

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();
    Set constraintViolations = validator.validate(RequestDetails);
    Iterator constraintViolationIterator = constraintViolations.iterator();

    if (constraintViolationIterator.hasNext()){
        ConstraintViolation Process = (ConstraintViolation) constraintViolationIterator.next();
        this.logger.info("Const Violation on  {}", Process.getPropertyPath());
        String ResponseMessage = Process.getPropertyPath() + " " + Process.getMessage();
        chkResponse = new ExpCheckResponse(RequestDetails.getRequestID(),RequestDetails.getCustomerID(),ResponseMessage," ","96");
        return chkResponse;
    }

    chkResponse = new ExpCheckResponse();
    String isSysApproved = Helper.isSystemApproved(ipAddress);
    if(!isSysApproved.equals("Y")&&!isSysApproved.equals("N")){
      chkResponse.setResponseCode("96");
      chkResponse.setErrorMessage(" Error encountered is: "+isSysApproved);
      return chkResponse;
    }else if(isSysApproved.equals("N")){
      chkResponse.setResponseCode("96");
      chkResponse.setErrorMessage(" System is Not Approved ");
      return chkResponse;
    }

    FI fiexecuter = new FI();
    chkResponse = fiexecuter.postCheckExposure(RequestDetails);
    if (chkResponse.getResponseMessage() != null && chkResponse.getResponseMessage() == "SUCCESS") {
    chkResponse.setRequestID(RequestDetails.getRequestID());
    chkResponse.setResponseMessage("SUCCESS");
    }
    this.logger.info("Exposure Check finished with response {}", chkResponse.toString());
    return chkResponse;
  }
}
