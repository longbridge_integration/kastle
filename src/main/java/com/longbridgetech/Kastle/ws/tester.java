package com.longbridgetech.Kastle.ws;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by LB-PRJ-020 on 1/4/2018.
 */
public class tester {
    private final static SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
    private final static SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MMM-yyyy");
    static String myDate = "2017-06-30";

    public static void main(String[] args) {
        try {
            Date tdat = simpleDateFormat1.parse(myDate);
            System.out.println("Nowww: "+tdat.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            myDate = simpleDateFormat2.format(simpleDateFormat1.parse(myDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("New date is: "+myDate);
    }
}
