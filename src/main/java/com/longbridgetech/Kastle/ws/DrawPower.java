package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.domain.DrawPowerRequest;
import com.longbridgetech.Kastle.domain.DrwPowerResponse;
import com.longbridgetech.Kastle.Entities.DrawPowerAudit;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import com.longbridgetech.Kastle.persistence.DrawPowerAuditDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by CHRISTIAN on 8/15/2016.
 */
@WebService
public class DrawPower {
    @Resource
    WebServiceContext wsContext;

    private Logger logger = LoggerFactory.getLogger(DrawPower.class);

    public DrawPower() {
    }

    @WebMethod
    public DrwPowerResponse doDrwPower(@WebParam(
            name = "EnquiryDetails"
    ) DrawPowerRequest requestDetails) {

        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        String ipAddress = req.getRemoteAddr();
        this.logger.info("TopUp Service Request With Data {}", requestDetails.toString());
        DrwPowerResponse drwPowerResponse = new DrwPowerResponse();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set constraintViolations = validator.validate(requestDetails);
        Iterator constraintViolationIterator = constraintViolations.iterator();

        if (constraintViolationIterator.hasNext()) {
            ConstraintViolation Process = (ConstraintViolation) constraintViolationIterator.next();
            this.logger.info("Const Violation on  {}", Process.getPropertyPath());
            String responseMessage = Process.getPropertyPath() + " " + Process.getMessage();
            drwPowerResponse = new DrwPowerResponse(requestDetails.getForacid(),"96");
            drwPowerResponse.setErrorMessage(responseMessage);
            return drwPowerResponse;
        }

        DrawPowerAuditDAOImpl transactnAuditDAO = new DrawPowerAuditDAOImpl();
        DrawPowerAudit audit = new DrawPowerAudit(requestDetails);
        String status = "FAILED";

        String isSysApproved = Helper.isSystemApproved(ipAddress);
        if(!isSysApproved.equals("Y")&&!isSysApproved.equals("N")){
            drwPowerResponse.setResponseCode("96");
            drwPowerResponse.setErrorMessage("Error encountered is: "+isSysApproved);
            return drwPowerResponse;
        }else if(isSysApproved.equals("N")){
            drwPowerResponse.setResponseCode("96");
            drwPowerResponse.setErrorMessage(" System is Not  Approved ");
            return drwPowerResponse;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date;
            try {
                date = sdf.parse(requestDetails.getAppDate());
                sdf = new SimpleDateFormat("dd-MM-yyyy");
                requestDetails.setAppDate(sdf.format(date));
            } catch (ParseException e) {
                this.logger.error("error formatting date  {}  ", e.getStackTrace());
                e.printStackTrace();
            }
            FI fiexecuter = new FI();
             drwPowerResponse = fiexecuter.postDrawPower(requestDetails);
            if(drwPowerResponse.getResponseCode().equals("00")){
                status = "SUCCESS";
            }
            else {
                status = "Request Failed";
            }

        audit.setStatus(status);
        audit.setResponseMessage(drwPowerResponse.getResponseMessage());
        audit.setResponseCode(drwPowerResponse.getResponseCode());
        audit.setErrorMessage(drwPowerResponse.getErrorMessage());
        transactnAuditDAO.saveRecordinTable(audit);
        this.logger.info("DrawPower finished with response {}", drwPowerResponse.toString());
        return drwPowerResponse;
    }
}
