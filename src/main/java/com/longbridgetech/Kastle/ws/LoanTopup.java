package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.domain.LoanTopupRequest;
import com.longbridgetech.Kastle.Entities.LoanTopupAudit;
import com.longbridgetech.Kastle.domain.tdAcctInq.LoanTopupResponse;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import com.longbridgetech.Kastle.persistence.LoanTopupAuditDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.util.Iterator;
import java.util.Set;

@WebService
public class LoanTopup {
    @Resource
    WebServiceContext wsContext;

    private Logger logger = LoggerFactory.getLogger(LoanTopup.class);

    public LoanTopup() {
    }

    @WebMethod
    public LoanTopupResponse doTopup(@WebParam(
            name = "EnquiryDetails"
    ) LoanTopupRequest requestDetails) {

        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        String ipAddress = req.getRemoteAddr();
        this.logger.info("TopUp Service Request With Data {}", requestDetails.toString());
        LoanTopupResponse topupResponse = new LoanTopupResponse();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set constraintViolations = validator.validate(requestDetails);
        Iterator constraintViolationIterator = constraintViolations.iterator();

        if (constraintViolationIterator.hasNext()) {
            ConstraintViolation Process = (ConstraintViolation) constraintViolationIterator.next();
            this.logger.info("Const Violation on  {}", Process.getPropertyPath());
            String responseMessage = Process.getPropertyPath() + " " + Process.getMessage();
            topupResponse = new LoanTopupResponse(requestDetails.getAccountNumber(),"96");
            topupResponse.setErrorMessage(responseMessage);
            return topupResponse;
        }

        LoanTopupAuditDAOImpl transactnAuditDAO = new LoanTopupAuditDAOImpl();
        LoanTopupAudit audit = new LoanTopupAudit(requestDetails);
        String status;
        String isSysApproved = Helper.isSystemApproved(ipAddress);
        if(!isSysApproved.equals("Y")&&!isSysApproved.equals("N")){
            topupResponse.setResponseCode("96");
            topupResponse.setErrorMessage("Error encountered is : "+isSysApproved);
            return topupResponse;
        }else if(isSysApproved.equals("N")){
            topupResponse.setResponseCode("96");
            topupResponse.setErrorMessage(" System is Not  Approved ");
            return topupResponse;
        }
        FI fiexecuter = new FI();
        topupResponse = fiexecuter.postLoanTopup(requestDetails);
        if(topupResponse.getResponseCode().equals("00")){
            status = "SUCCESS";
        }
        else {
            status = "Request Failed";
            this.logger.error("Topup request failed:  {}  ", topupResponse.getErrorMessage());
        }

        audit.setStatus(status);
        audit.setResponseMessage(topupResponse.getResponseMessage());
        audit.setResponseCode(topupResponse.getResponseCode());
        audit.setErrorMessage(topupResponse.getErrorMessage());
        transactnAuditDAO.saveRecordinTable(audit);
        this.logger.info("Topup finished with response {}", topupResponse.toString());
        return topupResponse;
    }
}
