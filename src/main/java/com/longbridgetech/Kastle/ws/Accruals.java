//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.domain.*;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import com.longbridgetech.Kastle.finnacleIntegrator.TestCase;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import static com.google.common.math.IntMath.pow;


@WebService
public class Accruals {
    @Resource
    WebServiceContext wsContext;
    MessageContext mc ;
    HttpServletRequest req ;
    ApprovedSystems isSysApproved = null;
    LienBatchResponse lienBatchResponse ;
    private Logger logger = LoggerFactory.getLogger(Accruals.class);

    public Accruals() {
    }

    @WebMethod
    public KasEodResponse doAccrual(@WebParam(name = "EnquiryDetails")KasEodRequest kasEodRequest) {
        mc = this.wsContext.getMessageContext();
        req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        String ipAddress = req.getRemoteAddr();
        KasEodResponse kasEodResponse = new KasEodResponse();
        this.logger.info("Accruals Service Request With Data {}", kasEodRequest.toString());
        kasEodResponse.setResponseDesc("Invalid Accruals Operation Specified");
        kasEodResponse.setResponseCode("96");
        String respfrmService=kasEodRequest.getEodOperatn();
        String eodType = kasEodRequest.getEodType()==null|| StringUtils.isBlank(kasEodRequest.getEodType())?"ACCRUAL":kasEodRequest.getEodType();
        if(!eodType.toUpperCase().equals("ACCRUAL")&& !eodType.toUpperCase().equals("REPAYMENT") ){
            kasEodResponse.setResponseDesc("Invalid Accruals Type Specified");
            kasEodResponse.setResponseCode("96");
            return kasEodResponse;
        }
        String isSysApproved = Helper.isSystemApproved(ipAddress);
        if(isSysApproved.equals("ERROR")){
            kasEodResponse.setResponseCode("96");
            kasEodResponse.setResponseDesc("Could not Connect to DB");
            return kasEodResponse;
        }else if(isSysApproved.equals("N")){
            kasEodResponse.setResponseCode("96");
            kasEodResponse.setResponseDesc("System is Not Approved");
            return kasEodResponse;
        }
        if(respfrmService.equals("CHECK")){
            kasEodResponse = new FI().checkEODStatus(respfrmService,eodType);
            if(kasEodResponse==null){
                kasEodResponse.setResponseDesc("Unable To Check Accruals Status");
                kasEodResponse.setResponseCode("96");
            }
            return  kasEodResponse;
        }
        if(respfrmService.equals("START")){
            kasEodResponse = new FI().checkEODStatus("START",eodType);
            if(kasEodResponse.getResponseDesc().equals("READY")){
                String threadCnt = kasEodRequest.getThreadNum()==null
                        ||StringUtils.isBlank(kasEodRequest.getThreadNum())
                        ?calcThreadNum(kasEodResponse.getTotalRecord()):kasEodRequest.getThreadNum();
                int threadNum = Integer.parseInt(threadCnt);
                int fetchRange = calcFetchRange(kasEodResponse.getTotalRecord(),threadCnt);
                TestCase backGroundProc = new TestCase(threadNum,eodType,"MULTI",fetchRange);
                Thread t = new Thread(backGroundProc);
                t.start();
                kasEodResponse.setResponseDesc("STARTED");
                kasEodResponse.setResponseCode("02");
                return kasEodResponse;
            }else if(kasEodResponse.getResponseDesc().equals("RUNNING")){
                kasEodResponse.setResponseDesc("RUNNING");
                kasEodResponse.setResponseCode("01");
                return kasEodResponse;
            }else if(kasEodResponse.getResponseDesc().equals("COMPLETED")){
                kasEodResponse.setResponseCode("00");
                return kasEodResponse;
            }else{
                kasEodResponse.setResponseCode("96");
                return kasEodResponse;
            }
        }
        else if(respfrmService.equals("TRUNCATE")){
            kasEodResponse = new FI().checkEODStatus(respfrmService,eodType);
            return kasEodResponse;
        }
        else if(respfrmService.equals("HALT")){
            kasEodResponse = new FI().checkEODStatus(respfrmService,eodType);
            return kasEodResponse;
        }
        else if(respfrmService.equals("REINITIATE")){
            kasEodResponse = new FI().checkEODStatus(respfrmService,eodType);
            if(kasEodResponse.getResponseDesc().equals("READY")){
                String numofThread = kasEodRequest.getThreadNum()==null
                        ||StringUtils.isBlank(kasEodRequest.getThreadNum())
                        ?calcThreadNum(kasEodResponse.getTotalRecord()):kasEodRequest.getThreadNum();
                int threadNum = Integer.parseInt(numofThread);
                int fetchRange = calcFetchRange(kasEodResponse.getTotalRecord(),numofThread);
                TestCase backGroundProc = new TestCase(threadNum,eodType,"MULTI",fetchRange);
                Thread t = new Thread(backGroundProc);
                t.start();
                kasEodResponse.setResponseDesc("RE-INITIATED");
                kasEodResponse.setResponseCode("00");
                return kasEodResponse;
            }else{
                kasEodResponse.setResponseDesc(kasEodResponse.getResponseDesc());
                kasEodResponse.setResponseCode("96");
                return kasEodResponse;
            }
        }
        else if(respfrmService.equals("REINITIATE2")){
            if(kasEodRequest.getTranNotfrmSolThreads().contains("_")) {
                kasEodResponse = new FI().checkEODStatus("REINITIATE",eodType);
                String[] threadString = kasEodRequest.getTranNotfrmSolThreads().split("_");
                int cnter = 0;
                String numofThread = kasEodRequest.getThreadNum()==null
                        ||StringUtils.isBlank(kasEodRequest.getThreadNum())
                        ?calcThreadNum(kasEodResponse.getTotalRecord()):kasEodRequest.getThreadNum();
                int threadNum = Integer.parseInt(numofThread);
                int fetchRange = calcFetchRange(kasEodResponse.getTotalRecord(),numofThread);
                while (cnter < threadString.length) {
                    int threadID = Integer.parseInt(threadString[cnter].replaceAll("[\\D]",""));
                    TestCase backGroundProc = new TestCase(threadNum,eodType, "SINGLE", fetchRange,(threadID - 1));
                    Thread t = new Thread(backGroundProc);
                    t.start();cnter++;
                }
                kasEodResponse.setResponseDesc("RE-INITIATED");
                kasEodResponse.setResponseCode("00");
            }
            return kasEodResponse;
        }
        return kasEodResponse;

    }

//    public static void main(String[] args) {
//        String res = new Accruals().calcThreadNum("200000");
//        System.out.println(res);
//    }

    private String calcThreadNum(String recCount){
        int numOfRecords = Integer.parseInt(recCount);int numOfThread;
        if(numOfRecords<=40){
            numOfThread = 1;
        }else if(40<numOfRecords&&numOfRecords<70){
            numOfThread = 2;
        }else{
            double exactThreadNum = (Math.log(numOfRecords)/ Math.log(10));
            int wholeParts = (int) (Math.log(numOfRecords)/Math.log(10));
            numOfThread = 2 * pow(2,wholeParts-1);
            double fractionalParts = (exactThreadNum-wholeParts)*numOfThread;
            numOfThread+=fractionalParts;
        }
        return String.valueOf(numOfThread);
    }

    private int calcFetchRange(String numOfRecords,String numOfThread ){
        int totTableRecords = Integer.parseInt(numOfRecords)* 2;
        int threadCnt = Integer.parseInt(numOfThread);
        int fetchRange = totTableRecords/threadCnt;
        double recordsBalancer = new Double(totTableRecords) /threadCnt;
        if( recordsBalancer > fetchRange){
            fetchRange = fetchRange+1;
        }
//        System.out.println("Fetch Range for each Thread would be: "+fetchRange);
        return fetchRange;
    }

}
