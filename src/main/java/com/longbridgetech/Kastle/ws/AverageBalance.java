

package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.domain.*;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import com.longbridgetech.Kastle.persistence.ApprovedSystemsDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.util.Iterator;
import java.util.Set;



@WebService
public class AverageBalance {
  @Resource
  WebServiceContext wsContext;

  private Logger logger = LoggerFactory.getLogger(AverageBalance.class);

  public AverageBalance() {
  }

  @WebMethod
  public AverageBalResponse doAverageBalance(@WebParam(
          name = "EnquiryDetails"
  ) AverageBalanceRequest RequestDetails) {

    MessageContext mc = this.wsContext.getMessageContext();
    HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
    String ipAddress = req.getRemoteAddr();
    AverageBalanceRequest  requestSetter = new AverageBalanceRequest(RequestDetails.getRequestID(),RequestDetails.getAccountNumber(),RequestDetails.getnoOfYears());
    this.logger.info("Average Balance Service Request With Data {}", requestSetter.toString());

    AverageBalResponse balResponse= new AverageBalResponse();

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();
    Set constraintViolations = validator.validate(RequestDetails);
    Iterator constraintViolationIterator = constraintViolations.iterator();

    if (constraintViolationIterator.hasNext()){
        ConstraintViolation Process = (ConstraintViolation) constraintViolationIterator.next();
        this.logger.info("Const Violation on  {}", Process.getPropertyPath());
        String ResponseMessage = Process.getPropertyPath() + " " + Process.getMessage();
        balResponse = new AverageBalResponse(RequestDetails.getRequestID(),RequestDetails.getAccountNumber(),RequestDetails.getnoOfYears(),ResponseMessage," ","96");
        return balResponse;
    }

      FIResponse fiResponse;
      String status;

      String isSysApproved = Helper.isSystemApproved(ipAddress);
      if(!isSysApproved.equals("Y")&&!isSysApproved.equals("N")){
          balResponse.setResponseCode("96");
          balResponse.setErrorMessage("Error eencountered is: "+isSysApproved);
          return balResponse;
      }else if(isSysApproved.equals("N")){
          balResponse.setResponseCode("96");
          balResponse.setErrorMessage("System is  Not Approved");
          return balResponse;
      }

      FI fiexecuter = new FI();
      fiResponse = fiexecuter.fetchAverageBalance(RequestDetails);
      if(fiResponse.isSuccessful()){
        balResponse = new AverageBalResponse(RequestDetails.getRequestID(),RequestDetails.getAccountNumber(),RequestDetails.getnoOfYears(),fiResponse.getResponse(),"","00");
      }else{
        status = "FAILED";
        balResponse = new AverageBalResponse(RequestDetails.getRequestID(),RequestDetails.getAccountNumber(),RequestDetails.getnoOfYears(),status,fiResponse.getResponse(),"96");
      }
      return balResponse;
  }
}
