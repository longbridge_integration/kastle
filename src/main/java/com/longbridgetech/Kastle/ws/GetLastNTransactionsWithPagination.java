package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionRequest.getLastNTransactionsWithPaginationRequest;
import com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionResp.Body;
import com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionResp.GetLastNTransactionsWithPaginationResponse;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

/**
 * Created by CHRISTIAN on 8/15/2016.
 */
@WebService
public class GetLastNTransactionsWithPagination {
    @Resource
    WebServiceContext wsContext;

    private Logger logger = LoggerFactory.getLogger(GetLastNTransactionsWithPagination.class);

    public GetLastNTransactionsWithPagination() {
    }

    @WebMethod
    public Body LastNTransactionsWithPagination(@WebParam(
            name = "getLastNTransactionsWithPaginationRequest"
    ) getLastNTransactionsWithPaginationRequest requestDetails) {

        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        String ipAddress = req.getRemoteAddr();
        Body respWrapper = new Body();
        GetLastNTransactionsWithPaginationResponse LastNResp = new GetLastNTransactionsWithPaginationResponse();
        this.logger.info("LastNTransactionWithDescription Service Request With Data {}", requestDetails.toString());

//        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
//        Validator validator = factory.getValidator();
//        Set constraintViolations = validator.validate(requestDetails);
//        Iterator constraintViolationIterator = constraintViolations.iterator();

//        if (constraintViolationIterator.hasNext()) {
//            ConstraintViolation Process = (ConstraintViolation) constraintViolationIterator.next();
//            this.logger.info("Const Violation on  {}", Process.getPropertyPath());
//            String responseMessage = Process.getPropertyPath() + " " + Process.getMessage();
//            acctInqResponse = new AcctInqResponse(requestDetails.getAccountNumber(),"96");
//            acctInqResponse.setErrorMessage(responseMessage);
//            return acctInqResponse;
//        }

        String isSysApproved = Helper.isSystemApproved(ipAddress);
        if(!isSysApproved.equals("Y")&&!isSysApproved.equals("N")){
            LastNResp.setRespCode("96");
            LastNResp.setErrorMessage("Error encountered :"+isSysApproved);
            respWrapper.setGetLastNTransactionsWithPaginationResponse(LastNResp);
            return respWrapper;
        }else if(isSysApproved.equals("N")){
            LastNResp.setRespCode("96");
            LastNResp.setErrorMessage("System is Not Approved");
            respWrapper.setGetLastNTransactionsWithPaginationResponse(LastNResp);
            return respWrapper;
        }
        FI fiexecuter = new FI();
        LastNResp = fiexecuter.getLastnTransaction(requestDetails);
        if(LastNResp==null){
            LastNResp = new GetLastNTransactionsWithPaginationResponse();
            LastNResp.setErrorMessage("Fatal Error Occurred");
        }
        respWrapper.setGetLastNTransactionsWithPaginationResponse(LastNResp);
        this.logger.info("LastNTransaction finished with response {}", respWrapper.toString());
        return respWrapper;
    }
}
