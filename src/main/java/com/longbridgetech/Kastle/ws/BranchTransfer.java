//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.domain.*;
import com.longbridgetech.Kastle.Entities.BranchTransfAudit;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import com.longbridgetech.Kastle.persistence.BranchTransAuditDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

//import com.longbridgetech.integrify.persistence.PaymentDetailsDAOImpl;

@WebService
public class BranchTransfer {
  @Resource
  WebServiceContext wsContext;

  private Logger logger = LoggerFactory.getLogger(BranchTransfer.class);

  public BranchTransfer() {
  }

  @WebMethod
  public BranchTransferResponse doBranchTransfer(@WebParam(
          name = "EnquiryDetails"
  ) BranchTransferRequest RequestDetails) throws IOException {


    MessageContext mc = this.wsContext.getMessageContext();
    HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
    String ipAddress =req.getRemoteAddr();
    BranchTransferRequest  requestSetter = new BranchTransferRequest(RequestDetails.getRequestID(),RequestDetails.getAccountNumber(),RequestDetails.getToSoL());
    this.logger.info("Branch Transfer Service Request With Data {}", requestSetter.toString());

    BranchTransferResponse brXferResponse = new BranchTransferResponse();

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();
    Set constraintViolations = validator.validate(RequestDetails);
    Iterator constraintViolationIterator = constraintViolations.iterator();

    if (constraintViolationIterator.hasNext()){
      ConstraintViolation Process = (ConstraintViolation) constraintViolationIterator.next();
      this.logger.info("Const Violation on  {}", Process.getPropertyPath());
      String ResponseMessage = Process.getPropertyPath() + " " + Process.getMessage();
      brXferResponse = new BranchTransferResponse(RequestDetails.getRequestID(),RequestDetails.getAccountNumber(),"000", RequestDetails.getToSoL(),ResponseMessage,"96"," ");
      return brXferResponse;
    }

    String status;
    BranchTransAuditDAOImpl transactnAuditDAO = new BranchTransAuditDAOImpl();
    BranchTransfAudit audit = new BranchTransfAudit(RequestDetails);
    FIResponse fiResponse;
    String fromSol = "";

      String isSysApproved = Helper.isSystemApproved(ipAddress);
     if(!isSysApproved.equals("Y")&&!isSysApproved.equals("N")){
        brXferResponse.setResponseCode("96");
        brXferResponse.setErrorMessage("Error encountered is: "+isSysApproved);
        return brXferResponse;
      }else if(isSysApproved.equals("N")){
        brXferResponse.setResponseCode("96");
        brXferResponse.setErrorMessage("System is Not  Approved");
        return brXferResponse;
      }

      FI fiexecuter = new FI();
      fiResponse = fiexecuter.postBranchTransfer(RequestDetails);
      String[]resComp = fiResponse.getResponse().split("_");
      fiResponse.setResponse(resComp[0]);
      if(fiResponse.isSuccessful()){

        status = "SUCCESS";
        fromSol = resComp[1];
        audit.setFromSol(fromSol);
        brXferResponse = new BranchTransferResponse(RequestDetails.getRequestID(),RequestDetails.getAccountNumber(),fromSol,RequestDetails.getToSoL(),"","00",status);
      }else{
        status = "FAILED";
        brXferResponse = new BranchTransferResponse(RequestDetails.getRequestID(),RequestDetails.getAccountNumber(),"", RequestDetails.getToSoL(),status,"96","");
      }
    audit.setStatus(status);
    audit.setResponseMessage(brXferResponse.getResponseMessage());
    audit.setResponseCode(brXferResponse.getResponseCode());
    audit.setErrorMessage(brXferResponse.getErrorMessage());
    audit.setFromSol(fromSol);
    transactnAuditDAO.saveRecordinTable(audit);
    return brXferResponse;
  }
}
