package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.domain.*;
import com.longbridgetech.Kastle.Entities.AcctClosureAudit;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import com.longbridgetech.Kastle.persistence.AcctClosureAuditDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by CHRISTIAN on 8/15/2016.
 */
@WebService
public class AcctClosure {
    @Resource
    WebServiceContext wsContext;

    private Logger logger = LoggerFactory.getLogger(AcctClosure.class);

    public AcctClosure() {
    }

    @WebMethod
    public AcctClosureResponse doAcctClosure(@WebParam(
            name = "EnquiryDetails"
    ) AcctClosureRequest requestDetails) {

        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        String ipAddress = req.getRemoteAddr();
        this.logger.info("AcctClosure Service Request With Data {}", requestDetails.toString());
        AcctClosureResponse closureResponse= new AcctClosureResponse();

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set constraintViolations = validator.validate(requestDetails);
        Iterator constraintViolationIterator = constraintViolations.iterator();

        if (constraintViolationIterator.hasNext()) {
            ConstraintViolation Process = (ConstraintViolation) constraintViolationIterator.next();
            this.logger.info("Const Violation on  {}", Process.getPropertyPath());
            String responseMessage = Process.getPropertyPath() + " " + Process.getMessage();
            closureResponse = new AcctClosureResponse(requestDetails.getAccountNumber(),"96");
            closureResponse.setErrorMessage(responseMessage);
            return closureResponse;
        }

        AcctClosureAuditDAOImpl transactnAuditDAO = new AcctClosureAuditDAOImpl();
        AcctClosureAudit  audit = new AcctClosureAudit(requestDetails);
        String status = null;

        String isSysApproved = Helper.isSystemApproved(ipAddress);
        if(!isSysApproved.equals("Y")&&!isSysApproved.equals("N")){
            closureResponse.setResponseCode("96");
            closureResponse.setErrorMessage("Error encountered is : "+isSysApproved);
            return closureResponse;
        }else if(isSysApproved.equals("N")){
            closureResponse.setResponseCode("96");
            closureResponse.setErrorMessage("System is Not Approved");
            return closureResponse;
        }

        FI fiexecuter = new FI();
        closureResponse = fiexecuter.postAcctClosure(requestDetails);
        if(closureResponse!=null){
            closureResponse.setAccountNumber(requestDetails.getAccountNumber());
            if(closureResponse.getResponseCode().equals("00")){
                status = "SUCCESS";
            }
            else {
                status = "FAILED";
            }
        }

        audit.setStatus(status);
        audit.setResponseMessage(closureResponse.getResponseMessage());
        audit.setResponseCode(closureResponse.getResponseCode());
        audit.setErrorMessage(closureResponse.getErrorMessage());
        audit.setResponseDate(new Timestamp(new Date().getTime()));
        transactnAuditDAO.saveRecordinTable(audit);
        this.logger.info("AcctClosure finished with response {}", closureResponse.toString());
        return closureResponse;
    }
}
