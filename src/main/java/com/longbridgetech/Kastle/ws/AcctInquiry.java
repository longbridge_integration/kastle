package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.domain.*;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import com.longbridgetech.Kastle.persistence.ApprovedSystemsDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by CHRISTIAN on 8/15/2016.
 */
@WebService
public class AcctInquiry {
    @Resource
    WebServiceContext wsContext;

    private Logger logger = LoggerFactory.getLogger(AcctInquiry.class);

    public AcctInquiry() {
    }

    @WebMethod
    public AcctInqResponse doAcctInq(@WebParam(
            name = "EnquiryDetails"
    ) AcctInqRequest requestDetails) {

        MessageContext mc = this.wsContext.getMessageContext();
        HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
        String ipAddress = req.getRemoteAddr();
        AcctInqRequest acctInqRequest = new AcctInqRequest( requestDetails.getAccountNumber());
        this.logger.info("AcctInquiry Service Request With Data {}", acctInqRequest.toString());
        AcctInqResponse acctInqResponse = new AcctInqResponse();

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set constraintViolations = validator.validate(requestDetails);
        Iterator constraintViolationIterator = constraintViolations.iterator();

        if (constraintViolationIterator.hasNext()) {
            ConstraintViolation Process = (ConstraintViolation) constraintViolationIterator.next();
            this.logger.info("Const Violation on  {}", Process.getPropertyPath());
            String responseMessage = Process.getPropertyPath() + " " + Process.getMessage();
            acctInqResponse = new AcctInqResponse(requestDetails.getAccountNumber(),"96");
            acctInqResponse.setErrorMessage(responseMessage);
            return acctInqResponse;
        }

            String isSysApproved = Helper.isSystemApproved(ipAddress);
            if(!isSysApproved.equals("Y")&&!isSysApproved.equals("N")){
                acctInqResponse.setResponseCode("96");
                acctInqResponse.setErrorMessage("Error encountered :"+isSysApproved);
                return acctInqResponse;
            }else if(isSysApproved.equals("N")){
                acctInqResponse.setResponseCode("96");
                acctInqResponse.setErrorMessage("System is Not Approved");
                return acctInqResponse;
            }
            FI fiexecuter = new FI();
            acctInqResponse = fiexecuter.postAcctInq(requestDetails);
        this.logger.info("AcctInquiry finished with response {}", acctInqResponse.toString());
        return acctInqResponse;
    }
}
