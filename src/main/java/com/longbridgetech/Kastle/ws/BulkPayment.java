//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.ws;

import com.longbridgetech.Kastle.Entities.BulkPayAudit;
import com.longbridgetech.Kastle.domain.*;
import com.longbridgetech.Kastle.finnacleIntegrator.FI;
import com.longbridgetech.Kastle.finnacleIntegrator.Helper;
import com.longbridgetech.Kastle.persistence.BulkPayAuditDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

//import com.longbridgetech.integrify.persistence.PaymentDetailsDAOImpl;

@WebService
public class BulkPayment {
  @Resource
  WebServiceContext wsContext;

  private Logger logger = LoggerFactory.getLogger(BulkPayment.class);

  public BulkPayment() {
  }

  @WebMethod
  public BulkPaymentResponse doBulkPayment(@WebParam(
          name = "EnquiryDetails"
  ) PaymentDetails RequestDetails) {

    MessageContext mc = this.wsContext.getMessageContext();
    HttpServletRequest req = (HttpServletRequest) mc.get("javax.xml.ws.servlet.request");
    String ipAddress = req.getRemoteAddr();
    this.logger.info("Bulk Payment Service Request With Data {}", RequestDetails.toString());
    BulkPaymentResponse bulkPayResponse=null;
    ArrayList<BulkPaymentTran> bulkPaymentTranArray;



    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();
    Set constraintViolations = validator.validate(RequestDetails);
    Iterator constraintViolationIterator = constraintViolations.iterator();

    if (constraintViolationIterator.hasNext()){
      ConstraintViolation Process = (ConstraintViolation) constraintViolationIterator.next();
      this.logger.info("Const Violation on  {}", Process.getPropertyPath());
      String errorMessage = Process.getPropertyPath() + " " + Process.getMessage();
      bulkPayResponse = new BulkPaymentResponse(errorMessage,"96");
      return bulkPayResponse;
    }

    String status ="FAILED";
    BulkPayAuditDAOImpl transactnAuditDAO = new BulkPayAuditDAOImpl();

        String isSysApproved = Helper.isSystemApproved(ipAddress);
        BulkPaymentTran bulkPaymentTran;
        if(isSysApproved.equals("Y"))
        {
            BulkPayAudit audit;
            bulkPaymentTranArray = new ArrayList<BulkPaymentTran>();
            FIResponse fiResponse;
            for (BulkPaymentRequest sngPayment:RequestDetails.getBulkPayRequest())
            {
                  fiResponse = new FI().postBulkPayment(sngPayment);
                  if(fiResponse.isSuccessful()){
                    status = "SUCCESS";
                    bulkPaymentTran = new BulkPaymentTran(sngPayment.getRequestID(),fiResponse.getResponse(),status,null,"00");
                    bulkPaymentTranArray.add(bulkPaymentTran);
                  }
                  else{
                    status = "FAILED";
                    bulkPaymentTran = new BulkPaymentTran(sngPayment.getRequestID(),null,status,fiResponse.getResponse(),"96");
                    bulkPaymentTranArray.add(bulkPaymentTran);
                  }
                    audit= new BulkPayAudit(sngPayment);
                    audit.setStatus(status);
                    audit.setResponseMessage(bulkPaymentTran.getResponseMessage());
                    audit.setResponseCode(bulkPaymentTran.getResponseCode());
                    audit.setErrorMessage(bulkPaymentTran.getErrorMessage());
                    transactnAuditDAO.saveRecordinTable(audit);
              }
              bulkPayResponse = new BulkPaymentResponse();
              bulkPayResponse.setBulkPaymentTran(bulkPaymentTranArray);
        }
        else if(isSysApproved.equals("N")){
            String errorMessage = "System is Not Approved for BulkPayment";
            bulkPayResponse = new BulkPaymentResponse(errorMessage,"96");
        }else{
            String errorMessage = "Erorr encountered is: "+isSysApproved;
            bulkPayResponse = new BulkPaymentResponse(errorMessage,"96");
        }
      return bulkPayResponse;
  }
}
