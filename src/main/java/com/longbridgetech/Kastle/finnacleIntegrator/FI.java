package com.longbridgetech.Kastle.finnacleIntegrator;

import com.google.common.base.CaseFormat;
import com.longbridgetech.Kastle.domain.*;
import com.longbridgetech.Kastle.domain.expCheck.ExpChechInfo;
import com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionRequest.getLastNTransactionsWithPaginationRequest;
import com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionResp.GetLastNTransactionsWithPaginationResponse;
import com.longbridgetech.Kastle.domain.lienUpdate.*;
import com.longbridgetech.Kastle.domain.tdAcctInq.LoanTopupResponse;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.KeyStore;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static com.sun.xml.internal.messaging.saaj.packaging.mime.util.ASCIIUtility.getBytes;
import com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionResp.GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails;

public class FI {
    private final Logger logger = LoggerFactory.getLogger(FI.class);
    private final String FI_SERVICE_URL = this.getPropertyFromFile("FI.URL");
    private VelocityEngine ve;
    private Template t;
    private VelocityContext context;

    public FI() {
        Properties props = new Properties();
        props.put("resource.loader", "class");
        props.put("runtime.log.logsystem.class","org.apache.velocity.runtime.log.NullLogSystem");
        props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        this.ve = new VelocityEngine();
        this.ve.init(props);
        this.context = new VelocityContext();
    }

//    public CustInqResponse postCustInq(CustInqRequest custInqRequest) {
//        this.logger.info("*******************INITIATING CIF INQUIRY...**********************");
//        this.logger.info("request {}", custInqRequest);
//        StringWriter writer = new StringWriter();
//
//        this.t = this.ve.getTemplate("FI-XML/CustInquiry.vm");
//        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);
//        this.context.put("RequestUUID", requestId);
//
//        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
//        this.context.put("CustId", custInqRequest.getCIF_id());
//
//
//        this.t.merge(this.context, writer);
//        String payload = writer.toString();
//        String responseMsg = this.callService(payload);
//        String charSequence = "<Status>SUCCESS</Status>";
//        boolean isSuccessful = responseMsg.contains(charSequence);
//
//        CustInqResponse custInqResponse;
//        if (isSuccessful) {
//            this.logger.info("****************************************SUCCESS ****************************************");
//            try {
//                String responseBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
//                responseBody = responseBody + "<CustInqResponse>\n";
//                responseBody = responseBody + StringUtils.substringBetween(responseMsg, "<CustInqRs>", "</CustInqRs>").trim();
//                responseBody = responseBody + "\n</CustInqResponse>";
//                logger.info("Response Body: {}", responseBody);
//                Pattern pattern = Pattern.compile("<\\w+");
//                Matcher matcher = pattern.matcher(responseBody);
//                while (matcher.find()) {
//                    if (!matcher.group().matches("<[A-Z]{2,}.*")) {
//                        responseBody = responseBody.replaceAll(matcher.group(), matcher.group().substring(0, 1) +
//                                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, matcher.group().substring(1)));
//                    }
//                }
//                pattern = Pattern.compile("</\\w+");
//                matcher = pattern.matcher(responseBody);
//                while (matcher.find()) {
//                    if (!matcher.group().matches("</[A-Z]{2,}.*")) {
//                        responseBody = responseBody.replaceAll(matcher.group(), matcher.group().substring(0, 2) +
//                                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, matcher.group().substring(2)));
//                    }
//                }
//                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//                InputSource inputSource = new InputSource();
//                inputSource.setCharacterStream(new StringReader(responseBody));
//                Document document = documentBuilder.parse(inputSource);
//
//                NodeList nodes = document.getElementsByTagName("custPhoneEmailInfo");
//                for (int i = 0; i < nodes.getLength(); i++) {
//                    document.renameNode(nodes.item(i), null, "custPhoneEmailInfos");
//                }
//                DOMSource domSource = new DOMSource(document);
//                Transformer transformer = TransformerFactory.newInstance().newTransformer();
//                StringWriter sw = new StringWriter();
//                StreamResult sr = new StreamResult(sw);
//                transformer.transform(domSource, sr);
//                responseBody = sw.toString();
//                InputStream inputStream = new ByteArrayInputStream(getBytes(responseBody));
//                JAXBContext jaxbContext = JAXBContext.newInstance(CustInqResponse.class);
//
//                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//                custInqResponse = (CustInqResponse) jaxbUnmarshaller.unmarshal(inputStream);
//                String BVN = fetchCustBVN(custInqRequest.getCIF_id());
//                custInqResponse.setBVN(BVN);
//                custInqResponse.setResponseMessage("CustInq Successfully");
//                custInqResponse.setResponseCode("00");
//
//
//            } catch (Exception e) {
//                custInqResponse = new CustInqResponse();
//                custInqResponse.setErrorMessage("Error Processsing request");
//                this.logger.error("******************** FI encountered error. Error Description {} ********************", e.toString());
//                custInqResponse.setResponseCode("96");
//                e.printStackTrace();
//            }
//
//
//        } else {
//            String errorCode = StringUtils.substringBetween(responseMsg, "<ErrorCode>", "</ErrorCode>");
//            String errorDesc = StringUtils.substringBetween(responseMsg, "<ErrorDesc>", "</ErrorDesc>");
//            String errorSource = StringUtils.substringBetween(responseMsg, "<ErrorSource>", "</ErrorSource>");
//            String errorType = StringUtils.substringBetween(responseMsg, "<ErrorType>", "</ErrorType>");
//            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
//            this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);
//            custInqResponse = new CustInqResponse();
//            custInqResponse.setErrorMessage(errorMessage);
//            custInqResponse.setResponseCode("96");
//            // custInqResponse.setRequestID(custInqRequest.getRequestID());
//        }
//
//        this.logger.info("**************************************** CUST INQ  ENDED****************************************");
//        return custInqResponse;
//    }


    public CustInqResponse postCustInq(CustInqRequest custInqRequest) {
        this.logger.info("*******************INITIATING CIF INQUIRY...**********************");
        this.logger.info("request {}", custInqRequest);
        StringWriter writer = new StringWriter();

        this.t = this.ve.getTemplate("FI-XML/CustInquiry.vm");
        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", requestId);

        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("CustId", custInqRequest.getCIF_id());


        this.t.merge(this.context, writer);
        String payload = writer.toString();
        String responseMsg = this.callService(payload);
        String charSequence = "<Status>SUCCESS</Status>";
        boolean isSuccessful = responseMsg.contains(charSequence);

        CustInqResponse custInqResponse;
        if (isSuccessful) {
            this.logger.info("****************************************SUCCESS ****************************************");
            try {
                String responseBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
                responseBody = responseBody + "<CustInqResponse>\n";
                responseBody = responseBody + StringUtils.substringBetween(responseMsg, "<CustInqRs>", "</CustInqRs>").trim();
                responseBody = responseBody + "\n</CustInqResponse>";
                logger.info("Response Body: {}", responseBody);
                Pattern pattern = Pattern.compile("<\\w+");
                Matcher matcher = pattern.matcher(responseBody);
                while (matcher.find()) {
                    if (!matcher.group().matches("<[A-Z]{2,}.*")) {
                        responseBody = responseBody.replaceAll(matcher.group(), matcher.group().substring(0, 1) +
                                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, matcher.group().substring(1)));
                    }
                }
                pattern = Pattern.compile("</\\w+");
                matcher = pattern.matcher(responseBody);
                while (matcher.find()) {
                    if (!matcher.group().matches("</[A-Z]{2,}.*")) {
                        responseBody = responseBody.replaceAll(matcher.group(), matcher.group().substring(0, 2) +
                                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, matcher.group().substring(2)));
                    }
                }
                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                InputSource inputSource = new InputSource();
                inputSource.setCharacterStream(new StringReader(responseBody));
                Document document = documentBuilder.parse(inputSource);

                NodeList nodes = document.getElementsByTagName("custPhoneEmailInfo");
                for (int i = 0; i < nodes.getLength(); i++) {
                    document.renameNode(nodes.item(i), null, "custPhoneEmailInfos");
                }
                DOMSource domSource = new DOMSource(document);
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                StringWriter sw = new StringWriter();
                StreamResult sr = new StreamResult(sw);
                transformer.transform(domSource, sr);
                responseBody = sw.toString();
                InputStream inputStream = new ByteArrayInputStream(getBytes(responseBody));
                JAXBContext jaxbContext = JAXBContext.newInstance(CustInqResponse.class);

                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//                custInqResponse = (CustInqResponse) jaxbUnmarshaller.unmarshal(inputStream);
//                custInqResponse = (CustInqResponse) jaxbUnmarshaller.unmarshal(new InputStreamReader(new FileInputStream(responseBody), "ISO-8859-1"));
                custInqResponse = (CustInqResponse) jaxbUnmarshaller.unmarshal(new StringReader(responseBody));
                String BVN = fetchCustBVN(custInqRequest.getCIF_id());
                custInqResponse.setBVN(BVN);
                custInqResponse.setResponseMessage("CustInq Successfully");
                custInqResponse.setResponseCode("00");


            } catch (Exception e) {
                custInqResponse = new CustInqResponse();
                custInqResponse.setErrorMessage("Error Processsing request");
                this.logger.error("******************** FI encountered error. Error Description {} ********************", e.toString());
                custInqResponse.setResponseCode("96");
                e.printStackTrace();
            }


        } else {
            String errorCode = StringUtils.substringBetween(responseMsg, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMsg, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMsg, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMsg, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);
            custInqResponse = new CustInqResponse();
            custInqResponse.setErrorMessage(errorMessage);
            custInqResponse.setResponseCode("96");
            // custInqResponse.setRequestID(custInqRequest.getRequestID());
        }

        this.logger.info("**************************************** CUST INQ  ENDED****************************************");
        return custInqResponse;
    }

    public AcctInqResponse postAcctInq(AcctInqRequest acctInqRequest) {
        this.logger.info("*******************INITIATING ACCOUNT INQUIRY...**********************");
        this.logger.info("request {}", acctInqRequest);
        StringWriter writer = new StringWriter();

        this.t = this.ve.getTemplate("FI-XML/AccountInq.vm");
        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", requestId);

        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("AcctId", acctInqRequest.getAccountNumber());


        this.t.merge(this.context, writer);
        String payload = writer.toString();
        System.out.println("Request is \n"+payload);
        String responseMsg = this.callService(payload);
        String charSequence = "<Status>SUCCESS</Status>";
        System.out.println("Issue is: \n"+responseMsg);
        boolean isSuccessful = responseMsg.contains(charSequence);
        AcctInqResponse acctInqResponse = new AcctInqResponse();
        if (isSuccessful) {
            this.logger.info("****************************************SUCCESS ****************************************");
            try {
                String responseBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
                responseBody = responseBody + "<AcctInqResponse>\n";
                responseBody = responseBody + StringUtils.substringBetween(responseMsg, "<AcctInqRs>", "</AcctInqRs>").trim();
                String acctStatus = StringUtils.substringBetween(responseMsg, "<ACCTSTATUS>", "</ACCTSTATUS>");
                if (acctStatus != null) {
                    responseBody = responseBody + "<AcctStatus>" + acctStatus + "</AcctStatus>\n";
                }
                responseBody = responseBody + "\n</AcctInqResponse>";
                //String alphaAndDigits = input.replaceAll("[^a-zA-Z0-9]+","");
                Pattern pattern = Pattern.compile("<\\w+");
                Matcher matcher = pattern.matcher(responseBody);
                while (matcher.find()) {
                    if (!matcher.group().matches("<[A-Z]{2,}.*")) {
                        responseBody = responseBody.replaceAll(matcher.group(), matcher.group().substring(0, 1) +
                                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, matcher.group().substring(1)));
                    }
                }
                pattern = Pattern.compile("</\\w+");
                matcher = pattern.matcher(responseBody);
                while (matcher.find()) {
                    if (!matcher.group().matches("</[A-Z]{2,}.*")) {
                        responseBody = responseBody.replaceAll(matcher.group(), matcher.group().substring(0, 2) +
                                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, matcher.group().substring(2)));
                    }
                }

                logger.info("responseBefore: {}", responseBody);
                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                InputSource inputSource = new InputSource();
                inputSource.setCharacterStream(new StringReader(responseBody));
                Document document = documentBuilder.parse(inputSource);

                NodeList nodes = document.getElementsByTagName("custId");
                document.renameNode(nodes.item(0), null, "custInfo");
                nodes = document.getElementsByTagName("acctId");
                document.renameNode(nodes.item(0), null, "acctInfo");
                nodes = document.getElementsByTagName("name");
                document.renameNode(nodes.item(0), null, "bankName");
                nodes = document.getElementsByTagName("city");
                document.renameNode(nodes.item(0), null, "addrCity");
                nodes = document.getElementsByTagName("stateProv");
                document.renameNode(nodes.item(0), null, "addrStateProv");

                DOMSource domSource = new DOMSource(document);
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                StringWriter sw = new StringWriter();
                StreamResult sr = new StreamResult(sw);
                transformer.transform(domSource, sr);
                responseBody = sw.toString();

                InputStream inputStream = new ByteArrayInputStream(getBytes(responseBody));
                JAXBContext jaxbContext = JAXBContext.newInstance(AcctInqResponse.class);

                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                acctInqResponse = (AcctInqResponse) jaxbUnmarshaller.unmarshal
                        (new InputStreamReader(inputStream, "UTF-8"));


                //acctInqResponse.setAcctBal(new ArrayList<AcctBal>(Arrays.asList(acctInqFiResponse.getAcctBal())));
                // acctInqResponse.getODA().repl
                //acctInqResponse.setCifId(acctInqRequest.getCifId());
                acctInqResponse.setResponseCode("00");

                String AOC = Helper.getAOC(acctInqRequest.getAccountNumber());
                acctInqResponse.getAcctInfo().setAOC(AOC);
                String custDOB = Helper.getCustDOB(acctInqRequest.getAccountNumber());
                acctInqResponse.getCustInfo().setDateOfBirth(custDOB);
                acctInqResponse.setResponseCode("00");
                acctInqResponse.setAccountNumber(acctInqRequest.getAccountNumber());
                acctInqResponse.setResponseMessage("AcctInq Successfully");


            } catch (Exception e) {
                acctInqResponse = new AcctInqResponse();
                acctInqResponse.setErrorMessage("Error Processsing request");
                this.logger.error("******************** FI encountered error. Error Description {} ********************", e.toString());
                acctInqResponse.setResponseCode("96");
                e.printStackTrace();
            }
        } else {
            String errorCode = StringUtils.substringBetween(responseMsg, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMsg, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMsg, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMsg, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);
            acctInqResponse = new AcctInqResponse();
            acctInqResponse.setErrorMessage(errorMessage);
            acctInqResponse.setResponseCode("96");
            // acctInqFiResponse.setRequestID(acctInqRequest.getRequestID());
        }

        this.logger.info("**************************************** ACCT INQ  ENDED****************************************");
        return acctInqResponse;
    }


    public GetLastNTransactionsWithPaginationResponse getLastnTransaction(getLastNTransactionsWithPaginationRequest lastNRequestdetails) {
        this.logger.info("*******************INITIATING ACCOUNT INQUIRY...**********************");
        this.logger.info("request {}", lastNRequestdetails);
        StringWriter writer = new StringWriter();

        this.t = this.ve.getTemplate("FI-XML/lastNTransactionWithDescrptn.vm");
        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", requestId);

        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("foracid", lastNRequestdetails.getAcid());
        this.context.put("solId", lastNRequestdetails.getBranchId());
        this.context.put("numOfTranRow", lastNRequestdetails.getLastNTransactions());

        this.t.merge(this.context, writer);
        String payload = writer.toString();
        String responseMsg = this.callService(payload);
        String charSequence = "<Status>SUCCESS</Status>";
        boolean isSuccessful = responseMsg.contains(charSequence);
        GetLastNTransactionsWithPaginationResponse resp = new GetLastNTransactionsWithPaginationResponse();
        if (isSuccessful) {
            responseMsg =  StringUtils.substringBetween(responseMsg, "<Body>", "</Body>");
            System.out.println("I got : "+responseMsg);
            InputStream inputStream = new ByteArrayInputStream(getBytes(responseMsg));
            JAXBContext jaxbContext = null;
            try {
                jaxbContext = JAXBContext.newInstance(GetLastNTransactionsWithPaginationResponse.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//                resp = (GetLastNTransactionsWithPaginationResponseType) jaxbUnmarshaller.unmarshal(new InputStreamReader(inputStream, "UTF-8"));
                resp = (GetLastNTransactionsWithPaginationResponse) jaxbUnmarshaller.unmarshal(inputStream);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            if(resp!=null){
                String tranDescription;
                String acid = resp.getPaginatedAccountStatement().getAccountBalances().getAcid();
                acid = Helper.getAcid(acid);
                ArrayList<TransactionDetails> transactionList = new ArrayList<TransactionDetails>();
                GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement paginatedAccountStatement = resp.getPaginatedAccountStatement();
                TransactionDetails.TransactionSummary transactionSummary;
                for(TransactionDetails tranItem : resp.getPaginatedAccountStatement().getTransactionDetails())
                {
                    tranDescription = Helper.getTranDesciption(acid,tranItem);
                    transactionSummary = tranItem.getTransactionSummary();
                    transactionSummary.setTxnDesc(tranDescription);
                    tranItem.setTransactionSummary(transactionSummary);
                    transactionList.add(tranItem);
                }
                paginatedAccountStatement.setTransactionDetails(transactionList);
                resp.setPaginatedAccountStatement(paginatedAccountStatement);
                resp.setRespCode("00");
            }


//            this.logger.info("****************************************SUCCESS ****************************************");
//            try {
//                String responseBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
//                responseBody = responseBody + "<AcctInqResponse>\n";
//                responseBody = responseBody + StringUtils.substringBetween(responseMsg, "<AcctInqRs>", "</AcctInqRs>").trim();
//                String acctStatus = StringUtils.substringBetween(responseMsg, "<ACCTSTATUS>", "</ACCTSTATUS>");
//                if (acctStatus != null) {
//                    responseBody = responseBody + "<AcctStatus>" + acctStatus + "</AcctStatus>\n";
//                }
//                responseBody = responseBody + "\n</AcctInqResponse>";
//                //String alphaAndDigits = input.replaceAll("[^a-zA-Z0-9]+","");
//                Pattern pattern = Pattern.compile("<\\w+");
//                Matcher matcher = pattern.matcher(responseBody);
//                while (matcher.find()) {
//                    if (!matcher.group().matches("<[A-Z]{2,}.*")) {
//                        responseBody = responseBody.replaceAll(matcher.group(), matcher.group().substring(0, 1) +
//                                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, matcher.group().substring(1)));
//                    }
//                }
//                pattern = Pattern.compile("</\\w+");
//                matcher = pattern.matcher(responseBody);
//                while (matcher.find()) {
//                    if (!matcher.group().matches("</[A-Z]{2,}.*")) {
//                        responseBody = responseBody.replaceAll(matcher.group(), matcher.group().substring(0, 2) +
//                                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, matcher.group().substring(2)));
//                    }
//                }
//
//                logger.info("responseBefore: {}", responseBody);
//                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//                InputSource inputSource = new InputSource();
//                inputSource.setCharacterStream(new StringReader(responseBody));
//                Document document = documentBuilder.parse(inputSource);
//
//                NodeList nodes = document.getElementsByTagName("custId");
//                document.renameNode(nodes.item(0), null, "custInfo");
//                nodes = document.getElementsByTagName("acctId");
//                document.renameNode(nodes.item(0), null, "acctInfo");
//                nodes = document.getElementsByTagName("name");
//                document.renameNode(nodes.item(0), null, "bankName");
//                nodes = document.getElementsByTagName("city");
//                document.renameNode(nodes.item(0), null, "addrCity");
//                nodes = document.getElementsByTagName("stateProv");
//                document.renameNode(nodes.item(0), null, "addrStateProv");
//
//                DOMSource domSource = new DOMSource(document);
//                Transformer transformer = TransformerFactory.newInstance().newTransformer();
//                StringWriter sw = new StringWriter();
//                StreamResult sr = new StreamResult(sw);
//                transformer.transform(domSource, sr);
//                responseBody = sw.toString();
//
//                InputStream inputStream = new ByteArrayInputStream(getBytes(responseBody));
//                JAXBContext jaxbContext = JAXBContext.newInstance(AcctInqResponse.class);
//
//                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//                acctInqResponse = (AcctInqResponse) jaxbUnmarshaller.unmarshal
//                        (new InputStreamReader(inputStream, "UTF-8"));
//
//
//                //acctInqResponse.setAcctBal(new ArrayList<AcctBal>(Arrays.asList(acctInqFiResponse.getAcctBal())));
//                // acctInqResponse.getODA().repl
//                //acctInqResponse.setCifId(acctInqRequest.getCifId());
//                acctInqResponse.setResponseCode("00");
//
//                String AOC = Helper.getAOC(lastNRequestdetails.getAccountNumber());
//                acctInqResponse.getAcctInfo().setAOC(AOC);
//                String custDOB = Helper.getCustDOB(lastNRequestdetails.getAccountNumber());
//                acctInqResponse.getCustInfo().setDateOfBirth(custDOB);
//                acctInqResponse.setResponseCode("00");
//                acctInqResponse.setAccountNumber(lastNRequestdetails.getAccountNumber());
//                acctInqResponse.setResponseMessage("AcctInq Successfully");
//
//
//            } catch (Exception e) {
//                acctInqResponse = new AcctInqResponse();
//                acctInqResponse.setErrorMessage("Error Processsing request");
//                this.logger.error("******************** FI encountered error. Error Description {} ********************", e.toString());
//                acctInqResponse.setResponseCode("96");
//                e.printStackTrace();
//            }
        } else {
            String errorCode = StringUtils.substringBetween(responseMsg, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMsg, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMsg, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMsg, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);
//            acctInqResponse = new AcctInqResponse();
            resp.setErrorMessage(errorMessage);
            resp.setRespCode("96");
            resp.setRequestUUID(lastNRequestdetails.getRequestUUID());
        }

        this.logger.info("**************************************** ACCT INQ  ENDED****************************************");
        return resp;
    }


    public LienUpdateResponse postLienUpdate(LienUpdateRequest lienUpdateRequest) {
        boolean involvesLienMod=false;
        boolean involvesLienPlcMent = false;
        LienUpdateResponse lienUpdateResponse = new LienUpdateResponse();
        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-DD");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
        this.context.put("RequestUUID", requestId);
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-DD'T'HH:mm:ss.SSS")).format(new Date()));
        ArrayList custInfo = new ArrayList();
        ArrayList partInf = new ArrayList();
        int i = 1;
        //Accounting Entries
        if(lienUpdateRequest.getAccInfo() != null && org.apache.commons.lang.StringUtils.isNotBlank((lienUpdateRequest.getAccInfo().get(0)).getForacid())) {
            Iterator i$ = lienUpdateRequest.getAccInfo().iterator();
            AccInfo ci;
            while(i$ != null && i$.hasNext()) {
                ci = (AccInfo)i$.next();
                if(ci.getForacid() != null) {
                    CustInfo4Vm ciVm = new CustInfo4Vm();
                    ciVm.setCreditDebitFlg(ci.getCreditDebitFlg());
                    ciVm.setAmountValue(ci.getTrnAmt().getAmountValue());
                    ciVm.setCurrencyCode(ci.getTrnAmt().getCurrencyCode());
                    ciVm.setAllowFalseDebit(ci.getAllowFalseDebit());
                    ciVm.setAllowFreezeAccnt(ci.getAllowFreezeAccnt());
                    ciVm.setForacid(ci.getForacid());
                    ciVm.setTrnParticulars(ci.getTrnParticulars());
                    ciVm.setTrnParticularCode(ci.getTranParticularCode());
                    ciVm.setValueDt(ci.getValueDt().replace("T", " "));
                    ciVm.setRate(ci.getRate().toUpperCase());
                    ciVm.setSn(i + "");
                    custInfo.add(ciVm);
                    ++i;
                }
            }

            ci = null;
            i$ = null;
            int accinfoLength = lienUpdateRequest.getAccInfo() == null?0:lienUpdateRequest.getAccInfo().size();

            for(int l = 0; l < accinfoLength; ++l) {
                PartitionInfo partitionDetails = new PartitionInfo();
                partitionDetails.setSn(l + 1);
                partitionDetails.setKasLoanNumber((lienUpdateRequest.getAccInfo().get(l)).getPartitionDetails().getKasLoanNumber());
                partitionDetails.setEntityType((lienUpdateRequest.getAccInfo().get(l)).getPartitionDetails().getEntityType());
                partitionDetails.setKasNumber((lienUpdateRequest.getAccInfo().get(l)).getPartitionDetails().getKasNumber());
                partInf.add(partitionDetails);
            }
        }
        this.context.put("numOfAccts", Integer.valueOf(custInfo.size()) == null?"0":Integer.valueOf(custInfo.size()));
        this.context.put("accInfo", custInfo == null?"":custInfo);
        this.context.put("partInfo", partInf == null?"":partInf);
        String responseMsg;
        //Lien Modification
        if(lienUpdateRequest.getLienInfo() != null && lienUpdateRequest.getLienInfo().getModifyLien() != null && StringUtils.isNotBlank(lienUpdateRequest.getLienInfo().getModifyLien().getLienID())) {
            involvesLienMod = true;
            this.context.put("lienId", lienUpdateRequest.getLienInfo().getModifyLien().getLienID());
            this.context.put("unlienAmt", lienUpdateRequest.getLienInfo().getModifyLien().getUnLienAmt());
            responseMsg = "";
            if(lienUpdateRequest.getLienInfo().getModifyLien().getNewExpDate() != null) {
                try {
                    responseMsg = dateFormat2.format(dateFormat1.parse(lienUpdateRequest.getLienInfo().getModifyLien().getNewExpDate()));
                } catch (ParseException var14) {
                    var14.printStackTrace();
                }
            }

            this.context.put("newExpDate", responseMsg);
            this.context.put("newLienRmks", lienUpdateRequest.getLienInfo().getModifyLien().getNewLienRmks() == null?"":lienUpdateRequest.getLienInfo().getModifyLien().getNewLienRmks());
        } else {
            this.context.put("lienId", "");
            this.context.put("unlienAmt", "");
            this.context.put("newExpDate", "");
            this.context.put("newLienRmks", "");
        }
        //Lien Placement
        if(lienUpdateRequest.getLienInfo() != null && lienUpdateRequest.getLienInfo().getFreshLien() != null && StringUtils.isNotBlank(lienUpdateRequest.getLienInfo().getFreshLien().getForacid())) {
            involvesLienPlcMent = true;
            this.context.put("foracid", lienUpdateRequest.getLienInfo().getFreshLien().getForacid());
            this.context.put("lienAmt", lienUpdateRequest.getLienInfo().getFreshLien().getLienAmt());
            this.context.put("lienReasonCode", lienUpdateRequest.getLienInfo().getFreshLien().getLienReasonCode());
            this.context.put("lienRemark", lienUpdateRequest.getLienInfo().getFreshLien().getLienRemark());

            responseMsg = lienUpdateRequest.getLienInfo().getFreshLien().getExpiredDate();

            try {
                responseMsg = dateFormat2.format(dateFormat1.parse(lienUpdateRequest.getLienInfo().getFreshLien().getExpiredDate()));
            } catch (ParseException var13) {
                var13.printStackTrace();
            }

            this.context.put("expiredDate", responseMsg);
        } else {
            this.context.put("lienAmt", "");
            this.context.put("foracid", "");
            this.context.put("expiredDate", "");
            this.context.put("lienRemark", "");
            this.context.put("lienReasonCode", "");
        }
        t = ve.getTemplate("FI-XML/LienUpdate.vm");
        StringWriter writer = new StringWriter();
        t.merge(this.context, writer);

        try {
            responseMsg = new FI().callService(writer.toString());
        } catch (Exception var12x) {
            var12x.printStackTrace();
            lienUpdateResponse.setErrorMessage(var12x.getMessage());
            lienUpdateResponse.setResponseCode("96");
            return null;
        }

        String resultMsg = StringUtils.substringBetween(responseMsg, "<RESULT_MSG>", "</RESULT_MSG>");
        boolean isSuccessful = responseMsg.contains("<Status>SUCCESS</Status>");
        String err;
        String resp;
        if(isSuccessful){
            lienUpdateResponse.setResponseCode("00");
            lienUpdateResponse.setErrorMessage("");
            String[] result = resultMsg.split("__");
            if(result[0].trim().length() > 0) {
                if(!result[0].contains("::N::")) {
                    resp = result[0].trim();
                    if(resp.equals("...")) {
                        lienUpdateResponse.setLienADDResult("");
                    } else {
                        lienUpdateResponse.setLienADDResult(resp);
                    }
                } else {
                    resp = result[0].substring(5);
                    if(resp.equals("...")) {
                        lienUpdateResponse.setLienADDErrMsg("");
                    } else {
                        lienUpdateResponse.setLienADDErrMsg(resp);
                        lienUpdateResponse.setErrorMessage("Same Lien Already Placed");
                    }

                    lienUpdateResponse.setResponseCode("96");
                }
            }

            if(result[1].trim().length() > 0) {
                if(result[1].contains("::N::")) {
                    resp = result[1].substring(5);
                    if(resp.equals("...")) {
                        lienUpdateResponse.setLienModErrMsg("");
                    } else {
                        lienUpdateResponse.setLienModErrMsg(resp);
                    }

                    lienUpdateResponse.setResponseCode("96");
                } else {
                    resp = result[1].trim();
                    if(resp.equals("...")) {
                        lienUpdateResponse.setLienModResult("");
                    } else {
                        lienUpdateResponse.setLienModResult(resp);
                    }
                }
            }

            if(result[2].trim().length() > 0) {
                if(result[2].contains("::N::")) {
                    resp = result[2].substring(5);
                    if(resp.equals("...")) {
                        lienUpdateResponse.setAcountingErrMsg("");
                    } else {
                        lienUpdateResponse.setAcountingErrMsg(resp);
                    }

                    lienUpdateResponse.setResponseCode("96");
                } else {
                    resp = result[2].trim();
                    if(resp.equals("...")) {
                        lienUpdateResponse.setAcountingResult("");
                    } else {
                        lienUpdateResponse.setAcountingResult(resp);
                    }
                }
            }
        } else {
            err = org.apache.commons.lang3.StringUtils.substringBetween(responseMsg, "<ErrorCode>", "</ErrorCode>");
            resp = org.apache.commons.lang3.StringUtils.substringBetween(responseMsg, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = org.apache.commons.lang3.StringUtils.substringBetween(responseMsg, "<ErrorSource>", "</ErrorSource>");
            String errorType = org.apache.commons.lang3.StringUtils.substringBetween(responseMsg, "<ErrorType>", "</ErrorType>");
            String errorMessage = org.apache.commons.lang3.StringUtils.replaceEach("RequestUUID: " + requestId + " | ErrorCode: " + err + " | ErrorDesc: " + resp + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            lienUpdateResponse.setResponseCode("96");
            if(errorMessage.toLowerCase().contains("runtime")&&involvesLienMod){
                lienUpdateResponse.setErrorMessage("Finacle System Error Occurred while Modifying Lien, Kindly modify from Finacle menu or Retry Later");
            }else if(errorMessage.toLowerCase().contains("runtime") && !involvesLienMod && involvesLienPlcMent){
                lienUpdateResponse.setErrorMessage("Finacle System Error Occurred while Placing Lien, Kindly add from Finacle menu or Retry Later");
            }else if(errorMessage.toLowerCase().contains("runtime") && !involvesLienMod && !involvesLienPlcMent){
                lienUpdateResponse.setErrorMessage("Finacle System Error Occurred during Accounting Kindly Retry Later");
            }else{
                lienUpdateResponse.setErrorMessage(errorMessage);
            }
            if(errorMessage.contains("CARD AVAILABLE FOR CU")){
                lienUpdateResponse.setResponseCode("96");
                lienUpdateResponse.setErrorMessage("Please Check Transaction Status From DB error from Finacle is: "+errorMessage);
            }
            lienUpdateResponse.setAcountingResult("");
            lienUpdateResponse.setLienADDResult("");
            lienUpdateResponse.setLienModResult("");
            lienUpdateResponse.setLoanRepayResult("");
        }

        lienUpdateResponse.setRequestId(lienUpdateRequest.getRequestId());
        return lienUpdateResponse;
    }


    public String fetchCustBVN(String custID) {
        this.logger.info("*******************INITIATING BVN Fetch...**********************");
        String bvnString = "";
        StringWriter writer = new StringWriter();
        this.t = this.ve.getTemplate("FI-XML/FetchBvn.vm");
        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", requestId);
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("custID", custID);
        this.t.merge(this.context, writer);
        String payload = writer.toString();
        String responseMsg = this.callService(payload);
        String charSequence = "<Status>SUCCESS</Status>";

        boolean isSuccessful = responseMsg.contains(charSequence);
        if (isSuccessful) {
            bvnString = StringUtils.substringBetween(responseMsg, "<RESULT_MSG>", "</RESULT_MSG>");
        }
        this.logger.info("**************************************** Fetch BVN Ended with resp**************************************** {}",bvnString);
        return bvnString;
    }


    public AcctClosureResponse postAcctClosure(AcctClosureRequest acctClosureRequest) {
        this.logger.info("**************************************** BEGIN BRANCH TRANSFER ****************************************");

        AcctClosureResponse response = new AcctClosureResponse();
        StringWriter writer = new StringWriter();
        //LocalDateTime startDate = LocalDateTime.parse(this.getFIDate());
        this.t = this.ve.getTemplate("FI-XML/AccountClosure.vm");
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", "Req_" + RandomStringUtils.randomNumeric(13));
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("foracid", acctClosureRequest.getAccountNumber());
        //  this.context.put("ToSOL", transferDetails.getToSoL());


        this.t.merge(this.context, writer);
        String payload11 = writer.toString();
        String responseMessage = this.callService(payload11);

        String resultMsg = StringUtils.substringBetween(responseMessage, "<RESULT_MSG>", "</RESULT_MSG>");
        boolean isSuccessful = responseMessage.contains("<Status>SUCCESS</Status>");
        if (isSuccessful) {

            response.setResponseCode("00");
            response.setResponseMessage(resultMsg);
            this.logger.error("******************** Account Closure Successfully ********************");

        } else {
            String errorCode = StringUtils.substringBetween(responseMessage, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMessage, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMessage, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMessage, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId211 + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);
            response.setErrorMessage(errorMessage);
            response.setResponseCode("96");
        }

        this.logger.info("****************************************  ACCOUNT CLOSURE  ENDED****************************************");
        return response;
    }


    public LoanTopupResponse postLoanTopup(LoanTopupRequest loanTopupRequest) {
        this.logger.info("**************************************** BEGIN BRANCH TRANSFER ****************************************");

        LoanTopupResponse response = new LoanTopupResponse();
        StringWriter writer = new StringWriter();
        //LocalDateTime startDate = LocalDateTime.parse(this.getFIDate());
        this.t = this.ve.getTemplate("FI-XML/LoanTopup.vm");
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", "Req_" + RandomStringUtils.randomNumeric(13));
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("foracid", loanTopupRequest.getAccountNumber());
        this.context.put("topup", loanTopupRequest.getTopup());
        //  this.context.put("ToSOL", transferDetails.getToSoL());


        this.t.merge(this.context, writer);
        String payload11 = writer.toString();
        String responseMessage = this.callService(payload11);

        String resultMsg = StringUtils.substringBetween(responseMessage, "<RESULT_MSG>", "</RESULT_MSG>");
        boolean isSuccessful = responseMessage.contains("<Status>SUCCESS</Status>");
        if (isSuccessful) {

            response.setResponseCode("00");
            response.setResponseMessage(resultMsg);
            this.logger.error("******************** Account Closure Successfully ********************");

        } else {
            String errorCode = StringUtils.substringBetween(responseMessage, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMessage, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMessage, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMessage, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId211 + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);
            response.setErrorMessage(errorMessage);
            response.setResponseCode("96");
        }

        this.logger.info("****************************************  ACCOUNT CLOSURE  ENDED****************************************");
        return response;
    }


    public DrwPowerResponse postDrawPower(DrawPowerRequest request) {
        this.logger.info("**************************************** BEGIN DRAW POWER PROCESSING ****************************************");

        DrwPowerResponse response = new DrwPowerResponse();
        StringWriter writer = new StringWriter();
        this.t = this.ve.getTemplate("FI-XML/DrawingPower.vm");
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", "Req_" + RandomStringUtils.randomNumeric(13));
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("foracid", request.getForacid());
        this.context.put("AppDate", request.getAppDate());
        this.context.put("drwngPowerIndCode", request.getDrwngPowerIndCode());
        this.context.put("event", request.getEvent());
        this.context.put("drwngPower", request.getDrwngPower());
        this.context.put("remark", request.getRemark());


        this.t.merge(this.context, writer);
        String payload11 = writer.toString();
        String responseMessage = this.callService(payload11);

        String resultMsg = StringUtils.substringBetween(responseMessage, "<RESULT_MSG>", "</RESULT_MSG>");
        boolean isSuccessful = responseMessage.contains("<Status>SUCCESS</Status>");
        if (isSuccessful) {

            response.setResponseCode("00");
            response.setResponseMessage(resultMsg);
            this.logger.error("******************** Account Closure Successfully ********************");

        } else {
            String errorCode = StringUtils.substringBetween(responseMessage, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMessage, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMessage, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMessage, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId211 + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);
            response.setErrorMessage(errorMessage);
            response.setResponseCode("96");
        }

        this.logger.info("****************************************  ACCOUNT CLOSURE  ENDED****************************************");
        return response;
    }


    public FIResponse postBulkPayment(BulkPaymentRequest details) {
        this.logger.info("**************************************** BEGIN PAYMENT PROCESS ****************************************");
        BulkPaymentResponse response = new BulkPaymentResponse();

        StringWriter writer = new StringWriter();
        Date startDate = new Date();
        //Date.parse(this.getFIDate());
        //String presentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(startDate);
        //boolean isCorpLoan = Helper.getAccountType(details.getDebitAcctNumber());
        FIResponse fiResponse = new FIResponse();
        String opAcct = Helper.getOperatingAcct(details.getLoanAcctNum());
        if (StringUtils.isEmpty(opAcct) || opAcct == null) {
            fiResponse.setIsSuccessful(false);
            String errorMessage = "Operative account for Loan Account Not Found";
            fiResponse.setResponse(errorMessage);
            response.setResponseCode("96");
            this.logger.info("****************************************{}****************************************", errorMessage);
            this.logger.info("**************************************** PAYMENT PROCESS  ENDED****************************************");
            return fiResponse;
        }
        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", requestId);
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
//        if(isCorpLoan){
//            this.t = this.ve.getTemplate("FI-XML/corporateLoanDisbursment.vm");
//            this.context.put("valueDate",presentDate );
//            this.context.put("acctNumber", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(startDate)));
//            this.context.put("drawdownAmt", details.getAmount());
//            this.context.put("creditAcctNumber", "opacid");
//            this.context.put("remarks", "Loan DisburseMent");
//        }else{
//            this.t = this.ve.getTemplate("FI-XML/loanDisbursement.vm");
//            this.context.put("glDate", presentDate);
//            this.context.put("crValueDate", "101");
//            this.context.put("TrnDt", "101");
//            this.context.put("crValueDate", "101");
//
//            this.context.put("amtAlreadyDisbursedAmountValue", details.getAmount());
//            this.context.put("amtAlreadyDisbursedCurrencyCode", details.getAmount());
//            this.context.put("laAmtCrncyAmountValue", "");
//            this.context.put("laAmtCrncyCurrencyCode", details.getAmount());
//            this.context.put("crncyCode", "NGN");
//            this.context.put("disburseAmtAmountValue", "101");
//            this.context.put("disburseAmtCurrencyCode", presentDate);
//            this.context.put("laAcctCrncyCode", details.getAmount());
//            this.context.put("laAcctForacid", details.getAmount());
//            this.context.put("netDisbursalAmtAmountValue", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(startDate)));
//            this.context.put("netDisbursalAmtCurrencyCode", details.getAmount());
//            this.context.put("crAcctForAcid", "NGN");
//            this.context.put("amtAvailForDisbursementAmountValue", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(startDate)));
//            this.context.put("amtAvailForDisbursementCurrencyCode", details.getAmount());
//            this.context.put("solId", "NGN");
//
//        }
        this.t = this.ve.getTemplate("FI-XML/BulkPayment.vm");
        this.context.put("crLoanAcct", details.getLoanAcctNum());
        this.context.put("dbOpAcct", opAcct);
        this.context.put("crAmount", details.getAmount());

        this.t.merge(this.context, writer);
        String payload11 = writer.toString();
        String responseMessage = this.callService(payload11);
        String charSequence1 = "<Status>SUCCESS</Status>";
        String resultMssg = StringUtils.substringBetween(responseMessage, "<RESULT_MSG>", "</RESULT_MSG>");
        String[] resComp = resultMssg.split("_");
        resultMssg = resComp[0];
        String msgDesrciption = resComp[1];
        boolean isSuccessful = responseMessage.contains(charSequence1);

        if (isSuccessful) {
            fiResponse.setIsSuccessful(true);
            fiResponse.setResponse(resultMssg + "_" + msgDesrciption);
            response.setResponseCode("00");

            this.logger.info("**************************************** success ****************************************");
        } else {
            String errorCode = StringUtils.substringBetween(responseMessage, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMessage, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMessage, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMessage, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("****** FI encountered error. Error Description {} *****", errorMessage);
            fiResponse.setIsSuccessful(false);
            fiResponse.setResponse(errorMessage);
            response.setResponseCode("96");
        }

        this.logger.info("**************************************** PAYMENT PROCESS  ENDED****************************************");
        return fiResponse;
    }


    public FIResponse postBranchTransfer(BranchTransferRequest transferDetails) {
        this.logger.info("**************************************** BEGIN BRANCH TRANSFER ****************************************");
        BranchTransferResponse response = new BranchTransferResponse();

        StringWriter writer = new StringWriter();
        //LocalDateTime startDate = LocalDateTime.parse(this.getFIDate());
        this.t = this.ve.getTemplate("FI-XML/BranchTransfer.vm");
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", "Req_" + RandomStringUtils.randomNumeric(13));
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("foracid", transferDetails.getAccountNumber());
        this.context.put("ToSOL", transferDetails.getToSoL());


        this.t.merge(this.context, writer);
        String payload11 = writer.toString();
        String responseMessage = this.callService(payload11);

        String resultMssg = StringUtils.substringBetween(responseMessage, "<RESULT_MSG>", "</RESULT_MSG>");
        String[] resComp = resultMssg.split("_");
        resultMssg = resComp[0];

        boolean isMoved = resultMssg.contains("SUCCESSFUL");
        boolean isSuccessful = responseMessage.contains("<Status>SUCCESS</Status>");
        FIResponse fiResponse = new FIResponse();
        if (isSuccessful) {


            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String currentSol = Helper.getCustomerSOL(transferDetails.getAccountNumber());
            if (currentSol.equals(transferDetails.getToSoL())) {
                String fromSOL = resComp[1];
                fiResponse.setIsSuccessful(true);
                fiResponse.setResponse(resultMssg + "_" + fromSOL);
                response.setResponseCode(fromSOL);
                this.logger.info("**************************************** success ****************************************");
            } else {
                String errorMessage = "Account movement NOT successful";
                this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);
                fiResponse.setResponse(errorMessage);
                response.setResponseCode("96");
            }

        } else {
            String errorCode = StringUtils.substringBetween(responseMessage, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMessage, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMessage, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMessage, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId211 + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);
            fiResponse.setResponse(errorMessage);
            response.setResponseCode("96");
        }

        this.logger.info("**************************************** BRANCH TRANSFER  ENDED****************************************");
        return fiResponse;
    }


    public ExpCheckResponse postCheckExposure(ExpCheckRequest expDetails) {
        this.logger.info("**************************************** BEGIN EXPOSURE CHECK ENQUIRY ****************************************");

        StringWriter writer = new StringWriter();
        //LocalDateTime startDate = LocalDateTime.parse(this.getFIDate());

        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", "Req_" + requestId);
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("cifID", expDetails.getCustomerID());

        this.t = this.ve.getTemplate("FI-XML/ExpCheck.vm");
        this.t.merge(this.context, writer);
        String payload11 = writer.toString();
        String responseMessage = this.callService(payload11);
        String charSequence1 = "<Status>SUCCESS</Status>";
        //String responseMessage = "<Status>SUCCESS</Status><RESULT_MSG>3456789|23434342422242424|20160129|24500|20190812|101|34000|MAS203|4500|NGN|2300|NGN|4|0|P|0|2</RESULT_MSG>";
        boolean isSuccessful = responseMessage.contains(charSequence1);
        String components = StringUtils.substringBetween(responseMessage, "<RESULT_MSG>", "</RESULT_MSG>");
        ExpCheckResponse_old expResponse = new ExpCheckResponse_old();
        ExpCheckResponse expResponse2 = new ExpCheckResponse();
        if (isSuccessful) {
            String Status = "";
            String[] resultComp;
            String[] resultOne;

            if (components.contains("_") && components.split("_").length >= 10) {

                Status = "SUCCESS";
                expResponse2.setResponseMessage("SUCCESS");
                expResponse2.setResponseCode("00");
                expResponse2.setCustomerID(expDetails.getCustomerID());
                expResponse2.setResultInfos(new ArrayList<ExpChechInfo>());
                if (components.contains("___")) {
                    resultOne = components.split("___");
                } else {
                    resultOne = new String[]{components};
                }
                for (String oneRecord : resultOne) {

                    ExpChechInfo resultInfo = new ExpChechInfo();
                    resultComp = oneRecord.split("_");
                    //<editor-fold desc="Description">
                    /**
                     expResponse.setServicingBranch(resultComp[0]);
                     expResponse.setCustomerloanAccNo(resultComp[2]);
                     expResponse.setCustRelationship(resultComp[1]);
                     expResponse.setCreditLimit(resultComp[4]);
                     expResponse.setLoanRefCreationDate(resultComp[5]);
                     expResponse.setProductCode(resultComp[6]);
                     expResponse.setBalanceOutstanding(resultComp[7]);
                     expResponse.setBalanceOutstandingCur(resultComp[8]);
                     expResponse.setInstallmentAmtCurrency(resultComp[8]);
                     expResponse.setInstallmentAmt(resultComp[9]);
                     expResponse.setInstallmentFrequency(resultComp[10]);
                     expResponse.setExpiryDate(resultComp[11]);
                     expResponse.setTotalSecurityValAttached(resultComp[12]);
                     expResponse.setTenor(resultComp[13]);
                     expResponse.setApprovedLimit(resultComp[4]);
                     expResponse.setAccConduct(resultComp[14]);

                     //// **/
                    //</editor-fold>
                    //<editor-fold desc="Description">
                    resultInfo.setAccountOfficerCode(resultComp[0]);
                    resultInfo.setAccountOpenDate(resultComp[1]);
                    resultInfo.setAccountStatus(resultComp[2]);
                    resultInfo.setCurrency(resultComp[3]);
                    resultInfo.setForacid(resultComp[4]);
                    resultInfo.setMaturityDate(resultComp[5]);
                    resultInfo.setODLimit(resultComp[6]);
                    resultInfo.setOutstandingBalance(resultComp[7]);
                    resultInfo.setSchmCode(resultComp[8]);
                    resultInfo.setServicingBranch(resultComp[9]);
                    //</editor-fold>
                    expResponse2.getResultInfos().add(resultInfo);
                }

            } else {
                Status = "FAILED";
                expResponse2.setErrorMessage(components);
                expResponse2.setResponseMessage(Status);
                expResponse2.setResponseCode("96");
            }

            this.logger.info("**************************************** {} ****************************************", Status);
        } else {
            String errorCode = StringUtils.substringBetween(responseMessage, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMessage, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMessage, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMessage, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID:  " + requestId + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);

            expResponse2.setErrorMessage(errorMessage);
            expResponse2.setResponseCode("96");
        }

        this.logger.info("**************************************** EXPOSURE CHECK ENQUIRY ENDED****************************************");
        return expResponse2;
    }


    public FIResponse fetchAverageBalance(AverageBalanceRequest balanceDetails) {
        this.logger.info("**************************************** BEGIN AVERAGE BALANCE ENQUIRY ****************************************");

        StringWriter writer = new StringWriter();
        this.t = this.ve.getTemplate("FI-XML/AverageBal.vm");
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", "Req_" + RandomStringUtils.randomNumeric(13));
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("foracid", balanceDetails.getAccountNumber());
        this.context.put("Years", balanceDetails.getnoOfYears());


        this.t.merge(this.context, writer);
        String payload11 = writer.toString();
        String responseMessage = this.callService(payload11);
        //String responseMessage ="<Status>SUCCESS</Status><RESULT_MSG>45999900</RESULT_MSG>";
        String charSequence1 = "<Status>SUCCESS</Status>";

        boolean isSuccessful = responseMessage.contains(charSequence1);
        FIResponse fiResponse = new FIResponse();
        if (isSuccessful) {
            String AvgBalance = StringUtils.substringBetween(responseMessage, "<RESULT_MSG>", "</RESULT_MSG>");
            boolean avgBalFetched = AvgBalance.matches(".*[a-zA-Z].*");

            this.logger.info("**************************************** contains alphabets{} ****************************************", avgBalFetched);
            if (avgBalFetched) {
                fiResponse.setIsSuccessful(false);
                fiResponse.setResponse(AvgBalance);
                this.logger.info("**************************************** failed ****************************************");
            } else {
                fiResponse.setIsSuccessful(true);
                fiResponse.setResponse(AvgBalance);
                this.logger.info("**************************************** success ****************************************");
            }

        } else {
            String errorCode = StringUtils.substringBetween(responseMessage, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMessage, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMessage, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMessage, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId211 + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("********************  FI encountered error. Error Description {} ********************", errorMessage);
            fiResponse.setResponse(errorMessage);
        }

        this.logger.info("**************************************** AVERAGE BALANCE ENQUIR ENDED****************************************");
        return fiResponse;
    }


    public KasEodResponse checkEODStatus(String processID,String eodType) {
        this.logger.info("**************************************** Accruals STATUS ENQUIRY ****************************************");
        KasEodResponse  kasEodResponse = new KasEodResponse();
        BulkPaymentResponse response = new BulkPaymentResponse();

        StringWriter writer = new StringWriter();
        this.t = this.ve.getTemplate("FI-XML/checkEOD.vm");
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", "Req_" + RandomStringUtils.randomNumeric(13));
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("processID",processID);
        this.context.put("eodType",eodType);

        this.t.merge(this.context, writer);
        String payload11 = writer.toString();
        this.logger.info("Request is: "+payload11);
        String responseMessage = this.callService(payload11);
        String charSequence1 = "<Status>SUCCESS</Status>";
        this.logger.info("Response is: "+responseMessage);
        boolean isSuccessful = responseMessage.contains(charSequence1);
        //FIResponse fiResponse = new FIResponse();
        if (isSuccessful){
            String inQueue = StringUtils.substringBetween(responseMessage, "<inQueue>", "</inQueue>");
            String notProcessed = StringUtils.substringBetween(responseMessage, "<notProcessed>", "</notProcessed>");
            String failed = StringUtils.substringBetween(responseMessage, "<failed>", "</failed>");
            String successfull = StringUtils.substringBetween(responseMessage, "<successfull>", "</successfull>");
            String eodStat = StringUtils.substringBetween(responseMessage, "<RESULT_MSG>", "</RESULT_MSG>");
            String stalkedProc = StringUtils.substringBetween(responseMessage, "<stalked>", "</stalked>");
            String totalRecord = StringUtils.substringBetween(responseMessage, "<totalRecord>", "</totalRecord>");
            kasEodResponse.setFailed(failed);
            kasEodResponse.setNotProcessed(notProcessed);
            kasEodResponse.setSuccessFullyPosted(successfull);
            kasEodResponse.setInQueue(inQueue);
            kasEodResponse.setResponseDesc(eodStat);
            kasEodResponse.setStalkedProcess(stalkedProc);
            kasEodResponse.setResponseCode("00");
            kasEodResponse.setTotalRecord(totalRecord);
        } else {
            String errorCode = StringUtils.substringBetween(responseMessage, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMessage, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMessage, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMessage, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId211 + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);
            kasEodResponse.setResponseDesc(errorMessage);
            kasEodResponse.setResponseCode("96");
        }
        this.logger.info("**************************************** Accruals STATUS ENQUIRy ENDED****************************************");
        return kasEodResponse;
    }


    public TDAcctInqResponse postTDAcctInq(AcctInqRequest acctInqRequest) {
        this.logger.info("*******************INITIATING ACCOUNT INQUIRY...**********************");
        this.logger.info("request {}", acctInqRequest);
        StringWriter writer = new StringWriter();

        this.t = this.ve.getTemplate("FI-XML/TDAccountInq.vm");
        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", requestId);

        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("AcctInfo", acctInqRequest.getAccountNumber());


        this.t.merge(this.context, writer);
        String payload = writer.toString();
        String responseMsg = this.callService(payload);
        String charSequence = "<Status>SUCCESS</Status>";
        boolean isSuccessful = responseMsg.contains(charSequence);
        TDAcctInqResponse tdAcctInqResponse;
        if (isSuccessful) {
            this.logger.info("****************************************SUCCESS ****************************************");

            try {
                String responseBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
                responseBody = responseBody + "<tdAcctInqResponse>\n";
                responseBody = responseBody + StringUtils.substringBetween(responseMsg, "<TDAcctInqRs>", "</TDAcctInqRs>").trim();
                responseBody = responseBody + "\n</tdAcctInqResponse>";

                Pattern pattern = Pattern.compile("<\\w+");
                Matcher matcher = pattern.matcher(responseBody);
                while (matcher.find()) {
                    if (!matcher.group().matches("<[A-Z]{2,}.*")) {
                        responseBody = responseBody.replaceAll(matcher.group(), matcher.group().substring(0, 1) +
                                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, matcher.group().substring(1)));
                    }
                }
                pattern = Pattern.compile("</\\w+");
                matcher = pattern.matcher(responseBody);
                while (matcher.find()) {
                    if (!matcher.group().matches("</[A-Z]{2,}.*")) {
                        responseBody = responseBody.replaceAll(matcher.group(), matcher.group().substring(0, 2) +
                                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, matcher.group().substring(2)));
                    }
                }
                InputStream inputStream = new ByteArrayInputStream(getBytes(responseBody));
                JAXBContext jaxbContext = JAXBContext.newInstance(TDAcctInqResponse.class);

                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                tdAcctInqResponse = (TDAcctInqResponse) jaxbUnmarshaller.unmarshal(inputStream);
                this.logger.info("{}",tdAcctInqResponse);

                tdAcctInqResponse.getTDAcctId().setBankInfo(Helper.getBankInfo(acctInqRequest.getAccountNumber()));
                tdAcctInqResponse.getRepayAcctId().setBankInfo(
                        Helper.getBankInfo(tdAcctInqResponse.getRepayAcctId().getAcctId()));

                tdAcctInqResponse.setResponseCode("00");
                tdAcctInqResponse.setResponseMessage("AcctInq Successfully");


            } catch (JAXBException e) {
                tdAcctInqResponse = new TDAcctInqResponse();
                tdAcctInqResponse.setErrorMessage("Error Processsing request");
                this.logger.error("******************** FI encountered error. Error Description {} ********************", e.toString());
                tdAcctInqResponse.setResponseCode("96");
                e.printStackTrace();
            }
        } else {
            String errorCode = StringUtils.substringBetween(responseMsg, "<ErrorCode>", "</ErrorCode>");
            String errorDesc = StringUtils.substringBetween(responseMsg, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMsg, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMsg, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId + " | ErrorCode: " + errorCode + " | ErrorDesc: " + errorDesc + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            this.logger.error("******************** FI encountered error. Error Description {} ********************", errorMessage);
            tdAcctInqResponse = new TDAcctInqResponse();
            tdAcctInqResponse.setErrorMessage(errorMessage);
            tdAcctInqResponse.setResponseCode("96");
            // tdAcctInqResponse.setRequestID(acctInqRequest.getRequestID());
        }

        this.logger.info("**************************************** ACCT INQ  ENDED****************************************");
        return tdAcctInqResponse;
    }



    public String getPropertyFromFile(String property) {
        Properties prop = new Properties();
        InputStream input = null;
        String value = "";

        try {
            input = this.getClass().getResourceAsStream("/FiUrl.properties");
            prop.load(input);
            value = prop.getProperty(property);
            this.logger.info("Property : {} \t Value : {}", property, value);
        } catch (IOException var14) {
            this.logger.error("Error in reading props file {}", var14.getLocalizedMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException var13) {
                    this.logger.error("error in closing input stream {}", var13.toString());
                }
            }

        }

        return value;
    }


    public String callService(String payload) {
        try {
            this.logger.info("****************************************FI REQUEST**************************************** {}", payload);
            KeyStore var12 = KeyStore.getInstance(KeyStore.getDefaultType());
            var12.load( null,  null);
            MySSLSocketFactory sf = new MySSLSocketFactory(var12);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            BasicHttpParams params = new BasicHttpParams();
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));
            ThreadSafeClientConnManager ccm = new ThreadSafeClientConnManager(params, registry);
            DefaultHttpClient httpClient = new DefaultHttpClient(ccm, params);
            ByteArrayEntity httpEntity = new ByteArrayEntity(payload.getBytes("UTF-8"));
            HttpPost httpPost = new HttpPost(this.FI_SERVICE_URL);
            httpPost.addHeader("SOAPAction", "");
            httpPost.setEntity(httpEntity);
            CloseableHttpResponse response = httpClient.execute(httpPost);
            String responseMessage = EntityUtils.toString(response.getEntity());
            if (responseMessage != null && responseMessage.length() > 0) {
                responseMessage = responseMessage.replace("&lt;", "<").replace("&gt;", ">");
            }

            this.logger.info("****************************************FI RESPONSE**************************************** {}", responseMessage);
            return responseMessage;
        } catch (Exception exception) {
            this.logger.error("Problem occurred in reaching fi {}", exception);
            return "";
        }
    }
}
