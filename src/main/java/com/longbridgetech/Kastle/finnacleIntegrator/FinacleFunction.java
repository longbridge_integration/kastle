//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.finnacleIntegrator;

import com.longbridgetech.Kastle.domain.FIResponse;

public interface FinacleFunction {
    String generateXml();

    FIResponse execute();
}
