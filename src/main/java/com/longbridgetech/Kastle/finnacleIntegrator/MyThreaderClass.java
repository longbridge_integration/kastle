//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.finnacleIntegrator;

import com.longbridgetech.Kastle.Entities.LienUpdAudit;
import com.longbridgetech.Kastle.domain.lienUpdate.AccInfo;
import com.longbridgetech.Kastle.domain.lienUpdate.CustInfo4Vm;
import com.longbridgetech.Kastle.domain.lienUpdate.LienUpdateRequest;
import com.longbridgetech.Kastle.domain.lienUpdate.LienUpdateResponse;
import com.longbridgetech.Kastle.domain.lienUpdate.PartitionInfo;
import com.longbridgetech.Kastle.persistence.LienUpdAuditDAOImpl;

import java.io.InputStream;
import java.io.StringWriter;
import java.security.KeyStore;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyThreaderClass  implements Runnable {
    private Logger logger = LoggerFactory.getLogger(MyThreaderClass.class);
    private LienUpdateRequest lienUpdateRequest;
    private LienUpdateResponse lienUpdateResponse;
    private int threadCnt;
    private boolean running = false;
    private String resultMsg;
    private LienUpdAuditDAOImpl transactnAuditDAO;
    private LienUpdAudit audit;
    private ArrayList custInfo;
    private ArrayList partInf;
    private VelocityEngine ve;
    private Template t;
    private VelocityContext context;

    public boolean isRunning() {
        return this.running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public LienUpdateResponse getLienUpdateResponse() {
        return this.lienUpdateResponse;
    }

    public void setLienUpdateResponse(LienUpdateResponse lienUpdateResponse) {
        this.lienUpdateResponse = lienUpdateResponse;
    }

    public MyThreaderClass(LienUpdateRequest lienUpdateRequest, int threadCnt) {
        this.lienUpdateRequest = lienUpdateRequest;
        this.lienUpdateResponse = new LienUpdateResponse();
        this.running = true;
        this.threadCnt = threadCnt;
        Properties props = new Properties();
        props.put("resource.loader", "class");
        props.put("runtime.log.logsystem.class","org.apache.velocity.runtime.log.NullLogSystem");
        props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        this.ve = new VelocityEngine();
        this.ve.init(props);
        this.context = new VelocityContext();
    }

    public void run() {
        this.transactnAuditDAO = new LienUpdAuditDAOImpl();
        this.audit = new LienUpdAudit(this.lienUpdateRequest);
        this.audit.setRequestDate(new Timestamp((new Date()).getTime()));
        this.logger.info("*******************INITIATING LIEN UPDATE FOR  Thread  {} ****Starts at {}******************", Integer.valueOf(this.threadCnt), new Timestamp((new Date()).getTime()));
        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", requestId);
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")).format(new Date()));
        this.custInfo = new ArrayList();
        int i = 1;

        if(this.lienUpdateRequest.getAccInfo()!=null){
            Iterator i$ = this.lienUpdateRequest.getAccInfo().iterator();
            AccInfo ci;
            while(i$!=null && i$.hasNext()) {
                ci = (AccInfo)i$.next();
                if(ci.getForacid() != null) {
                    CustInfo4Vm ciVm = new CustInfo4Vm();
                    ciVm.setForacid(ci.getForacid());
                    ciVm.setCreditDebitFlg(ci.getCreditDebitFlg());
                    ciVm.setAmountValue(ci.getTrnAmt().getAmountValue());
                    ciVm.setCurrencyCode(ci.getTrnAmt().getCurrencyCode());
                    ciVm.setAllowFreezeAccnt(ci.getAllowFreezeAccnt());
                    ciVm.setAllowFalseDebit(ci.getAllowFalseDebit());
                    ciVm.setTrnParticulars(ci.getTrnParticulars());
                    ciVm.setValueDt(ci.getValueDt().replace("T", " "));
                    ciVm.setRate(ci.getRate().toUpperCase());
                    ciVm.setSn(i + "");
                    this.custInfo.add(ciVm);
                    ++i;
                }
            }
            ci = null;
            int accinfoLength = this.lienUpdateRequest.getAccInfo()== null?0:this.lienUpdateRequest.getAccInfo().size();
            this.partInf = new ArrayList();
            this.logger.info("accinfo length is " + accinfoLength);

            for(int l = 0; l < accinfoLength; ++l) {
                PartitionInfo partitionDetails = new PartitionInfo();
                partitionDetails.setEntityType(((AccInfo)this.lienUpdateRequest.getAccInfo().get(l)).getPartitionDetails().getEntityType());
                partitionDetails.setKasNumber(((AccInfo)this.lienUpdateRequest.getAccInfo().get(l)).getPartitionDetails().getKasNumber());
                partitionDetails.setKasLoanNumber(((AccInfo)this.lienUpdateRequest.getAccInfo().get(l)).getPartitionDetails().getKasLoanNumber());
                partitionDetails.setSn(l + 1);
                this.partInf.add(partitionDetails);
            }

        }
        this.context.put("numOfAccts", Integer.valueOf(this.custInfo.size())==null?"0":Integer.valueOf(this.custInfo.size()));
        this.context.put("accInfo", this.custInfo==null?"":this.custInfo);
        this.context.put("partInfo", this.partInf==null?"":this.partInf);

        if(this.lienUpdateRequest.getLienInfo()!= null && this.lienUpdateRequest.getLienInfo().getModifyLien()!=null){
            this.context.put("unlienAmt", this.lienUpdateRequest.getLienInfo().getModifyLien().getUnLienAmt());
            this.context.put("lienId", this.lienUpdateRequest.getLienInfo().getModifyLien().getLienID());
        }else{
            this.context.put("unlienAmt","");
            this.context.put("lienId","");
        }
        if(this.lienUpdateRequest.getLienInfo()!= null && this.lienUpdateRequest.getLienInfo().getFreshLien()!=null){
            this.context.put("lienAmt", this.lienUpdateRequest.getLienInfo().getFreshLien().getLienAmt());
            this.context.put("foracid", this.lienUpdateRequest.getLienInfo().getFreshLien().getForacid());
            this.context.put("lienReasonCode", this.lienUpdateRequest.getLienInfo().getFreshLien().getLienReasonCode());
            this.context.put("lienRemark", this.lienUpdateRequest.getLienInfo().getFreshLien().getLienRemark());
            this.context.put("expiredDate", this.lienUpdateRequest.getLienInfo().getFreshLien().getExpiredDate());
        }else{
            this.context.put("lienAmt", "");
            this.context.put("foracid", "");
            this.context.put("lienReasonCode", "");
            this.context.put("lienRemark", "");
            this.context.put("expiredDate", "");

        }



        StringWriter writer = new StringWriter();
        this.t = this.ve.getTemplate("LienUpdate.vm");
        if(this.t != null) {
            this.t.merge(this.context, writer);
        }


        String responseMsg;
        try {
            responseMsg = this.callService(writer.toString(), this.threadCnt);
        } catch (Exception var12x) {
            var12x.printStackTrace();
            this.lienUpdateResponse.setResponseCode("96");
            this.lienUpdateResponse.setErrorMessage(var12x.getMessage());
            this.running = false;
            return;
        }

//        this.custInfo = null;
//        this.partInf = null;
//        requestId = null;
        this.resultMsg = StringUtils.substringBetween(responseMsg, "<RESULT_MSG>", "</RESULT_MSG>");
        boolean isSuccessful = responseMsg.contains("<Status>SUCCESS</Status>");
        String status;
        String resp;
        String err;
        if(isSuccessful) {
            this.lienUpdateResponse.setResponseCode("00");
            this.lienUpdateResponse.setErrorMessage("");
            String[] result = this.resultMsg.split("__");
            System.out.println(result[0] + " " + result[1] + " " + result[2] + " " + result[3]);
            if(result[0].trim().length() > 0) {
                if(!result[0].contains("::N::")) {
                    resp = result[0].trim();
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setLienADDResult("");
                    } else {
                        this.lienUpdateResponse.setLienADDResult(resp);
                    }
                } else {
                    resp = result[0].substring(5);
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setLienADDErrMsg("");
                    } else {
                        this.lienUpdateResponse.setLienADDErrMsg(resp);
                    }

                    this.lienUpdateResponse.setResponseCode("96");
                }
            }

            if(result[1].trim().length() > 0) {
                if(result[1].contains("::N::")) {
                    resp = result[1].substring(5);
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setLienModErrMsg("");
                    } else {
                        this.lienUpdateResponse.setLienModErrMsg(resp);
                    }

                    this.lienUpdateResponse.setResponseCode("96");
                } else {
                    resp = result[1].trim();
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setLienModResult("");
                    } else {
                        this.lienUpdateResponse.setLienModResult(resp);
                    }
                }
            }

            if(result[2].trim().length() > 0) {
                if(result[2].contains("::N::")) {
                    resp = result[2].substring(5);
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setAcountingErrMsg("");
                    } else {
                        this.lienUpdateResponse.setAcountingErrMsg(resp);
                    }

                    this.lienUpdateResponse.setResponseCode("96");
                } else {
                    resp = result[2].trim();
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setAcountingResult("");
                    } else {
                        this.lienUpdateResponse.setAcountingResult(resp);
                    }
                }
            }

            status = "SUCCESS";
        } else {
            err = StringUtils.substringBetween(responseMsg, "<ErrorCode>", "</ErrorCode>");
            resp = StringUtils.substringBetween(responseMsg, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = StringUtils.substringBetween(responseMsg, "<ErrorSource>", "</ErrorSource>");
            String errorType = StringUtils.substringBetween(responseMsg, "<ErrorType>", "</ErrorType>");
            String errorMessage = StringUtils.replaceEach("RequestUUID: " + requestId + " | ErrorCode: " + err + " | ErrorDesc: " + resp + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            status = "FAILED";
            this.lienUpdateResponse.setResponseCode("96");
            this.lienUpdateResponse.setErrorMessage(errorMessage);
            this.lienUpdateResponse.setAcountingResult("");
            this.lienUpdateResponse.setLienModResult("");
            this.lienUpdateResponse.setLienADDResult("");
            this.lienUpdateResponse.setLoanRepayResult("");
        }

        this.lienUpdateResponse.setRequestId(this.lienUpdateRequest.getRequestId());
        err = this.lienUpdateResponse.getAcountingErrMsg() + " | " + this.lienUpdateResponse.getLienADDErrMsg() + " | " + this.lienUpdateResponse.getLienModErrMsg() + " | " + this.lienUpdateResponse.getLoanRepayErrMsg();
        resp = this.lienUpdateResponse.getAcountingResult() + " | " + this.lienUpdateResponse.getLienADDResult() + " | " + this.lienUpdateResponse.getLienModResult() + " | " + this.lienUpdateResponse.getLoanRepayResult();
        this.audit.setStatus(status);
        this.audit.setResponseMessage(resp);
        this.audit.setResponseCode(this.lienUpdateResponse.getResponseCode());
        this.audit.setErrorMessage(this.lienUpdateResponse.getErrorMessage() + " | " + err);
        this.audit.setResponseDate(new Timestamp((new Date()).getTime()));
        this.logger.info("*******************ENDING LIEN UPDATE FOR  Thread  {} ****Starts at {}******************", Integer.valueOf(this.threadCnt), new Timestamp((new Date()).getTime()));
        try{
            this.transactnAuditDAO.saveRecordinTable(this.audit);
            this.logger.info("Saved Succesfully");}
        catch(Exception e){
            this.logger.info("Unable to save this due to"+e.getMessage());
            this.running = false;
            return;
        }
        this.running = false;

    }

    private String callService(String payload, int threadCnnt) {
        this.logger.info("**************************Initiating FI Call for Thread {} starts at {} with Request{}********************", Integer.valueOf(threadCnnt), new Timestamp((new Date()).getTime()),payload);
        String respMessage = "";

        try {
            KeyStore var12 = KeyStore.getInstance(KeyStore.getDefaultType());
            var12.load((InputStream) null, (char[]) null);
            MySSLSocketFactory sf = new MySSLSocketFactory(var12);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            BasicHttpParams params = new BasicHttpParams();
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));
            ThreadSafeClientConnManager ccm = new ThreadSafeClientConnManager(params, registry);
            DefaultHttpClient httpClient = new DefaultHttpClient(ccm, params);
            ByteArrayEntity httpEntity = new ByteArrayEntity(payload.getBytes("UTF-8"));
            HttpPost httpPost = new HttpPost("https://172.27.10.71:9250/fiwebservice/FIWebService");
            httpPost.addHeader("SOAPAction", "");
            httpPost.setEntity(httpEntity);
            CloseableHttpResponse response = httpClient.execute(httpPost);
            String responseMessage = EntityUtils.toString(response.getEntity());
            if (responseMessage != null && responseMessage.length() > 0) {
                responseMessage = responseMessage.replace("&lt;", "<").replace("&gt;", ">");
            }
            //return responseMessage;
        } catch (Exception exception) {
            this.logger.error("Problem occurred in reaching fi {}", exception);
            return "";
        }

        this.logger.info("**************************Initiating FI Call for Thread {} ends at {} with Response {} ********************", Integer.valueOf(threadCnnt), new Timestamp((new Date()).getTime()),respMessage);
        return respMessage;
    }
}
