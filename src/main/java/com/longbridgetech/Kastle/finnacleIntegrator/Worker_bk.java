//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.finnacleIntegrator;

import com.longbridgetech.Kastle.domain.LienBatchRequest;
import com.longbridgetech.Kastle.domain.LienBatchResponse;
import com.longbridgetech.Kastle.Entities.LienUpdAudit;
import com.longbridgetech.Kastle.domain.lienUpdate.*;
import com.longbridgetech.Kastle.persistence.LienUpdAuditDAOImpl;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.StringWriter;
import java.security.KeyStore;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Worker_bk extends Thread {
    private final Logger logger = LoggerFactory.getLogger(Worker_bk.class);
    private LienUpdateRequest lienUpdateRequest;
    private LienUpdateResponse lienUpdateResponse;
    private LienBatchResponse lienBatchResponse;
    private List<Worker_bk> threadList;
    ArrayList<LienUpdateResponse> lienUpdateResponses;
    private volatile int threadCnt;
    public volatile boolean running = false;
    LienUpdAudit audit;
    LienUpdAuditDAOImpl transactnAuditDAO;
    private VelocityEngine ve = new VelocityEngine();
    private Template t;
    private VelocityContext context;
    private ArrayList custInfo;
    private ArrayList partInf;
    private String resultMsg;
    private Properties props;
    private Thread thread;
    private StringWriter writer;

    private KeyStore var12 ;
    private MySSLSocketFactory sf;
    private BasicHttpParams params ;
    private SchemeRegistry registry;
    private ThreadSafeClientConnManager ccm ;
    private DefaultHttpClient httpClient;
    private ByteArrayEntity httpEntity;
    private HttpPost httpPost;
    private CloseableHttpResponse response;
    private SimpleDateFormat simpleDateFormat;
    private String requestId;
    private String responseMessage;
    private PartitionInfo partitionDetails;

    public Worker_bk(LienUpdateRequest lienUpdateRequest, int countt) {
        props = new Properties();
        props.put("resource.loader", "class");
        props.put("runtime.log.logsystem.class","org.apache.velocity.runtime.log.NullLogSystem");
        props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        //this.ve = new VelocityEngine();
        this.ve.init(props);
        this.context = new VelocityContext();
        this.lienUpdateRequest = lienUpdateRequest;
        this.lienUpdateResponse = new LienUpdateResponse();
        this.running = true;
        this.threadCnt = countt;
        this.t = ve.getTemplate("LienUpdate.vm");
        transactnAuditDAO = new LienUpdAuditDAOImpl();
        thread = new Thread(this);
        thread.start();
    }

    public Worker_bk() {
    }

    public LienBatchResponse trigger(LienBatchRequest lienBatchRequest) throws InterruptedException {
        lienBatchResponse = new LienBatchResponse();
        threadList = new ArrayList();
        this.logger.info("********************Start Time For Batch Request is: {} ****************************", new Timestamp((new Date()).getTime()));
        lienUpdateResponses = new ArrayList();
        int cnt = 0;
        Iterator i$ = lienBatchRequest.getLienUpdateRequests().iterator();

        while(i$.hasNext()) {
            lienUpdateRequest = (LienUpdateRequest)i$.next();
            ++cnt;

            try {
                threadList.add(new Worker_bk(lienUpdateRequest, cnt));
                this.logger.info("********************Thread {} Started at {} ****************************", Integer.valueOf(cnt), new Timestamp((new Date()).getTime()));
            } catch (Exception var10) {
                var10.printStackTrace();
            }
        }

        i$ = threadList.iterator();

        while(i$.hasNext()) {
            Worker_bk worker2 = (Worker_bk)i$.next();

            while(worker2.running) {
                Thread.sleep(100);
            }
            lienUpdateResponses.add(worker2.lienUpdateResponse);
            worker2 = null;
        }

        lienBatchResponse.setLienUpdateResponses(lienUpdateResponses);
        this.logger.info("********************End Time For Batch Request is: {} ****************************", new Timestamp((new Date()).getTime()));
        return lienBatchResponse;
    }

    public void run() {
        this.audit = new LienUpdAudit(this.lienUpdateRequest);
        this.audit.setRequestDate(new Timestamp((new Date()).getTime()));
        this.logger.info("*******************INITIATING LIEN UPDATE FOR  Thread  {} ****Starts at {}******************", Integer.valueOf(this.threadCnt), new Timestamp((new Date()).getTime()));
        requestId = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID", requestId);
        this.context.put("MessageDateTime", (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")).format(new Date()));
        this.custInfo = new ArrayList();
        int i = 1;

        if(this.lienUpdateRequest.getAccInfo()!=null){
            Iterator i$ = this.lienUpdateRequest.getAccInfo().iterator();
            AccInfo ci;
            while(i$!=null && i$.hasNext()) {
                ci = (AccInfo)i$.next();
                if(ci.getForacid() != null) {
                    CustInfo4Vm ciVm = new CustInfo4Vm();
                    ciVm.setCreditDebitFlg(ci.getCreditDebitFlg());
                    ciVm.setAmountValue(ci.getTrnAmt().getAmountValue());
                    ciVm.setForacid(ci.getForacid());
                    ciVm.setAllowFreezeAccnt(ci.getAllowFreezeAccnt());
                    ciVm.setAllowFalseDebit(ci.getAllowFalseDebit());
                    ciVm.setCurrencyCode(ci.getTrnAmt().getCurrencyCode());
                    ciVm.setTrnParticulars(ci.getTrnParticulars());
                    ciVm.setValueDt(ci.getValueDt().replace("T", " "));
                    ciVm.setRate(ci.getRate().toUpperCase());
                    ciVm.setSn(i + "");
                    this.custInfo.add(ciVm);
                    ++i;
                }
            }
            ci = null;
            i$ = null;
            int accinfoLength = this.lienUpdateRequest.getAccInfo()== null?0:this.lienUpdateRequest.getAccInfo().size();
            this.partInf = new ArrayList();
            this.logger.info("accinfo length is " + accinfoLength);

            for(int l = 0; l < accinfoLength; ++l) {
                partitionDetails = new PartitionInfo();
                partitionDetails.setKasLoanNumber(((AccInfo)this.lienUpdateRequest.getAccInfo().get(l)).getPartitionDetails().getKasLoanNumber());
                partitionDetails.setEntityType(((AccInfo)this.lienUpdateRequest.getAccInfo().get(l)).getPartitionDetails().getEntityType());
                partitionDetails.setKasNumber(((AccInfo)this.lienUpdateRequest.getAccInfo().get(l)).getPartitionDetails().getKasNumber());
                partitionDetails.setSn(l + 1);
                this.partInf.add(partitionDetails);
            }

        }
        this.context.put("numOfAccts", Integer.valueOf(this.custInfo.size())==null?"0":Integer.valueOf(this.custInfo.size()));
        this.context.put("accInfo", this.custInfo==null?"":this.custInfo);
        this.context.put("partInfo", this.partInf==null?"":this.partInf);

        if(this.lienUpdateRequest.getLienInfo()!= null && this.lienUpdateRequest.getLienInfo().getModifyLien()!=null){
            this.context.put("unlienAmt", this.lienUpdateRequest.getLienInfo().getModifyLien().getUnLienAmt());
            this.context.put("lienId", this.lienUpdateRequest.getLienInfo().getModifyLien().getLienID());
        }else{
            this.context.put("lienId","");
            this.context.put("unlienAmt","");
        }
        if(this.lienUpdateRequest.getLienInfo()!= null && this.lienUpdateRequest.getLienInfo().getFreshLien()!=null){
            this.context.put("lienAmt", this.lienUpdateRequest.getLienInfo().getFreshLien().getLienAmt());
            this.context.put("foracid", this.lienUpdateRequest.getLienInfo().getFreshLien().getForacid());
            this.context.put("lienReasonCode", this.lienUpdateRequest.getLienInfo().getFreshLien().getLienReasonCode());
            this.context.put("lienRemark", this.lienUpdateRequest.getLienInfo().getFreshLien().getLienRemark());
            simpleDateFormat  = new SimpleDateFormat("YYYY-MM-dd");
            String expDate="";
            try {
                expDate = new SimpleDateFormat("dd-MM-YYYY").format(simpleDateFormat.parse(this.lienUpdateRequest.getLienInfo().getFreshLien().getExpiredDate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            this.context.put("expiredDate",expDate);
        }else{
            this.context.put("lienAmt", "");
            this.context.put("lienReasonCode", "");
            this.context.put("lienRemark", "");
            this.context.put("foracid", "");
            this.context.put("expiredDate", "");

        }
        writer = new StringWriter();

        if(this.t != null) {
            this.t.merge(this.context, writer);
        }
        String responseMsg;
        try {
            responseMsg = this.callService(writer.toString(), this.threadCnt);
        } catch (Exception var12x) {
            var12x.printStackTrace();
            this.lienUpdateResponse.setErrorMessage(var12x.getMessage());
            this.lienUpdateResponse.setResponseCode("96");
            this.running = false;
            return;
        }

        this.resultMsg = org.apache.commons.lang3.StringUtils.substringBetween(responseMsg, "<RESULT_MSG>", "</RESULT_MSG>");
        boolean isSuccessful = responseMsg.contains("<Status>SUCCESS</Status>");
        String status;
        String resp;
        String err;
        if(isSuccessful) {

            this.lienUpdateResponse.setErrorMessage("");
            this.lienUpdateResponse.setResponseCode("00");
            String[] result = this.resultMsg.split("__");
            System.out.println(result[0] + " " + result[1] + " " + result[2] + " " + result[3]);
            if(result[0].trim().length() > 0) {
                if(!result[0].contains("::N::")) {
                    resp = result[0].trim();
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setLienADDResult("");
                    } else {
                        this.lienUpdateResponse.setLienADDResult(resp);
                    }
                } else {
                    resp = result[0].substring(5);
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setLienADDErrMsg("");
                    } else {
                        this.lienUpdateResponse.setLienADDErrMsg(resp);
                    }

                    this.lienUpdateResponse.setResponseCode("96");
                }
            }

            if(result[1].trim().length() > 0) {
                if(result[1].contains("::N::")) {
                    resp = result[1].substring(5);
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setLienModErrMsg("");
                    } else {
                        this.lienUpdateResponse.setLienModErrMsg(resp);
                    }

                    this.lienUpdateResponse.setResponseCode("96");
                } else {
                    resp = result[1].trim();
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setLienModResult("");
                    } else {
                        this.lienUpdateResponse.setLienModResult(resp);
                    }
                }
            }

            if(result[2].trim().length() > 0) {
                if(result[2].contains("::N::")) {
                    resp = result[2].substring(5);
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setAcountingErrMsg("");
                    } else {
                        this.lienUpdateResponse.setAcountingErrMsg(resp);
                    }

                    this.lienUpdateResponse.setResponseCode("96");
                } else {
                    resp = result[2].trim();
                    if(resp.equals("...")) {
                        this.lienUpdateResponse.setAcountingResult("");
                    } else {
                        this.lienUpdateResponse.setAcountingResult(resp);
                    }
                }
            }

            status = "SUCCESS";
        } else {
            err = org.apache.commons.lang3.StringUtils.substringBetween(responseMsg, "<ErrorCode>", "</ErrorCode>");
            resp = org.apache.commons.lang3.StringUtils.substringBetween(responseMsg, "<ErrorDesc>", "</ErrorDesc>");
            String errorSource = org.apache.commons.lang3.StringUtils.substringBetween(responseMsg, "<ErrorSource>", "</ErrorSource>");
            String errorType = org.apache.commons.lang3.StringUtils.substringBetween(responseMsg, "<ErrorType>", "</ErrorType>");
            String errorMessage = org.apache.commons.lang3.StringUtils.replaceEach("RequestUUID: " + requestId + " | ErrorCode: " + err + " | ErrorDesc: " + resp + " | ErrorSource: " + errorSource + " | ErrorType: " + errorType, new String[]{"\n", "\t", "\r"}, new String[]{" ", " ", " "});
            status = "FAILED";
            this.lienUpdateResponse.setResponseCode("96");
            this.lienUpdateResponse.setErrorMessage(errorMessage);
            this.lienUpdateResponse.setAcountingResult("");
            this.lienUpdateResponse.setLienModResult("");
            this.lienUpdateResponse.setLienADDResult("");
            this.lienUpdateResponse.setLoanRepayResult("");
        }

        this.lienUpdateResponse.setRequestId(this.lienUpdateRequest.getRequestId());
        err = this.lienUpdateResponse.getAcountingErrMsg() + " | " + this.lienUpdateResponse.getLienADDErrMsg() + " | " + this.lienUpdateResponse.getLienModErrMsg() + " | " + this.lienUpdateResponse.getLoanRepayErrMsg();
        resp = this.lienUpdateResponse.getAcountingResult() + " | " + this.lienUpdateResponse.getLienADDResult() + " | " + this.lienUpdateResponse.getLienModResult() + " | " + this.lienUpdateResponse.getLoanRepayResult();
        audit.setStatus(status);
        audit.setResponseMessage(resp);
        audit.setResponseCode(this.lienUpdateResponse.getResponseCode());
        audit.setErrorMessage(this.lienUpdateResponse.getErrorMessage() + " | " + err);
        audit.setResponseDate(new Timestamp((new Date()).getTime()));
        this.logger.info("*******************ENDING LIEN UPDATE FOR  Thread  {} ****Starts at {}******************", Integer.valueOf(this.threadCnt), new Timestamp((new Date()).getTime()));
        this.logger.info("Audit Content is "+audit.toString());
        try{
            transactnAuditDAO.saveRecordinTable(audit);
            this.logger.info("Saved Succesfully");}
        catch(Exception e){
            this.logger.info("Unable to save this due to"+e.getMessage());
            this.running = false;
            return;
        }
        this.running = false;

    }

    private String callService(String payload, int threadCnnt) {
        this.logger.info("**************************Initiating FI Call for Thread {} starts at {} with Request{}********************", Integer.valueOf(threadCnnt), new Timestamp((new Date()).getTime()),payload);
        responseMessage = "";

        try {
            var12 = KeyStore.getInstance(KeyStore.getDefaultType());
            var12.load((InputStream) null, (char[]) null);
            sf = new MySSLSocketFactory(var12);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            params = new BasicHttpParams();
            registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));
            ccm = new ThreadSafeClientConnManager(params, registry);
            httpClient = new DefaultHttpClient(ccm, params);
            httpEntity = new ByteArrayEntity(payload.getBytes("UTF-8"));
            httpPost = new HttpPost("https://172.27.10.179:6500/fiwebservice/FIWebService");
            httpPost.addHeader("SOAPAction", "");
            httpPost.setEntity(httpEntity);
            response = httpClient.execute(httpPost);
            responseMessage = EntityUtils.toString(response.getEntity());
            if (responseMessage != null && responseMessage.length() > 0) {
                responseMessage = responseMessage.replace("&lt;", "<").replace("&gt;", ">");
            }
        } catch (Exception exception) {
            this.logger.error("Problem occurred in reaching fi  {}", exception.getMessage());
            return "";
        }

        this.logger.info("**************************Initiating FI Call for Thread {} ends at {} with Response {} ********************", Integer.valueOf(threadCnnt), new Timestamp((new Date()).getTime()),responseMessage);
        return responseMessage;
    }
}
