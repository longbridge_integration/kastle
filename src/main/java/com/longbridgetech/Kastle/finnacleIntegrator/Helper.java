//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.finnacleIntegrator;

import com.longbridgetech.Kastle.domain.lastNTrabansactionWithDescriptionResp.GetLastNTransactionsWithPaginationResponse;
import com.longbridgetech.Kastle.domain.tdAcctInq.BankInfo;
import com.longbridgetech.Kastle.domain.tdAcctInq.PostAddr;
import com.longbridgetech.Kastle.persistence.HibernateUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class Helper {
    private static final Logger logger = LoggerFactory.getLogger(Helper.class);

    private final static SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
    private final static SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd-MMM-yyyy");

    public Helper() {
    }

    public static String getAOC(String acctNum) {
        String AOC = "";
        logger.info("Begin fetchDate Query");
        try {
            HibernateUtil.beginTransaction();
            String e = "select free_code_10 from tbaadm.fcftt where acid = (select acid from tbaadm.gam where foracid='" + acctNum + "')";
            List entities = HibernateUtil.getSession().createSQLQuery(e).list();
            if (entities.size() > 0) {
                AOC = entities.get(0).toString();
            }
            HibernateUtil.commitTransaction();
        } catch (Exception var6) {
            logger.error("Error encountered wile processing Fetching AOC fromFinacle {}", var6);
            AOC = "Error Fetching AOC";
        } finally {
            HibernateUtil.closeSession();
        }

        return AOC;
    }

    public static String isSystemApproved(String ipAddress) {
        String isApproved="N";
        logger.info("Begin check for system approved status on  {}",ipAddress);
        try {
            //todo
            HibernateUtil.beginTransaction();
            String e = "select k.system_desc from kas_approved_systems_tbl k where k.IP_ADDRESS=:ipAddress";
            List entities = HibernateUtil.getSession().createSQLQuery(e).setParameter("ipAddress",ipAddress).list();
            if (entities.size() > 0) {
                isApproved ="Y";
            }
            HibernateUtil.commitTransaction();
        } catch (Exception e) {
            isApproved = e.toString();
            logger.error("Error encountered wile  Fetching Approved system stattus  {}", e.toString());
        }
        finally {
                HibernateUtil.closeSession();
        }
        logger.info("Result for IP Address "+ipAddress+"  is "+isApproved);
        return isApproved;
    }

    public static String getCustomerSOL(String acctNum) {
    String solID= "";
    logger.info("Begin fetchSol Query");

    try {
        HibernateUtil.beginTransaction();
        String e = "select sol_id from tbaadm.gam where foracid = '" + acctNum + "'";
        List entities = HibernateUtil.getSession().createSQLQuery(e).list();
        if (entities.size() > 0) {
            solID = entities.get(0).toString();
        }
        HibernateUtil.commitTransaction();
    } catch (Exception var6) {
        logger.error("Error encountered wile processing Fetching AOC fromFinacle {}", var6);
        solID= "";
    } finally {
        HibernateUtil.closeSession();
    }

    return solID;
    }

    public static BankInfo getBankInfo(String foracid) {
    BankInfo bankInfo = new BankInfo();
    logger.info("Begin fetchDate Query");

    try {
        HibernateUtil.beginTransaction();
      String query =   "select s.sol_id as bankid, bct.bank_name, s.br_code as BranchId, s.sol_desc as BranchName,s.Addr_1,s.Addr_2,Addr_3, city_code as City,\n" +
                "state_code as State, cntry_code as Country from \n" +
                "tbaadm.bank_Code_table bct, tbaadm.sol s where bct.bank_code = s.BANK_CODE AND s.sol_id = \n" +
                "(SELECT sol_id FROM tbaadm.gam WHERE foracid=:foracid)";

        List entities = HibernateUtil.getSession().createSQLQuery(query).setParameter("foracid",foracid).list();
        if (entities.size() > 0) {
            for (Object entity : entities) {
                Object[] data = (Object[]) entity;
                for(int i = 0; i < data.length;i++){
                    if(data[i]==null){
                        data[i]="";
                    }
                }
                if (data != null) {
                    bankInfo.setBankId(data[0].toString());
                    bankInfo.setName(data[1].toString());
                    bankInfo.setBranchId(data[2].toString());
                    bankInfo.setBranchName(data[3].toString());
                    PostAddr postAddr = new PostAddr();
                    postAddr.setAddr1(data[4].toString());
                    postAddr.setAddr2(data[5].toString());
                    postAddr.setAddr3(data[6].toString());
                    postAddr.setCity(data[7].toString());
                    postAddr.setStateProv(data[8].toString());
                    postAddr.setCountry(data[9].toString());
                    bankInfo.setPostAddr(postAddr);
                }
            }
        }
        HibernateUtil.commitTransaction();
    } catch (Exception var6) {
        logger.error("Error encountered wile processing Fetching AOC fromFinacle {}", var6);
    } finally {
        HibernateUtil.closeSession();
    }

    return bankInfo;
    }

    public static String getCustDOB(String acctNum) {
    String DOB = "";
    logger.info("Begin fetchDate Query");

    try {
        HibernateUtil.beginTransaction();
        String e = "select cust_dob , orgkey from crmuser.accounts where orgkey = (SELECT cif_id from tbaadm.gam where foracid = '"+acctNum + "')";
        List entities = HibernateUtil.getSession().createSQLQuery(e).addScalar("cust_dob").list();
        if (entities.size() > 0) {
            DOB = entities.get(0).toString().substring(0,10);
        }

        HibernateUtil.commitTransaction();
    } catch (Exception var6) {
        logger.error("Error encountered wile processing Fetching DOB fromFinacle {}", var6);
        DOB = "";
    } finally {
        HibernateUtil.closeSession();
    }
    logger.error("Cust DOB IS {}", DOB);
    return DOB;
    }


    public static String getTranDesciption(String acid,GetLastNTransactionsWithPaginationResponse.PaginatedAccountStatement.TransactionDetails transactionDetails){
        String tranDescription="";
        String trandate = transactionDetails.getTransactionSummary().getTxnDate().substring(0,10);
        String tranAmt = transactionDetails.getTransactionSummary().getTxnAmt().getAmountValue();
        try {
             trandate = simpleDateFormat2.format(simpleDateFormat1.parse(trandate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        logger.info("Begin check for Transaction Description using {}",transactionDetails.toString());
        try {
            HibernateUtil.beginTransaction();
            String e = "select TRAN_PARTICULAR from tbaadm.dtd where " +
                        "tran_date = '"+trandate+"' " +
                        "and part_tran_type = '"+transactionDetails.getTransactionSummary().getTxnType()+"' " +
                        "and tran_amt = '"+tranAmt+"' " +
                        "and trim(tran_id) = trim('"+transactionDetails.getTxnId()+"') " +
                        "and acid = '"+acid+"' " +
                        "union  " +
                        "select TRAN_PARTICULAR from tbaadm.htd where   " +
                        "tran_date = '"+trandate+"' " +
                        "and part_tran_type = '"+transactionDetails.getTransactionSummary().getTxnType()+"' " +
                        "and tran_amt = '"+tranAmt+"' " +
                        "and trim(tran_id) = trim('"+transactionDetails.getTxnId()+"') " +
                        "and acid = '"+acid+"'";
            List entities = HibernateUtil.getSession().createSQLQuery(e).list();
            if (entities.size() > 0) {
                System.out.println("Result is "+entities.get(0));
                tranDescription = entities.get(0).toString();
            }
            HibernateUtil.commitTransaction();
        } catch (Exception e) {
            tranDescription = "";
            logger.error("Error encountered wile  Fetching Tran Description:  {}", e.toString());
        }
        finally {
            HibernateUtil.closeSession();
        }
        return tranDescription;
    }

    public String  findByTranDetails(String acctNumber,String amnt,String valdate,String tranDescription){
        String tranID = null;
        String valuedate = valdate;
        try {
            valuedate = simpleDateFormat2.format(simpleDateFormat1.parse(valuedate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            HibernateUtil.beginTransaction();
            String e = "select d.tran_id from tbaadm.dtd d,tbaadm.gam g where g.acid = d.acid" +
                    " and value_date='"+valuedate+"' and tran_amt = '"+amnt+"'" +
                    " and tran_paticulars='"+tranDescription+"' and g.foracid = '"+acctNumber+"' and rownum < 2";
            List entities = HibernateUtil.getSession().createSQLQuery(e).list();
            if (entities.size() > 0) {
                System.out.println("Result is "+entities.get(0));
                tranID = entities.get(0).toString();
            }
            HibernateUtil.commitTransaction();
        } catch (Exception e) {
            tranID = "";
            logger.error("Error encountered wile  Fetching Tran Description:  {}", e.toString());
        }
        finally {
            HibernateUtil.closeSession();
        }
        return tranID;
    }


    public static String getOperatingAcct(String loanAcct) {

        String creditAcct = "";
        logger.info("BEGINS FETCH FOR OPERATING ACCOUNT USING LOANACCT: {} ", loanAcct);

        try {
            HibernateUtil.beginTransaction();
            String e = "select foracid from tbaadm.gam where acid=(select op_acid from tbaadm.lam \n" +
                    "where acid=(select acid from gam where foracid=:loanAcct and schm_type in ('LAA','CLA')))";

            List entities = HibernateUtil.getSession().createSQLQuery(e).setParameter("loanAcct", loanAcct).list();
            if (entities.size() > 0) {
                creditAcct = entities.get(0).toString();

                if (creditAcct.equals("")) {
                    creditAcct = "";
                }
            }

            logger.info("Operating Account found {} ", creditAcct);
            HibernateUtil.commitTransaction();
        } catch (Exception var7) {
            logger.error("Error encountered wile processing getGlSubHeadCode {}", var7);
        } finally {
            HibernateUtil.closeSession();
        }

        return creditAcct;
    }


    public static String getAcid(String foracid) {

        String acid = "";

        try {
            HibernateUtil.beginTransaction();
            String e = "select acid from tbaadm.gam where foracid='"+foracid+"' ";

            List entities = HibernateUtil.getSession().createSQLQuery(e).list();
            if (entities.size() > 0) {
                acid = entities.get(0).toString();
            }
            HibernateUtil.commitTransaction();
        } catch (Exception var7) {
            logger.error("Error encountered wile processing getAcid {}", var7);
        } finally {
            HibernateUtil.closeSession();
        }

        return acid;
    }
}
