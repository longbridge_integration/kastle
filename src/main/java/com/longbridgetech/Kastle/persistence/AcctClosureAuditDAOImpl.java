//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.persistence;

import com.longbridgetech.Kastle.Entities.AcctClosureAudit;
import com.longbridgetech.Kastle.persistence.interfaces.AcctClosueAuditDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AcctClosureAuditDAOImpl extends GenericDAOImpl<AcctClosureAudit, String> implements AcctClosueAuditDAO {
    Logger logger = LoggerFactory.getLogger(AcctClosureAuditDAOImpl.class);

    public AcctClosureAuditDAOImpl() {
    }


    public void saveRecordinTable(AcctClosureAudit tranAudit) {
        try {
            HibernateUtil.beginTransaction();
            this.logger.info("Save accTable record in DB -- {}", tranAudit.getRequestID());
            this.save(tranAudit);
            HibernateUtil.commitTransaction();
            this.logger.info("Record saved and committed in  DB successfully for unique code -- {}", tranAudit.getRequestID());
        } catch (Exception var6) {
            HibernateUtil.rollbackTransaction();
            this.logger.error("Error occured in save record in DB -- {} -- {} -- {}", var6.getLocalizedMessage(), var6.toString(), tranAudit.getRequestID());
            this.logger.error("{}", var6);
        } finally {
            HibernateUtil.closeSession();
            this.logger.info("Close save record in TransactionAudit table successful");
        }

    }

}
