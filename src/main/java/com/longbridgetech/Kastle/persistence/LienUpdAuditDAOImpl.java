//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.persistence;

import com.longbridgetech.Kastle.Entities.LienUpdAudit;
import com.longbridgetech.Kastle.domain.lienUpdate.LienUpdateResponse;
import com.longbridgetech.Kastle.persistence.interfaces.LienUpdateDAO;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LienUpdAuditDAOImpl extends GenericDAOImpl<LienUpdAudit, String> implements LienUpdateDAO {
    Logger logger = LoggerFactory.getLogger(LienUpdAuditDAOImpl.class);

    public LienUpdAuditDAOImpl() {
    }


    public void saveRecordinTable(LienUpdAudit tranAudit) {
        try {
            HibernateUtil.beginTransaction();
            this.logger.info("Save accTable record in DB  -- {}", tranAudit.getRequestID());
            this.save(tranAudit);
            HibernateUtil.commitTransaction();
            this.logger.info("Record saved and committed  in DB successfully for unique code -- {}", tranAudit.getRequestID());
        } catch (Exception var6) {
            HibernateUtil.rollbackTransaction();
            this.logger.error("Error occured in save record in DB -- {} -- {} -- {}", var6.getLocalizedMessage(), var6.toString(), tranAudit.getRequestID());
            this.logger.error("{}", var6);
        } finally {
            try{HibernateUtil.closeSession();}
            catch(Exception e){
                this.logger.error("Failed to Save Exception escaped {}",e);
                return;
            }
        }

    }

    public LienUpdateResponse findTransactionByRequestID(String requestID){
        LienUpdAudit lienUpdAudit = null;
        LienUpdateResponse lienUpdateResponse = null;
        boolean errorhasHappenned = false;
        String errorStng = "";
        try {
            HibernateUtil.beginTransaction();
            String ex = "select k from LienUpdAudit k where k.RequestID=:requestID and rownum < 2";
            Query queryVal = HibernateUtil.getSession().createQuery(ex).setParameter("requestID",requestID);
            lienUpdAudit = this.findOne(queryVal);
            HibernateUtil.commitTransaction();

        } catch (NonUniqueResultException var9) {
            this.logger.error("Error in unique result -- {} -- {}", var9.getLocalizedMessage(), var9.toString());
            errorStng = var9.toString();
            errorhasHappenned = true;
        } catch (HibernateException var10) {
            this.logger.error("General Hibernate exception thrown -- {} -- {}", var10.getLocalizedMessage(), var10.toString());
            errorStng = var10.toString();
            errorhasHappenned = true;
        } finally {
            HibernateUtil.closeSession();
            this.logger.info("Close Hibernate session successfully !!!");
        }
        if(lienUpdAudit!=null){
            String[] result = lienUpdAudit.getResponseMessage().split("\\|");
            lienUpdateResponse = new LienUpdateResponse();
            if(result.length>2){
                lienUpdateResponse.setAcountingResult(result[0]);
                lienUpdateResponse.setLienADDResult(result[1]);
                lienUpdateResponse.setLienModResult(result[2]);
            }else{
                lienUpdateResponse.setAcountingResult("");
                lienUpdateResponse.setAcountingResult("");
                lienUpdateResponse.setAcountingResult("");
            }
            lienUpdateResponse.setRequestId(requestID);
            lienUpdateResponse.setResponseCode(lienUpdAudit.getResponseCode());
            lienUpdateResponse.setErrorMessage(lienUpdAudit.getErrorMessage().replace("|","").replace("null",""));

        }
        if(errorhasHappenned){
            lienUpdateResponse = new LienUpdateResponse();
            lienUpdateResponse.setRequestId(requestID);
            lienUpdateResponse.setResponseCode("96");
            lienUpdateResponse.setErrorMessage("Error Occured Checking Status: "+errorStng);
        }
        return lienUpdateResponse;
    }

}
