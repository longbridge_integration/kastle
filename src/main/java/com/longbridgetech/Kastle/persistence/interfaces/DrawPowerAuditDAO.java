
package com.longbridgetech.Kastle.persistence.interfaces;

import com.longbridgetech.Kastle.Entities.DrawPowerAudit;

public interface DrawPowerAuditDAO extends GenericDAO<DrawPowerAudit, String> {
}
