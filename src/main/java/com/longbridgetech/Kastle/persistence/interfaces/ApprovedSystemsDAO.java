
package com.longbridgetech.Kastle.persistence.interfaces;

import com.longbridgetech.Kastle.domain.ApprovedSystems;

public interface ApprovedSystemsDAO extends GenericDAO<ApprovedSystems, ApprovedSystems> {
}
