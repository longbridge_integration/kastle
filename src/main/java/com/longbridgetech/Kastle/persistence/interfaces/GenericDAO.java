//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.persistence.interfaces;

import org.hibernate.Query;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T, ID extends Serializable> {
    void save(T var1);

    void merge(T var1);

    void delete(T var1);

    List<T> findMany(Query var1);

    T findOne(Query var1);

    List findAll(Class var1);

    T findByID(Class var1, String ID);
}
