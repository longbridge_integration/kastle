
package com.longbridgetech.Kastle.persistence.interfaces;

import com.longbridgetech.Kastle.Entities.BulkPayAudit;

public interface BulkPayAuditDAO extends GenericDAO<BulkPayAudit, String> {
}
