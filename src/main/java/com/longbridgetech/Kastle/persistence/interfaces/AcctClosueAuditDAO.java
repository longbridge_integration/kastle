
package com.longbridgetech.Kastle.persistence.interfaces;

import com.longbridgetech.Kastle.Entities.AcctClosureAudit;

public interface AcctClosueAuditDAO extends GenericDAO<AcctClosureAudit, String> {

}
