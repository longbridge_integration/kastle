
package com.longbridgetech.Kastle.persistence.interfaces;
import com.longbridgetech.Kastle.Entities.LoanTopupAudit;

public interface LoanTopupAuditDAO extends GenericDAO<LoanTopupAudit, String> {
}
