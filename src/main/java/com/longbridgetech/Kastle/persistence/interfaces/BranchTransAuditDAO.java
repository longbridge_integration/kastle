
package com.longbridgetech.Kastle.persistence.interfaces;

import com.longbridgetech.Kastle.Entities.BranchTransfAudit;

public interface BranchTransAuditDAO extends GenericDAO<BranchTransfAudit, String> {
}
