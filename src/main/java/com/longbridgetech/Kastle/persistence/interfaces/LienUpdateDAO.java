
package com.longbridgetech.Kastle.persistence.interfaces;

import com.longbridgetech.Kastle.Entities.LienUpdAudit;

public interface LienUpdateDAO extends GenericDAO<LienUpdAudit, String> {
}
