//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.persistence;

import com.longbridgetech.Kastle.Entities.DrawPowerAudit;
import com.longbridgetech.Kastle.persistence.interfaces.DrawPowerAuditDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DrawPowerAuditDAOImpl extends GenericDAOImpl<DrawPowerAudit, String> implements DrawPowerAuditDAO {
    Logger logger = LoggerFactory.getLogger(DrawPowerAuditDAOImpl.class);

    public DrawPowerAuditDAOImpl() {
    }


    public void saveRecordinTable(DrawPowerAudit tranAudit) {
        try {
            HibernateUtil.beginTransaction();
            this.logger.info("Save accTable  record in DB -- {}", tranAudit.getRequestID());
            this.save(tranAudit);
            HibernateUtil.commitTransaction();
            this.logger.info("Record saved and  committed in DB successfully for unique code -- {}", tranAudit.getRequestID());
        } catch (Exception var6) {
            HibernateUtil.rollbackTransaction();
            this.logger.error("Error occured in save record in DB -- {} -- {} -- {}", var6.getLocalizedMessage(), var6.toString(), tranAudit.getRequestID());
            this.logger.error("{}", var6);
        } finally {
            HibernateUtil.closeSession();
            this.logger.info("Close save record in TransactionAudit table successful");
        }

    }

}
