//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.persistence;

import com.longbridgetech.Kastle.domain.ApprovedSystems;
import com.longbridgetech.Kastle.persistence.interfaces.ApprovedSystemsDAO;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApprovedSystemsDAOImpl extends GenericDAOImpl<ApprovedSystems, ApprovedSystems> implements ApprovedSystemsDAO{
    private Logger logger = LoggerFactory.getLogger(ApprovedSystemsDAOImpl.class);

    public ApprovedSystemsDAOImpl() {
    }


}
