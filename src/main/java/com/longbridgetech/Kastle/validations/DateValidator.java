//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.validations;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateValidator implements ConstraintValidator<ValidDate, String> {
    private Logger logger = LoggerFactory.getLogger(DateValidator.class);
    private String datePattern;

    public DateValidator() {
    }

    public void initialize(ValidDate constraintAnnotation) {
        this.datePattern = constraintAnnotation.datePattern();
    }

    public boolean isValid(String inputDate, ConstraintValidatorContext context) {
        if (inputDate != null && !inputDate.trim().equals("")) {
            DateTime date = null;

            try {
                DateTimeFormatter e = DateTimeFormat.forPattern(this.datePattern);
                date = e.parseDateTime(inputDate);
                if (date != null) {
                    this.logger.debug("date is valid");
                }

                return true;
            } catch (Exception var5) {
                this.logger.debug(var5.getLocalizedMessage());
                return false;
            }
        } else {
            this.logger.debug("Date supplied was null");
            return false;
        }
    }
}
