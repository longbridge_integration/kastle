//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.Entities;

import com.longbridgetech.Kastle.domain.lienUpdate.LienUpdateRequest;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LienUpdAudit {

    private String RequestID;
    private String sessionID;
    private String appCode;
    private String opAcctNum;
    private String loanTransAmt;
    private String loanAcctNum;
    private String freshLienAmt;
    private String freshLienRCode;
    private String freshLienRemark;
    private String freshLienExpDate;
    private String modifyLienId;
    private String modifyUnlienAmt;
    private String acctInfo;
    private String acctInfoResp;
    private String loanTransAmtCurr;
    private String responseCode = "";
    private String responseMessage = "";
    private String errorMessage = "";
    private Timestamp requestDate;
    private Timestamp responseDate;
    private String status = "";
    private Logger logger = LoggerFactory.getLogger(LienUpdAudit.class);
    SimpleDateFormat formatter = new SimpleDateFormat("YYYYMMddHHmmSS");

    public LienUpdAudit() {

    }

    public LienUpdAudit(LienUpdateRequest request) {
        this.sessionID = formatter.format(new Timestamp(new Date().getTime()))+RandomStringUtils.randomNumeric(8);
        this.RequestID = request.getRequestId()==null?"null":request.getRequestId();//RandomStringUtils.randomNumeric(8);
        this.appCode = "KASLNUPD";
        this.acctInfo = request.getAccInfo()== null? "":request.getAccInfo().toString();
        if(request.getLienInfo()!=null){
            this.freshLienAmt = request.getLienInfo().getFreshLien() == null ? "":request.getLienInfo().getFreshLien().getLienAmt();
            this.freshLienExpDate = request.getLienInfo().getFreshLien()==null?"":request.getLienInfo().getFreshLien().getExpiredDate();
            this.freshLienRCode = request.getLienInfo().getFreshLien() == null?"":request.getLienInfo().getFreshLien().getLienReasonCode();
            this.freshLienRemark = request.getLienInfo().getFreshLien() ==null?"":request.getLienInfo().getFreshLien().getLienRemark();
            this.modifyLienId = request.getLienInfo().getModifyLien() ==null?"":request.getLienInfo().getModifyLien().getLienID();
            this.modifyUnlienAmt = request.getLienInfo().getModifyLien() == null?"":request.getLienInfo().getModifyLien().getUnLienAmt();

        }

    }



    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getOpAcctNum() {
        return opAcctNum;
    }

    public void setOpAcctNum(String opAcctNum) {
        this.opAcctNum = opAcctNum;
    }

    public String getLoanTransAmt() {
        return loanTransAmt;
    }

    public void setLoanTransAmt(String loanTransAmt) {
        this.loanTransAmt = loanTransAmt;
    }

    public String getLoanAcctNum() {
        return loanAcctNum;
    }

    public void setLoanAcctNum(String loanAcctNum) {
        this.loanAcctNum = loanAcctNum;
    }

    public String getFreshLienAmt() {
        return freshLienAmt;
    }

    public void setFreshLienAmt(String freshLienAmt) {
        this.freshLienAmt = freshLienAmt;
    }

    public String getFreshLienRCode() {
        return freshLienRCode;
    }

    public void setFreshLienRCode(String freshLienRCode) {
        this.freshLienRCode = freshLienRCode;
    }

    public String getFreshLienRemark() {
        return freshLienRemark;
    }

    public void setFreshLienRemark(String freshLienRemark) {
        this.freshLienRemark = freshLienRemark;
    }

    public String getFreshLienExpDate() {
        return freshLienExpDate;
    }

    public void setFreshLienExpDate(String freshLienExpDate) {
        this.freshLienExpDate = freshLienExpDate;
    }

    public String getModifyLienId() {
        return modifyLienId;
    }

    public void setModifyLienId(String modifyLienId) {
        this.modifyLienId = modifyLienId;
    }

    public String getModifyUnlienAmt() {
        return modifyUnlienAmt;
    }

    public void setModifyUnlienAmt(String modifyUnlienAmt) {
        this.modifyUnlienAmt = modifyUnlienAmt;
    }

    public String getAcctInfo() {
        return acctInfo;
    }

    public void setAcctInfo(String acctInfo) {
        this.acctInfo = acctInfo;
    }

    public String getLoanTransAmtCurr() {
        return loanTransAmtCurr;
    }

    public void setLoanTransAmtCurr(String loanTransAmtCurr) {
        this.loanTransAmtCurr = loanTransAmtCurr;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Timestamp getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public Timestamp getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Timestamp responseDate) {
        this.responseDate = responseDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getAcctInfoResp() {
        return acctInfoResp;
    }

    public void setAcctInfoResp(String acctInfoResp) {
        this.acctInfoResp = acctInfoResp;
    }

    public Logger getLogger() {
        return logger;
    }


    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public String toString() {
        return "LienUpdAudit{" +
                "RequestID='" + RequestID + '\'' +
                ", sessionID='" + sessionID + '\'' +
                ", appCode='" + appCode + '\'' +
                ", opAcctNum='" + opAcctNum + '\'' +
                ", loanTransAmt='" + loanTransAmt + '\'' +
                ", loanAcctNum='" + loanAcctNum + '\'' +
                ", freshLienAmt='" + freshLienAmt + '\'' +
                ", freshLienRCode='" + freshLienRCode + '\'' +
                ", freshLienRemark='" + freshLienRemark + '\'' +
                ", freshLienExpDate='" + freshLienExpDate + '\'' +
                ", modifyLienId='" + modifyLienId + '\'' +
                ", modifyUnlienAmt='" + modifyUnlienAmt + '\'' +
                ", acctInfo='" + acctInfo + '\'' +
                ", acctInfoResp='" + acctInfoResp + '\'' +
                ", loanTransAmtCurr='" + loanTransAmtCurr + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", requestDate=" + requestDate +
                ", responseDate=" + responseDate +
                ", status='" + status + '\'' +
                ", formatter=" + formatter +
                '}';
    }

}


