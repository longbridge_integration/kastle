//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.Entities;

import com.longbridgetech.Kastle.domain.BranchTransferRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class BranchTransfAudit {
    private long id;
    private String RequestID;
    private String appCode;
    private String accountNumber;
    private String toSol;
    private String responseCode = "";
    private String responseMessage = "";
    private String errorMessage = "";
    private Date requestDate = new Date();
    private Date responseDate = new Date();
    private String status = "";
    private String fromSol = "";
    private Logger logger = LoggerFactory.getLogger(BranchTransfAudit.class);

    public BranchTransfAudit() {

    }

    public BranchTransfAudit(BranchTransferRequest request) {
        this.RequestID = request.getRequestID();
        this.appCode = "KASBRTRANS";
        this.accountNumber = request.getAccountNumber();
        this.toSol = request.getToSoL();
    }


    public String getToSol() {
        return toSol;
    }

    public String getFromSol() {
        return fromSol;
    }

    public void setFromSol(String fromSol) {
        this.fromSol = fromSol;
    }

    public void setToSol(String toSol) {
        this.toSol = toSol;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public String toString() {
        return "BranchTransfAudit{" +
                "id=" + id +
                ", RequestID='" + RequestID + '\'' +
                ", appCode='" + appCode + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", toSol='" + toSol + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", requestDate=" + requestDate +
                ", responseDate=" + responseDate +
                ", status='" + status + '\'' +
                ", fromSol='" + fromSol + '\'' +
                '}';
    }

}


