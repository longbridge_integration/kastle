//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.Entities;

import com.longbridgetech.Kastle.domain.DrawPowerRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DrawPowerAudit {
    private long id;
    private String RequestID;
    private String appCode;
    private String foracid;
    private Date appDate;
    private String event;
    private String drawPwr;
    private String remark;
    private String drawPwrIndCode;
    private String responseCode = "";
    private String responseMessage = "";
    private String errorMessage = "";
    private Date requestDate = new Date();
    private Date responseDate = new Date();
    private String status = "";
    private Logger logger = LoggerFactory.getLogger(DrawPowerAudit.class);

    public DrawPowerAudit() {

    }

    public DrawPowerAudit(DrawPowerRequest request) {
        this.RequestID = request.getRequestID();
        this.appCode = "KASDRPOW";
        this.foracid = request.getForacid();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {

           this.appDate = formatter.parse(request.getAppDate());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.drawPwr = request.getDrwngPower();
        this.drawPwrIndCode = request.getDrwngPowerIndCode();
        this.event = request. getEvent();
        this.remark = request. getRemark();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public String getAppCode() {
        return appCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getForacid() {
        return foracid;
    }

    public void setForacid(String foracid) {
        this.foracid = foracid;
    }

    public Date getAppDate() {
        return appDate;
    }

    public void setAppDate(Date appDate) {
        this.appDate = appDate;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getDrawPwr() {
        return drawPwr;
    }

    public void setDrawPwr(String drawPwr) {
        this.drawPwr = drawPwr;
    }

    public String getDrawPwrIndCode() {
        return drawPwrIndCode;
    }

    public void setDrawPwrIndCode(String drawPwrIndCode) {
        this.drawPwrIndCode = drawPwrIndCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public String toString() {
        return "DrawPowerAudit{" +
                "id=" + id +
                ", RequestID='" + RequestID + '\'' +
                ", appCode='" + appCode + '\'' +
                ", foracid='" + foracid + '\'' +
                ", appDate=" + appDate +
                ", event='" + event + '\'' +
                ", drawPwr='" + drawPwr + '\'' +
                ", remark='" + remark + '\'' +
                ", drawPwrIndCode='" + drawPwrIndCode + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", requestDate=" + requestDate +
                ", responseDate=" + responseDate +
                ", status='" + status + '\'' +
                '}';
    }
}


