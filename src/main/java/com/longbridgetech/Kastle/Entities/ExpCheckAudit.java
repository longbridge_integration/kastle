//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.longbridgetech.Kastle.Entities;

import com.longbridgetech.Kastle.domain.ExpCheckRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class ExpCheckAudit {
    private long id;
    private String RequestID;
    private String appCode;
    private String custId;
    private String responseCode = "";
    private String responseMessage = "";
    private String errorMessage = "";
    private Date requestDate = new Date();
    private Date responseDate = new Date();
    private String status = "";
    private Logger logger = LoggerFactory.getLogger(ExpCheckAudit.class);

    public ExpCheckAudit() {

    }

    public ExpCheckAudit(ExpCheckRequest request) {
        this.RequestID = request.getRequestID();
        this.appCode = "KASEXPCHECK";
        this.custId = request.getCustomerID();
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRequestID() {
        return RequestID;
    }

    public void setRequestID(String requestID) {
        RequestID = requestID;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    @Override
    public String toString() {
        return "AcctClosureAudit{" +
                "id=" + id +
                ", RequestID='" + RequestID + '\'' +
                ", appCode='" + appCode + '\'' +
                ", custId='" + custId + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", requestDate=" + requestDate +
                ", responseDate=" + responseDate +
                ", status='" + status + '\'' +
                '}';
    }
}


